-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
local composer = require "composer"
local widget  = require "widget"
local json = require "json" 
local utility = require "loadsave"
local MyData = require "mydata"


local isRed, TouchEnabled
local scene = composer.newScene()  
local GRID_WIDTH = 6
local GRID_HEIGHT = 6
--
print( "CAZOOOOOOOOOOOOOOOOOOOOOOOOO" )

local precedenteScena =  composer.getSceneName( "previous" )

local Difficolta 
local livello
local numLivello
local buttonBack
local AroundBack 
local minMosse
local SbloccatoLivelloSuccessivo = false


function scene:create(event)

local sceneGroup = self.view

print("PRECEDENTE====")
print( precedenteScena )


GruppoScenaBase = display.newGroup()
sceneGroup:insert(GruppoScenaBase )

MyData.settings = utility.loadTable("settings8.json")

if event.params == nil then
	numLivello = 1
else
	numLivello = event.params.id
end

Difficolta = event.params.Difficolta
local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
	--
	-- first determine which edge is out of bounds
	--
	local YscaleFactor = fitHeight / displayObject.height 
	local XscaleFactor = fitWidth / displayObject.width 
	--local newWidth = displayObject.width * scaleFactor
--[[
	if not enlarge and scaleFactor > 1 then
		return
	end]]
	displayObject:scale( XscaleFactor, YscaleFactor )
end





getmetatable('').__index = function(str,a)  return string.sub(str,tonumber(a),tonumber(a)) end






Stack = {}

 grid = {}
for i = 1, 6 do
	grid[i] = {}
end

grid2 = {}
for i =1,6 do
		grid2[i]= {}
end



local GRID_WIDTH_PX = display.actualContentWidth-25
local GRID_HEIGHT_PX = GRID_WIDTH_PX

local CELL_WIDTH_V = GRID_WIDTH_PX/6
local CELL_HEIGHT_V = GRID_HEIGHT_PX/3

local CELL_WIDTH_O = GRID_WIDTH_PX/3
local CELL_HEIGHT_O = GRID_HEIGHT_PX/6

local CELL_WIDTH_V3 = GRID_WIDTH_PX/6
local CELL_HEIGHT_V3 = GRID_HEIGHT_PX/2

local CELL_WIDTH_O3 = GRID_WIDTH_PX/2
local CELL_HEIGHT_O3 = GRID_HEIGHT_PX/6


local background = display.newImageRect( GruppoScenaBase, "Leggerwood.png", display.actualContentWidth, display.actualContentHeight )
background.x = display.contentCenterX
background.y = display.contentCenterY

local RettangoloProva  = display.newImage("check_woodP.png")
RettangoloProva.x = display.contentCenterX+GRID_WIDTH_PX/2-10
RettangoloProva.y = display.contentCenterY 
fitImage(RettangoloProva,200,CELL_HEIGHT_O+10 , true)
GruppoScenaBase:insert(RettangoloProva)

local background = display.newImageRect( GruppoScenaBase, "Leggerwood.png", display.actualContentWidth, display.actualContentHeight )
background.x = display.contentCenterX-13
background.y = display.contentCenterY


local check  = display.newImage("check_wood2.png")
check.x = display.contentCenterX
check.y = display.contentCenterY
fitImage(check,GRID_WIDTH_PX, GRID_HEIGHT_PX)
GruppoScenaBase:insert(check)



 checkerBoard = display.newRoundedRect(GruppoScenaBase, display.contentCenterX,display.contentCenterY,GRID_WIDTH_PX, GRID_HEIGHT_PX,20)
 checkerBoard.alpha = 0


score = 0
local cacca = (6/10) *GRID_WIDTH_PX
local cacca2 = cacca/2
--print(display.screenOriginY)
--local line = display.newLine(display.contentCenterX,((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2),display.contentCenterX, (display.contentCenterY-(GRID_WIDTH_PX/2))-10)
--line:setStrokeColor(0,0,0 )
meta = (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2

--local ScoreLevel = display.newRoundedRect(GruppoScenaBase, display.contentCenterX -((GRID_WIDTH_PX/2)-cacca2) , (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2, cacca,((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2 ,6)

local ScoreLevel  = display.newImage("BoardText1.png")
ScoreLevel.x = display.contentCenterX -((GRID_WIDTH_PX/2)-cacca2)
ScoreLevel.y = (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2
fitImage(ScoreLevel,cacca,((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2,true)
GruppoScenaBase:insert( ScoreLevel)


local opzioniTestoBoxGrande = 
{
	parent = GruppoScenaBase,
    text = "Board:",     
    x = ScoreLevel.x,
    y = (ScoreLevel.y)-((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2)),
    font = "VAG.ttf",   
    fontSize = 20,
    align = "center"  -- Alignment parameter
}

local TextBoard = display.newText(opzioniTestoBoxGrande)
TextBoard:setFillColor( 1,1,1 )
TextBoard.y = TextBoard.y+(TextBoard.height)/2+4

local opzioniTestoBoxGrandeSotto = 
{
	parent = GruppoScenaBase,
    text = Difficolta,     
    x = ScoreLevel.x,
    y =(ScoreLevel.y)+((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2)),
    font = "VAG.ttf",   
    fontSize = 14,
    align = "center"  -- Alignment parameter
}
local TextBoardSotto = display.newText( opzioniTestoBoxGrandeSotto)
TextBoardSotto.y = -4 +TextBoardSotto.y - (TextBoardSotto.height)/2


local ScorePunt = display.newRoundedRect( GruppoScenaBase,display.contentCenterX+((GRID_WIDTH_PX)/3), (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2, GRID_WIDTH_PX/3, ((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2 , 6 )
ScorePunt.alpha = 0

local proca2 = display.newRect( GruppoScenaBase, ScorePunt.x, ScoreLevel.y, (ScorePunt.width/2), (ScorePunt.height/2) )
proca2:setFillColor( 0,0,0 )
proca2.alpha = 0

local proca3 = display.newRect( GruppoScenaBase, ScorePunt.x, proca2.y-(proca2.height/2)-(proca2.height/4), (ScorePunt.width/2), (ScorePunt.height/6) )
proca3:setFillColor( 0,0,0 )
proca3.alpha = 0

local YourScoreBox = display.newImage("BoardText1.png")
YourScoreBox.x = ScorePunt.x
YourScoreBox.y = ScorePunt.y
fitImage(YourScoreBox,(ScorePunt.width),(ScorePunt.height))
GruppoScenaBase:insert( YourScoreBox)

options0 = {
	parent = GruppoScenaBase,
	text = score,
	x = ScorePunt.x,
	y = ScorePunt.y,
	--width = ScorePunt.width/2,
	--height = ScorePunt.height/2,
	font = "VAG.ttf",
	fontSize = (ScoreLevel.contentBounds.yMax -ScoreLevel.contentBounds.yMin)/2,
	align = "center" 
}

local Text2 = display.newText( GruppoScenaBase,"Your Score :", ScorePunt.x, TextBoard.y, "VAG.ttf",15 )
Text2:setFillColor( 1,1,1 )

local opzioniNumeroBoxGrande = 
{
	parent = GruppoScenaBase,
    text = numLivello,     
    x = ScoreLevel.x,
    y = ScoreLevel.y+3,
    font = "VAG.ttf",   
    fontSize = (ScoreLevel.contentBounds.yMax -ScoreLevel.contentBounds.yMin)/2,
    align = "center"  -- Alignment parameter
}

local Text3 = display.newText(opzioniNumeroBoxGrande)
Text3:setFillColor( 1,1,1 )

local scoreBoard = display.newText(options0)
scoreBoard:setFillColor( 1,1,1 )





local gbOffsetX = checkerBoard.x - ( checkerBoard.width * checkerBoard.anchorX ) 
local gbOffsetY = checkerBoard.y - ( checkerBoard.height * checkerBoard.anchorY )




function addScore()
	score = score+1
	scoreBoard.text = tostring(score)
	return true
end

function subScore()
	score = score-1
	scoreBoard.text = tostring(score)
end


function Stack:Create()

  -- stack table
  local t = {}
  -- entry table
  t._et = {}

  -- push a value on to the stack
  function t:push(...)
    if ... then
      local targs = {...}
      -- add values
      for _,v in ipairs(targs) do
        table.insert(self._et, v)
      end
    end
  end

  -- pop a value from the stack
  function t:pop(num)

    -- get num values from stack
    local num = num or 1

    -- return table
    local entries = {}

    -- get values into entries
    for i = 1, num do
      -- get last entry
      if #self._et ~= 0 then
        table.insert(entries, self._et[#self._et])
        -- remove last value
        table.remove(self._et)
      else
        break
      end
    end
    -- return unpacked entries
    return unpack(entries)
  end

  -- get entries
  function t:getn()
    return #self._et
  end

  -- list values
  function t:list()
    for i,v in pairs(self._et) do
      print(i, v)
    end
  end
  return t
end

stack = Stack:Create()





--[[AroundBack = display.newRoundedRect( GruppoScenaBase, display.contentCenterX + (3/8)+ (checkerBoard.width/4 - (3/4))/2 ,(meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY),checkerBoard.width/4 - (3/4), checkerBoard.width/6 - (1/6),6 )
AroundBack.xScale = 0.8
AroundBack.yScale = 0.8
AroundBack.alpha = 0.75
--]]
AroundBack = display.newImage("PRIMO.png")
AroundBack.x = display.contentCenterX + (3/8)+ (checkerBoard.width/4 - (3/4))/2
AroundBack.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
fitImage(AroundBack,checkerBoard.width/4 - (3/4), checkerBoard.width/6 - (1/6) )
AroundBack:scale(0.96,0.8) 
GruppoScenaBase:insert(AroundBack)

 
local AroundRestart = display.newImage("PRIMO.png")
AroundRestart.x = AroundBack.x + (3/4)+ (checkerBoard.width/4 - (3/4))
AroundRestart.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
fitImage(AroundRestart,checkerBoard.width/4 - (3/4),checkerBoard.width/6 - (1/6))
AroundRestart:scale(0.96,0.8)
GruppoScenaBase:insert(AroundRestart)

local AroundResolve = display.newImage( "PRIMO.png")
AroundResolve.x = display.contentCenterX-(3/8)- (checkerBoard.width/4 - (3/4))/2
AroundResolve.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
fitImage(AroundResolve,checkerBoard.width/4 - (3/4),checkerBoard.width/6 - (1/6))
AroundResolve:scale(0.96,0.8)
GruppoScenaBase:insert(AroundResolve)

local AroundMenu = display.newImage( "PRIMO.png")
AroundMenu.x = AroundResolve.x - (1/4)-2*((checkerBoard.width/4 - (3/4))/2)
AroundMenu.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
fitImage(AroundMenu,checkerBoard.width/4 - (3/4), checkerBoard.width/6- (1/6))
AroundMenu:scale( 0.96, 0.8 )
GruppoScenaBase:insert(AroundMenu)

--[[
local AroundRestart = display.newRoundedRect( GruppoScenaBase, ,checkerBoard.width/4 - (3/4), checkerBoard.width/6 - (1/6),6 )
AroundRestart.xScale = 0.8
AroundRestart.yScale = 0.8--]]
--[[
local AroundResolve = display.newRoundedRect( GruppoScenaBase, ,, , ,6 )
AroundResolve.xScale = 0.8
AroundResolve.yScale = 0.8--]]
--[[
local AroundMenu= display.newRoundedRect( GruppoScenaBase,  ,, , checkerBoard.width/6- (1/6),6 )
AroundMenu.xScale = 0.8
AroundMenu.yScale = 0.8--]]

local options3 = {
	width = 256,
	height = 256,
	defaultFile = "back1.png",
	overFile = "back1Over.png",
	isEnabled = false
	}
local options4 = {
	width = 256,
	height = 256,
	defaultFile = "refresh1.png",
	isEnabled = true
	}
local options2 = {
	width = 256,
	height = 256,
	defaultFile = "risolvi1.png",
	isEnabled = false
	}
local options1 = {
	width = 256,
	height = 256,
	defaultFile = "pause1.png",
	isEnabled = false
	}

buttonBack = widget.newButton( options3 )
buttonBack.x = AroundBack.x
buttonBack.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
buttonBack.xScale = 0.15
buttonBack.yScale = 0.15
GruppoScenaBase:insert( buttonBack)

local buttonRestart = widget.newButton(options4)
buttonRestart.x = AroundRestart.x
buttonRestart.y = buttonBack.y
buttonRestart.xScale = 0.15
buttonRestart.yScale = 0.15
GruppoScenaBase:insert( buttonRestart)

local buttonResolve = widget.newButton( options2 )
buttonResolve.x = AroundResolve.x
buttonResolve.y = AroundResolve.y
buttonResolve.xScale = 0.15
buttonResolve.yScale = 0.15
GruppoScenaBase:insert( buttonResolve)

local buttonPause = widget.newButton( options1 )
buttonPause.x = AroundMenu.x
buttonPause.y = AroundMenu.y
buttonPause.xScale = 0.15
buttonPause.yScale = 0.15
GruppoScenaBase:insert( buttonPause)






local paint = {
    type = "image",
    filename = "perfettoVert.jpg" ,
}

local paintO = {
    type = "image",
    filename = "perfettoOrizz.jpg" ,
}

local paintRosso = {
    type = "image",
    filename = "rosso.jpg" ,
}



print( Difficolta )


objects = {}
-- load the background image:
local StringaJson = "prova2/"..Difficolta..".json"
print(StringaJson)

local filename = system.pathForFile( StringaJson, system.ResourceDirectory )
local tabella  = json.decodeFile(filename)


--[[if(event.params.numLivello) == nil then
	numLivello = "0"
else 
	numLivello = event.params.numLivello
end
--]]



livello = "livello"..tostring(numLivello)
--+numLivello

local tabellaStringa = tabella[livello]["r1"]..tabella[livello]["r2"]..tabella[livello]["r3"]..tabella[livello]["r4"]..tabella[livello]["r5"]..tabella[livello]["r6"]
minMosse = tabella[livello]["minMosse"]


function Abilita()

	buttonBack.isEnabled = true
	AroundBack.alpha = 1
end

function Disabilita()
	buttonBack.isEnabled = false
end







function MoveUndo(event)
	local arrayMoveUndo = stack:pop(1)
	if arrayMoveUndo == nil then
		buttonBack.isEnabled = false
		AroundBack.alpha = 0.75
	else
		buttonBack.isEnabled = true
		if arrayMoveUndo[1].What == "Orizz2" or arrayMoveUndo[1].What == "Orizz3" then 
			transition.to(arrayMoveUndo[1], { time = 100 ,onStart = Disabilita, onComplete = Abilita , x = arrayMoveUndo[2]})
			subScore()
		elseif arrayMoveUndo[1].What == "Vert2" or arrayMoveUndo[1].What == "Vert3" then
			transition.to(arrayMoveUndo[1], { time = 100,onStart = Disabilita ,onComplete= Abilita, y = arrayMoveUndo[3]})
			subScore()
		end
	end
end


local LimitatoriY = {}
for i = 1,6 do
	LimitatoriY[i] = {}
end

local LimitatoriX = {}
for i = 1,6 do
	LimitatoriX[i] = {}
end








l =1
r = 1
for i =1,6 do
	for j = 1,6 do
		local XPos,Ypos
		cell = display.newRoundedRect( GruppoScenaBase,0,0,CELL_WIDTH_V,CELL_WIDTH_V,6)
		cell.x = (j-1) *CELL_WIDTH_V + (CELL_WIDTH_V*0.5) +gbOffsetX
		cell.y = (i-1) * CELL_WIDTH_V + (CELL_WIDTH_V*0.5) + gbOffsetY
		cell.alpha = 0
		--cell:setFillColor( 0,0,0 )
		grid2[i][j]=cell
		if l<7 then
			LimitatoriX[l].X = cell.width
			LimitatoriX[l].Centro = cell.x
		end 
		l = l+1
	end
		LimitatoriY[i].Y = cell.height
		LimitatoriY[i].Centro = cell.y
end

--[[uscita = display.newRect( GruppoScenaBase, 0, 0, CELL_WIDTH_V/2,CELL_WIDTH_V, 25,CELL_WIDTH_V )
uscita.x =	grid2[3][6].x+24.5
uscita.y = grid2[3][6].y

uscita:setFillColor( 0,0,0 )
uscita.strokeWidth = 4
uscita.alpha = 1
uscita:setStrokeColor( 0,0,0 )--]]
RettangoloProva.y = grid2[3][6].y



ZonaLimitatoriY = {}
for i =1,6 do
	ZonaLimitatoriY[i] = display.newRect( GruppoScenaBase, checkerBoard.x, LimitatoriY[i].Centro, checkerBoard.width, LimitatoriY[i].Y )
	ZonaLimitatoriY[i]:setFillColor( 0,0,0 )
	ZonaLimitatoriY[i].alpha = 0
end

ZonaLimitatoriX = {}
for i =1,6 do
	ZonaLimitatoriX[i] = display.newRect( GruppoScenaBase, LimitatoriX[i].Centro, checkerBoard.y, LimitatoriX[i].X, checkerBoard.height )
	ZonaLimitatoriX[i]:setFillColor( 1,1,1 )
	ZonaLimitatoriX[i].alpha = 0
end





function WhoIsVert(MinZonaY,MaxZonaY,MinZonaX,MaxZonaX)
	local flag = true
	local flag2 = true
	local GiustaYMin
	local GiustaYMax
	local i = (MinZonaY-1)
	while (i >=1 and flag) do
		for p=1,#objects2 do
			local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[p])
			local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[p])
			--print( true and (ZonaXMinOggetto == MinZonaX or ZonaXMaxOggetto == MinZonaX ))
			if (i >= ZonaYMinOggetto and  i <= ZonaYMaxOggetto) and (MinZonaX >= ZonaXMinOggetto and  MaxZonaX <= ZonaXMaxOggetto)  then
				flag = false
				GiustaYMin = i
				break
			end
		end
		i = i-1
	end

	local j = (MaxZonaY)+1
	while (j<7 and flag2) do
		for k=1,#objects2 do
			local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[k])
			local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[k])
			if (j >= ZonaYMinOggetto and  j <= ZonaYMaxOggetto) and (MinZonaX >= ZonaXMinOggetto and  MaxZonaX <= ZonaXMaxOggetto) then
				GiustaYMax = j
				flag2 = false
				break
			end
		end
		j = j+1
	end

	return GiustaYMin,GiustaYMax

end

function WhoIsOriz(MinZonaX,MaxZonaX,MinZonaY,MaxZonaY)
	local flag = true
	local flag2 = true
	local GiustaXMax
	local GiustaXMin
	local i = MinZonaX-1
	while (i>=1 and flag) do
		for p = 1,#objects2 do
			local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[p])
			local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[p])
			if ((i >= ZonaXMinOggetto and  i <= ZonaXMaxOggetto) and (MinZonaY >= ZonaYMinOggetto and  MaxZonaY <= ZonaYMaxOggetto)) then
				flag = false
				GiustaXMin= i
				break
			end
		end
		i = i-1
	end
	local j = (MaxZonaX+1)
	while (j<7 and flag2) do
		for k =1, #objects2 do
			local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[k])
			local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[k])
			if ((j >= ZonaXMinOggetto and  j <= ZonaXMaxOggetto) and (MinZonaY >= ZonaYMinOggetto and  MaxZonaY <= ZonaYMaxOggetto)) then
				flag2 = false
				GiustaXMax= j
				break
			end
		end
		j=j+1
	end

	return GiustaXMin,GiustaXMax

end




function DammiPosLibereV(ZonaMinX,ZonaMaxX,ZonaMinY,ZonaMaxY,What,Lenght)
	local YmaxDrag,YminDrag,XmaxDrag,XminDrag 
	if (What == "Vert2" or What == "Vert3") then
		YminDrag,YmaxDrag = WhoIsVert(ZonaMinY,ZonaMaxY,ZonaMinX,ZonaMaxX)
		if YminDrag == nil then
			YminDrag = checkerBoard.y- checkerBoard.height/2
		end
		if YmaxDrag == nil then
			YmaxDrag = checkerBoard.y+checkerBoard.height/2
		end

	end

	return YminDrag,YmaxDrag
end


function DammiPosLibereO(ZonaMinX,ZonaMaxX,ZonaMinY,ZonaMaxY,What,Lenght)
	local XmaxDrag,XminDrag 
	if (What == "Orizz2" or What == "Orizz3" ) then
		XminDrag,XmaxDrag = WhoIsOriz(ZonaMinX,ZonaMaxX,ZonaMinY,ZonaMaxY)
		if XminDrag == nil then
			XminDrag = checkerBoard.x- checkerBoard.width/2
		end
		if XmaxDrag == nil then
			XmaxDrag = checkerBoard.x+checkerBoard.width/2
		end
	end
	return XminDrag,XmaxDrag
end



function StampaQuadratiDoveMiTrovoX(rettangolo)
	local ZonaXMin, ZonaXMax
	if (rettangolo.What == "Vert2" or rettangolo.What == "Vert3" ) then
		if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[1].contentBounds.xMax ) then
			ZonaXmin = 1
			ZonaXMax = 1
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[2].contentBounds.xMax ) then
			ZonaXmin = 2
			ZonaXMax = 2
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax) then
			ZonaXmin = 3
			ZonaXMax = 3
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax) then
			ZonaXmin = 4
			ZonaXMax = 4
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
			ZonaXmin = 5
			ZonaXMax = 5
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax) then
			ZonaXmin = 6
			ZonaXMax = 6
		end
	
	elseif (rettangolo.What == "Orizz2") then
		if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[2].contentBounds.xMax ) then
			ZonaXmin = 1
			ZonaXMax = 2
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax ) then
			ZonaXmin = 2
			ZonaXMax = 3
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax ) then
			ZonaXmin = 3
			ZonaXMax = 4
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
			ZonaXmin = 4
			ZonaXMax = 5
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax ) then
			ZonaXmin = 5
			ZonaXMax = 6
		end

	elseif (rettangolo.What == "Orizz3") then
		if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax ) then
			ZonaXmin = 1
			ZonaXMax = 3
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax ) then
			ZonaXmin = 2
			ZonaXMax = 4
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
			ZonaXmin = 3
			ZonaXMax = 5
		elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax ) then
			ZonaXmin = 4
			ZonaXMax = 6
		end

	end
	return ZonaXmin,ZonaXMax

end




function StampaQuadratiDoveMiTrovoY(rettangolo)
	local ZonaYMin, ZonaYMax 
	if (rettangolo.What == "Vert2") then
			if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[2].contentBounds.yMax ) then
				ZonaYMin =1
				ZonaYMax = 2 
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
				ZonaYMin =2
				ZonaYMax = 3
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax ) then
				ZonaYMin =3
				ZonaYMax = 4
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
				ZonaYMin =4
				ZonaYMax = 5
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
				ZonaYMin =5
				ZonaYMax = 6
			end
	elseif (rettangolo.What == "Vert3") then
			if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
				ZonaYMin =1
				ZonaYMax = 3
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax) then
				ZonaYMin =2
				ZonaYMax = 4
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
				ZonaYMin =3
				ZonaYMax = 5
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
				ZonaYMin =4
				ZonaYMax = 6
			end
	elseif (rettangolo.What == "Orizz2" or rettangolo.What == "Orizz3") then
			if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[1].contentBounds.yMax) then
				ZonaYMin =1
				ZonaYMax = 1
			elseif(rettangolo.contentBounds.yMax < ZonaLimitatoriY[2].contentBounds.yMax ) then
				ZonaYMin =2
				ZonaYMax = 2
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
				ZonaYMin =3
				ZonaYMax = 3
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax ) then
				ZonaYMin =4
				ZonaYMax = 4
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
				ZonaYMin =5
				ZonaYMax = 5
			elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
				ZonaYMin =6
				ZonaYMax = 6
			end
	
	end
	return ZonaYMin,ZonaYMax
end


function StampaSottoZoneV(rettangolo)
	if (rettangolo.What == "Vert2") then
		if (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[2].contentBounds.yMin + (ZonaLimitatoriY[2].height)/2 ) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[1].contentBounds.yMax ) then
			print( "Zona = 2.1 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMax})
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[2].contentBounds.yMin + (ZonaLimitatoriY[2].height)/2 ) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMin ) then
			print( "Zona = 2.2 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMax})
			ListenerOrizz(rettangolo)


		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height)/2 ) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[2].contentBounds.yMax ) then
			print( "Zona = 3.1 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMin ) then
			print( "Zona = 3.2 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[2].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[3].contentBounds.yMax ) then
			print( "Zona = 4.1 Max" )
			transition.to(rettangolo,{ time =100, y =ZonaLimitatoriY[2].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMin ) then
			print( "Zona = 4.2 Max" )
			transition.to(rettangolo,{ time=100, y =ZonaLimitatoriY[3].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2))  and  rettangolo.contentBounds.yMax > ZonaLimitatoriY[4].contentBounds.yMax) then
			print( "Zona = 5.1 Max" )
			transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[3].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMin ) then
			print( "Zona = 5.2 Max" )
			transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[4].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[5].contentBounds.yMax) then
			print( "Zona = 6.1 Max" )
			transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[4].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2))) then
			print( "Zona = 6.2 Max" )
			transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[5].contentBounds.yMax   })
			ListenerOrizz(rettangolo)
		end

	elseif (rettangolo.What == "Vert3") then
		if (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height)/2 ) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[2].contentBounds.yMax ) then
			print( "Zona = 3.1 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMin + (ZonaLimitatoriY[1].height)/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMin ) then
			print( "Zona = 3.2 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[2].contentBounds.yMin + ZonaLimitatoriY[2].height/2    })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[3].contentBounds.yMax ) then
			print( "Zona = 4.1 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[2].contentBounds.yMin +(ZonaLimitatoriY[2].height/2)   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMin ) then
			print( "Zona = 4.2 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[3].contentBounds.yMin + ZonaLimitatoriY[3].height/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2))  and  rettangolo.contentBounds.yMax > ZonaLimitatoriY[4].contentBounds.yMax) then
			print( "Zona = 5.1 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[3].contentBounds.yMin + ZonaLimitatoriY[3].height/2 })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMin ) then
			print( "Zona = 5.2 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[4].contentBounds.yMin +ZonaLimitatoriY[4].height/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[5].contentBounds.yMax) then
			print( "Zona = 6.1 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[4].contentBounds.yMin + ZonaLimitatoriY[4].height/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2))) then
			print( "Zona = 6.2 Max" )
			transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[5].contentBounds.yMin + ZonaLimitatoriY[5].height/2    })
			ListenerOrizz(rettangolo)
		end

	end
end



centroX = 0
centroY = 0
function ListenerOrizz(oggetto)
	if oggetto.What == "Vert2" or oggetto.What == "Vert3" then
		timer.performWithDelay(100,function()  if not ControllaDueCentriV(oggetto) then
		local array = {}
		array[1] = oggetto
		array[2] = centroX
		array[3] = centroY
		--print( centroX )
		buttonBack.isEnabled = true 
		AroundBack.alpha = 1
		stack:push(array)
			end
			end)
	
	elseif oggetto.What == "Orizz2" or oggetto.What=="Orizz3" then
		timer.performWithDelay(100,function()  if not ControllaDueCentriO(oggetto) then
		local array = {}
		array[1] = oggetto
		array[2] = centroX
		array[3] = centroY
		--print( centroX )
		buttonBack.isEnabled = true 
		AroundBack.alpha = 1
		stack:push(array)
			end
			end)
end
end

function ControllaDueCentriO( rettangolo)
	local flagControlla = false
	print( "CENTRIX:"..centroX,rettangolo.x )
	if centroX <= (rettangolo.x+0.5) and centroX >= (rettangolo.x -0.5)  then
		flagControlla = true
	end
	print( flagControlla )
	return flagControlla
end



function ControllaDueCentriV( rettangolo)
	local flagControlla = false
	print( "CENTRI:"..centroY,rettangolo.y )
	if centroY <= (rettangolo.y+0.5) and centroY >= (rettangolo.y -0.5)  then
		flagControlla = true
	end
	return flagControlla
end

function StampaSottoZoneO(rettangolo)
	print("NON DEVO STARE QUAAAAAAAAAAA")
	if (rettangolo.What == "Orizz2") then
		if (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[2].contentBounds.xMin + (ZonaLimitatoriX[2].width)/2 ) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[1].contentBounds.xMax ) then
			print( "Zona = 2.1 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMax})
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[2].contentBounds.xMin + (ZonaLimitatoriX[2].width)/2 ) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMin ) then
			print( "Zona = 2.2 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMax})
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width)/2 ) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[2].contentBounds.xMax ) then
			print( "Zona = 3.1 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMax})
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMin ) then
			print( "Zona = 3.2 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[2].contentBounds.xMax   })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[3].contentBounds.xMax ) then
			print( "Zona = 4.1 Max" )
			transition.to(rettangolo,{ time =100, x =ZonaLimitatoriX[2].contentBounds.xMax })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMin ) then
			print( "Zona = 4.2 Max" )
			transition.to(rettangolo,{ time=100, x =ZonaLimitatoriX[3].contentBounds.xMax })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2))  and  rettangolo.contentBounds.xMax > ZonaLimitatoriX[4].contentBounds.xMax) then
			print( "Zona = 5.1 Max" )
			transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[3].contentBounds.xMax})
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMin ) then
			print( "Zona = 5.2 Max" )
			transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[4].contentBounds.xMax })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[5].contentBounds.xMax) then
			transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[4].contentBounds.xMax })
			ListenerOrizz(rettangolo)
		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2))) then 
			print( "Zona = 6.1 Max" )
			print( "Zona = 6.2 Max" )
			transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[5].contentBounds.xMax})
			ListenerOrizz(rettangolo)
			
		end

	elseif (rettangolo.What == "Orizz3") then
		if (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width)/2 ) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[2].contentBounds.xMax ) then
			print( "Zona = 3.1 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMin + (ZonaLimitatoriX[1].width)/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMin ) then
			print( "Zona = 3.2 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[2].contentBounds.xMin + ZonaLimitatoriX[2].width/2    })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[3].contentBounds.xMax ) then
			print( "Zona = 4.1 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[2].contentBounds.xMin +(ZonaLimitatoriX[2].width/2)   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMin ) then
			print( "Zona = 4.2 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[3].contentBounds.xMin + ZonaLimitatoriX[3].width/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2))  and  rettangolo.contentBounds.xMax > ZonaLimitatoriX[4].contentBounds.xMax) then
			print( "Zona = 5.1 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[3].contentBounds.xMin + ZonaLimitatoriX[3].width/2 , time = 300 })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMin ) then
			print( "Zona = 5.2 Max" )
			transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[4].contentBounds.xMin +ZonaLimitatoriX[4].width/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[5].contentBounds.xMax) then
			print( "Zona = 6.1 Max" )
			transition.to(rettangolo,{time =100, x =ZonaLimitatoriX[4].contentBounds.xMin + ZonaLimitatoriX[4].width/2   })
			ListenerOrizz(rettangolo)

		elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2))) then
			print( "Zona = 6.2 Max" )
			transition.to(rettangolo,{time =100, x =ZonaLimitatoriX[5].contentBounds.xMin + ZonaLimitatoriX[5].width/2    })
			ListenerOrizz(rettangolo)
		end

	end
end



function onTouchO(event)
	if event.phase == "began" then
		objects2 = {}
		for i=1, #objects do
			objects2[i] = objects[i]
		end
		flagNilXMin = false
		flagNilXMax = false
		centroX = event.target.x
		--flagVert3 = false
		table.remove(objects2, event.target.MyID)
		--YMINIMODRAG,YMASSIMODRAG 
		--print( "began" )
		local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
		local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
		XMINIMODRAG,XMASSIMODRAG = DammiPosLibereO(arg1,arg2,arg3,arg4,event.target.What,#objects2)
		print( XMINIMODRAG,XMASSIMODRAG )
		if XMASSIMODRAG == checkerBoard.x + checkerBoard.width/2 then
			flagNilXMax = true
		end
		if XMINIMODRAG == checkerBoard.x - checkerBoard.width/2 then
			flagNilXMin = true
		end
		display.getCurrentStage( ):setFocus( event.target)
		event.target.isFocus = true
		event.target.xStart = event.x-event.target.x

	elseif event.target.isFocus then
		if event.phase == "moved" then
			event.target.x = event.x-event.target.xStart
			if event.target.What == "Orizz3" then

				if flagNilXMin and not flagNilXMax then
					print( "1" )
					if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
						event.target.x = ZonaLimitatoriX[XMASSIMODRAG-2].contentBounds.xMin + CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
						event.target.x = ZonaLimitatoriX[2].contentBounds.xMax -CELL_HEIGHT_O/2
					end

				elseif flagNilXMax and not flagNilXMin then
					print( "2" )
					if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
						event.target.x = ZonaLimitatoriX[5].contentBounds.xMin + CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
						event.target.x = ZonaLimitatoriX[XMINIMODRAG+2].contentBounds.xMax - CELL_HEIGHT_O/2
					end 
				elseif flagNilXMin and flagNilXMax then
					print( "3" )
					if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
						event.target.x = ZonaLimitatoriX[5].contentBounds.xMin +CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
						event.target.x = ZonaLimitatoriX[2].contentBounds.xMax -CELL_HEIGHT_O/2
					end
				else 
					print( "4" )
					if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
						event.target.x = ZonaLimitatoriX[XMASSIMODRAG-2].contentBounds.xMin +CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
						event.target.x = ZonaLimitatoriX[XMINIMODRAG+2].contentBounds.xMax - CELL_HEIGHT_O/2
					end
				end
			

			else
				if flagNilXMin and not flagNilXMax then
					print( "1e" )
					if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
						event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
					elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
						event.target.x = ZonaLimitatoriX[2].contentBounds.xMin 
					end

				elseif flagNilXMax and not flagNilXMin then
					print( "2e" )
					if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
						event.target.x = ZonaLimitatoriX[5].contentBounds.xMax
					elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
						event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
					end 
				elseif flagNilXMin and flagNilXMax then
					print( "3e" )
					if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
						event.target.x = ZonaLimitatoriX[5].contentBounds.xMax 
					elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
						event.target.x = ZonaLimitatoriX[2].contentBounds.xMin
					end
				else 
					print( "4e" )
					if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
						event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
					elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
						event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
					end
				end
			end


		elseif event.phase =="ended" or event.phase == "cancelled" then
			StampaSottoZoneO(event.target)
			 timer.performWithDelay( 100, function( ) if not ControllaDueCentriO(event.target) then addScore() end end  ) 
			display.getCurrentStage( ):setFocus( event.target,nil )
			event.target.isFocus = false
		end

	end
	return true
end


function onTouchOX(event)
	if event.phase == "began" then
		objects2 = {}
		for i=1, #objects do
			objects2[i] = objects[i]
		end
		flagNilXMin = false
		flagNilXMax = false
		centroX = event.target.x
		--flagVert3 = false
		table.remove(objects2, event.target.MyID)
		--YMINIMODRAG,YMASSIMODRAG 
		--print( "began" )
		local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
		local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
		XMINIMODRAG,XMASSIMODRAG = DammiPosLibereO(arg1,arg2,arg3,arg4,event.target.What,#objects2)
		print( XMINIMODRAG,XMASSIMODRAG )
		if XMASSIMODRAG == checkerBoard.x + checkerBoard.width/2 then
			flagNilXMax = true
		end
		if XMINIMODRAG == checkerBoard.x - checkerBoard.width/2 then
			flagNilXMin = true
		end
		isRed = true
		TouchEnabled = true
		display.getCurrentStage( ):setFocus( event.target)
		event.target.isFocus = true
		event.target.xStart = event.x-event.target.x

	elseif event.target.isFocus then

		if event.phase == "moved" and TouchEnabled then
			print("MOVEEEE")
			event.target.x = event.x-event.target.xStart
				if flagNilXMin and not flagNilXMax then
					print( "1e" )
					if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
						event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
					elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
						event.target.x = ZonaLimitatoriX[2].contentBounds.xMin 
					end

				elseif flagNilXMax and not flagNilXMin then
					print( "2e" )
					if(event.target.contentBounds.xMax > XMASSIMODRAG-40) then 
						event.target.x = ZonaLimitatoriX[5].contentBounds.xMax
						TouchEnabled = false
						print("appenaMessoFalse")
					elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
						event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax  
					end 

				elseif flagNilXMin and flagNilXMax then
					print( "3e" )
					if(event.target.contentBounds.xMax > XMASSIMODRAG-40) then 
						event.target.x = ZonaLimitatoriX[5].contentBounds.xMax 
						TouchEnabled = false
					elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
						event.target.x = ZonaLimitatoriX[2].contentBounds.xMin
					end

				else 
					print( "4e" )
					if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
						event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
					elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
						event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
					end
				end

		elseif event.phase =="ended" or event.phase == "cancelled" then
			if  TouchEnabled then
				print("NNON CI DEVO STARE!!")
				StampaSottoZoneO(event.target)
				timer.performWithDelay( 100, function( ) if not ControllaDueCentriO(event.target) then addScore() end end  ) 
				display.getCurrentStage( ):setFocus( event.target,nil )
				event.target.isFocus = false
			else
				timer.performWithDelay( 100, function( ) if not ControllaDueCentriO(event.target) then addScore() end end  ) 
				display.getCurrentStage( ):setFocus( event.target,nil )
				event.target.isFocus = false
			end

		end
	end
	return true
end







function onTouchV(event)
	if event.phase == "began" then
		objects2 = {}
		for i=1, #objects do
			objects2[i] = objects[i]
		end
		flagNilYMin = false
		flagNilYMax = false
		--flagVert3 = false
		centroY = event.target.y
		table.remove(objects2, event.target.MyID)
		--YMINIMODRAG,YMASSIMODRAG 
		--print( "began" )
		local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
		local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
		YMINIMODRAG,YMASSIMODRAG = DammiPosLibereV(arg1,arg2,arg3,arg4,event.target.What,#objects2)
		if YMASSIMODRAG == checkerBoard.y + checkerBoard.height/2 then
			flagNilYMax = true
		end
		if YMINIMODRAG == checkerBoard.y - checkerBoard.height/2 then
			flagNilYMin = true
		end
		print( flagNilYMax,flagNilYMin )
		display.getCurrentStage( ):setFocus( event.target)
		event.target.isFocus = true
		event.target.yStart = event.y-event.target.y

	elseif event.target.isFocus then
		if event.phase == "moved" then
			event.target.y = event.y-event.target.yStart
			if event.target.What == "Vert3" then

				if flagNilYMin and not flagNilYMax then
					print( "1" )
					if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
						event.target.y= ZonaLimitatoriY[YMASSIMODRAG-2].contentBounds.yMin + CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
						event.target.y = ZonaLimitatoriY[2].contentBounds.yMax -CELL_HEIGHT_O/2
					end

				elseif flagNilYMax and not flagNilYMin then
					print( "2" )
					if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
						event.target.y= ZonaLimitatoriY[5].contentBounds.yMin + CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
						event.target.y = ZonaLimitatoriY[YMINIMODRAG+2].contentBounds.yMax - CELL_HEIGHT_O/2
					end 
				elseif flagNilYMin and flagNilYMax then
					print( "3" )
					if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
						event.target.y= ZonaLimitatoriY[5].contentBounds.yMin +CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
						event.target.y = ZonaLimitatoriY[2].contentBounds.yMax -CELL_HEIGHT_O/2
					end
				else 
					print( "4" )
					if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
						event.target.y= ZonaLimitatoriY[YMASSIMODRAG-2].contentBounds.yMin +CELL_HEIGHT_O/2
					elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
						event.target.y = ZonaLimitatoriY[YMINIMODRAG+2].contentBounds.yMax - CELL_HEIGHT_O/2
					end
				end
			

			else
				if flagNilYMin and not flagNilYMax then
					print( "1e" )
					if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
						event.target.y= ZonaLimitatoriY[YMASSIMODRAG-1].contentBounds.yMin 
					elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
						event.target.y = ZonaLimitatoriY[2].contentBounds.yMin 
					end

				elseif flagNilYMax and not flagNilYMin then
					print( "2e" )
					if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
						event.target.y= ZonaLimitatoriY[5].contentBounds.yMax
					elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
						event.target.y = ZonaLimitatoriY[YMINIMODRAG+1].contentBounds.yMax 
					end 
				elseif flagNilYMin and flagNilYMax then
					print( "3e" )
					if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
						event.target.y= ZonaLimitatoriY[5].contentBounds.yMax 
					elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
						event.target.y = ZonaLimitatoriY[2].contentBounds.yMin
					end
				else 
					print( "4e" )
					if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
						event.target.y= ZonaLimitatoriY[YMASSIMODRAG-1].contentBounds.yMin 
					elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
						event.target.y = ZonaLimitatoriY[YMINIMODRAG+1].contentBounds.yMax 
					end
				end
			end








		
		elseif event.phase =="ended" or event.phase == "cancelled" then
			print( "ended" )
			StampaSottoZoneV(event.target)
			 timer.performWithDelay( 100, function( ) if not ControllaDueCentriV(event.target) then addScore() end end  )  
			

			display.getCurrentStage( ):setFocus( event.target,nil )
			event.target.isFocus = false
		end

	end
	return true
end








k =1
j = 1
for riga =1,6 do
	for colonna = 1,6 do

		if(tabellaStringa[k] == 'B' and grid[riga][colonna] == nil ) then
			piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_V, CELL_HEIGHT_V,6 )
			piece.x = (colonna - 1) * CELL_WIDTH_V + (CELL_WIDTH_V * 0.5) + gbOffsetX
			piece.y = (riga) * CELL_HEIGHT_O + gbOffsetY 
			piece.xScale = 0.93
			piece.yScale = 0.95
			--piece:setFillColor(0.13,0.29,0.41)
			piece.MyID = j
			piece.What = "Vert2"
			piece.fill = paint
			grid[riga][colonna] = piece
			grid[riga+1][colonna] = piece
			objects[j] = piece
			print( piece.height )
			j = j+1
			piece:addEventListener("touch",onTouchV)	

			elseif(tabellaStringa[k] == 'C' and grid[riga][colonna] == nil ) then
				piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_V3, CELL_HEIGHT_V3,6 )
				piece.x = (colonna - 1) * CELL_WIDTH_V3+ (CELL_WIDTH_V3 * 0.5) + gbOffsetX
				piece.y = (riga ) * CELL_HEIGHT_O + (CELL_WIDTH_V3 * 0.5)+gbOffsetY 
				piece.xScale = 0.93
				piece.yScale = 0.97
				--piece:setFillColor( 0.13,0.29,0.41)
				piece.MyID = j
				piece.What = "Vert3"
				piece.fill = paint
				grid[riga][colonna] = piece
				grid[riga+1][colonna] = piece
				grid[riga+2][colonna] = piece
				objects[j] = piece
				j = j+1
				piece:addEventListener("touch",onTouchV)	
		
			elseif(tabellaStringa[k] == '2' and grid[riga][colonna] == nil) then
				piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
				piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
				piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
				piece.xScale = 0.95
				piece.yScale = 0.90
				--piece:setFillColor(0.13,0.29,0.41)
				piece.MyID = j
				piece.What = "Orizz2"
				piece.fill = paintO
				grid[riga][colonna] = piece
				grid[riga][colonna+1] = grid[riga][colonna]
				objects[j] = piece
				j = j+1
				piece:addEventListener("touch",onTouchO)	
		
			elseif(tabellaStringa[k] == '3' and grid[riga][colonna] == nil) then
				piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_O3, CELL_HEIGHT_O3,6 )
				piece.x = (colonna -1) * CELL_HEIGHT_O3 + (CELL_WIDTH_O3 * 0.5) + gbOffsetX
				piece.y = (riga - 1) * CELL_HEIGHT_O3 + (CELL_HEIGHT_O3 * 0.5) + gbOffsetY 
				piece.xScale = 0.97
				piece.yScale = 0.92
				--piece:setFillColor(0.13,0.29,0.41)
				piece.MyID = j
				piece.What = "Orizz3"
				piece.fill = paintO
				grid[riga][colonna] = piece
				grid[riga][colonna+1] = grid[riga][colonna]
				grid[riga][colonna+2] = grid[riga][colonna]
				objects[j] = piece
				j = j+1
				piece:addEventListener("touch",onTouchO)	

			elseif (tabellaStringa[k] == 'X'and grid[riga][colonna] == nil ) then
				piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
				piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
				piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
				piece.xScale = 0.95
				piece.yScale = 0.90
				piece:setFillColor( 0.90, 0.22, 0.22)
				piece.MyID = j
				piece.What = "Orizz2"
				piece.Red = "Red"
				piece.fill = paintRosso
				grid[riga][colonna] = piece
				grid[riga][colonna+1] = grid[riga][colonna]
				objects[j] = piece
				piece.isToucheEnable = true
				Rossa = j
				j = j+1
				piece:addEventListener("touch",onTouchOX)				

		end
		k = k+1
	end
end
	
--local buttunPause = display.newRect(GruppoScenaBase,display.contentCenterX, 50+display.contentCenterY+ checkerBoard.height/2,50,50)






function Refresh(event)
	local opzioni = {params = {id = numLivello}}
	--local corrente = composer.getSceneName( "current" )
	AroundBack:removeEventListener( "tap", MoveUndo )
	buttonRestart:removeEventListener( "tap", Refresh )
	--Runtime:removeEventListener("enterFrame", enterFrame)
	--composer.removeScene( corrente )
	composer.gotoScene( "reload", opzioni )
end



AroundMenu:addEventListener( "tap", Pause )
AroundBack:addEventListener( "tap", MoveUndo)
buttonRestart:addEventListener( "tap", Refresh)



end ---scene:create


function TrovaDifficoltaSuccessiva(DifficoltaCorrente)
	local ProssimaDifficolta 
	if DifficoltaCorrente == "Beginner" then
		ProssimaDifficolta = "Intermediate" 
	elseif DifficoltaCorrente == "Intermediate" then
		ProssimaDifficolta = "Advanced"
	end
	return ProssimaDifficolta
end

function DentroEnterFrame()
			
			if MyData.settings.levels.Difficolta[tonumber(numLivello)].stars == 0 and tonumber(numLivello) <4 then  --Se non è stata gia risolta e non è l'ultima
				MyData.settings.unlockedLevels.Difficolta = MyData.settings.unlockedLevels.Difficolta + 1			--allora sblocca la board successiva
			end
			if tonumber(numLivello) == 4 then
				SbloccatoLivelloSuccessivo = true
			end

			if tostring( score ) == minMosse then
				MyData.settings.levels.Difficolta[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) < 5 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Difficolta[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) >= 5 then
				MyData.settings.levels.Difficolta[tonumber(numLivello)].stars = 1
			end
			
			utility.saveTable(MyData.settings, "settings8.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {LivelloSuccessivoSbloccato = SbloccatoLivelloSuccessivo, ScoreMin =minMosse, starToPrint = MyData.settings.levels[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score+1  }} )
		end

local function enterFrame(event)
	print("frameeeeee")
	if(TouchEnabled == false ) then
		Runtime:removeEventListener("enterFrame", enterFrame)
		timer.performWithDelay(200, DentroEnterFrame)
											 
	end
	 

	if score == 0 then
		buttonBack.isEnabled = false
		AroundBack.alpha = 0.7
	end

end

function scene:ProvaEnterFrame()
	Runtime:addEventListener("enterFrame",enterFrame)
end

function Pause(event)
	Runtime:removeEventListener("enterFrame", enterFrame)
	composer.showOverlay("OnPause", { isModal = true, effect = "slideRight", time = 700 , params = {level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height}} )
end

function scene:show(event)
	local phase = event.phase
	if ( phase == "will" ) then
		Runtime:addEventListener("enterFrame", enterFrame)
	elseif ( phase == "did" ) then
		
	end
end

function scene:hide( event )
	local phase = event.phase
	if ( phase == "will" ) then
		--audio.fadeOut( { time = 1000 })
	elseif ( phase == "did" ) then
		Runtime:removeEventListener("enterFrame", enterFrame)
			--print("HIDE: "..hide)
	end
end

function scene:destroy( event )
end


scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene




















 







--
-- Calculate some values
--



--
-- Using positions 1, 8 for X and Y, draw the object at the right place in the grid
--


			




--
-- Generate a few objects
--
--


	--else
	--	local xPos = math.random( 1, 6)
	--	local yPos = math.random( 1, 6 )
	--	flagOrizz=false
	--	spawnPiece(xPos, yPos,flagOrizz)
--	end
 

--timer.performWithDelay(200, function() movePiece( lastObject, 4, 5); end, 1)