local composer = require( "composer" )
local widget = require "widget"
local utility = require "loadsave"
local MyData = require "mydata"

local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 
 
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
 
    local sceneGroup = self.view

    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        displayObject:scale( XscaleFactor, YscaleFactor )
    end

    local background = display.newImage("Large.jpg")
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    fitImage(background,display.actualContentWidth,display.actualContentHeight)
    sceneGroup:insert( background )

    local Titolo = display.newImage( "Titolo.png")
    Titolo.x = display.contentCenterX
    Titolo.y = display.screenOriginY +100
    fitImage(Titolo,display.actualContentWidth-70,90)
    sceneGroup:insert( Titolo )

    
    local Play = display.newImage( "Play.png")
    Play.x = display.contentCenterX
    Play.y = Titolo.x+100
    fitImage(Play,160,30)
    sceneGroup:insert( Play )

    local Boards = display.newImage( "BoardsHome.png" )
    Boards.x = display.contentCenterX
    Boards.y = Play.y+60
    fitImage(Boards,160,30)
    sceneGroup:insert( Boards )
    
    local SettingsHome = display.newImage( "SettingsHome.png" )
    SettingsHome.x = display.contentCenterX
    SettingsHome.y = Boards.y+60
    fitImage(SettingsHome,160,30)
    sceneGroup:insert( SettingsHome )

    local fromBoard = false
    local fromPlay = false

    local function HandlerGoPlay(event)
        MyData.settings.Play = true
        MyData.settings.Board = false
        utility.saveTable(MyData.settings, "settings9.json")
        composer.gotoScene( "SelectionMod",{time = 700,effect = "fromRight",params = {fromPlay = true, fromBoard = false}})
    end 

    local function HandlerGoBoards(event)
        MyData.settings.Play = false
        MyData.settings.Board = true
        utility.saveTable(MyData.settings, "settings9.json")
        composer.gotoScene( "SelectionMod",{effect = "slideLeft", params = {fromPlay = false, fromBoard = true}} )
        
    end 

    
    Play:addEventListener( "tap", HandlerGoPlay )
    Boards:addEventListener( "tap", HandlerGoBoards )



end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        MyData.settings = utility.loadTable("settings9.json")
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene