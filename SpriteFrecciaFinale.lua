--
-- created with TexturePacker - https://www.codeandweb.com/texturepacker
--
-- $TexturePacker:SmartUpdate:33a18ef223f45f710bb7761bf4e2a34f:e554973b2dc4bb7e4d16efe3254c485e:a017e400b1b8961b1970f698fa5c8752$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 1
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 2
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 5,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 3
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 9,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 4
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 11,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 5
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 16,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 6
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 18,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 7
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 22,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 8
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 25,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 9
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 29,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 10
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 32,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 11
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 35,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 12
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 38,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 13
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 43,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 14
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 45,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 15
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 49,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 16
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 52,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 17
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 56,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 18
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 61,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 19
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 64,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 20
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 68,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 21
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 72,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 22
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 76,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 23
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 79,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 24
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 84,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 25
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 88,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 26
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 91,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 27
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 96,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 28
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 100,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 29
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 103,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 30
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 108,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 31
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 111,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 32
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 111,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 33
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 116,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 34
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 120,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 35
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 123,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 36
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 127,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 37
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 131,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 38
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 135,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 39
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 139,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 40
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 143,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 41
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 147,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 42
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 150,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 43
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 155,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 44
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 159,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 45
            x=1,
            y=1,
            width=77,
            height=74,

            sourceX = 162,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 46
            x=80,
            y=1,
            width=77,
            height=74,

            sourceX = 167,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 47
            x=159,
            y=1,
            width=77,
            height=74,

            sourceX = 170,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 48
            x=1,
            y=77,
            width=77,
            height=74,

            sourceX = 175,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 49
            x=80,
            y=77,
            width=77,
            height=74,

            sourceX = 179,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 50
            x=159,
            y=77,
            width=77,
            height=74,

            sourceX = 182,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 51
            x=1,
            y=153,
            width=77,
            height=74,

            sourceX = 187,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 52
            x=80,
            y=153,
            width=77,
            height=74,

            sourceX = 194,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 53
            x=159,
            y=153,
            width=77,
            height=74,

            sourceX = 198,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 54
            x=1,
            y=229,
            width=77,
            height=74,

            sourceX = 202,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 55
            x=80,
            y=229,
            width=77,
            height=74,

            sourceX = 209,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 56
            x=159,
            y=229,
            width=77,
            height=74,

            sourceX = 214,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 57
            x=1,
            y=305,
            width=77,
            height=74,

            sourceX = 218,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 58
            x=80,
            y=305,
            width=75,
            height=74,

            sourceX = 220,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
        {
            -- 59
            x=157,
            y=305,
            width=75,
            height=74,

            sourceX = 220,
            sourceY = 0,
            sourceWidth = 295,
            sourceHeight = 74
        },
    },

    sheetContentWidth = 237,
    sheetContentHeight = 380
}

SheetInfo.frameIndex =
{

    ["1"] = 1,
    ["2"] = 2,
    ["3"] = 3,
    ["4"] = 4,
    ["5"] = 5,
    ["6"] = 6,
    ["7"] = 7,
    ["8"] = 8,
    ["9"] = 9,
    ["10"] = 10,
    ["11"] = 11,
    ["12"] = 12,
    ["13"] = 13,
    ["14"] = 14,
    ["15"] = 15,
    ["16"] = 16,
    ["17"] = 17,
    ["18"] = 18,
    ["19"] = 19,
    ["20"] = 20,
    ["21"] = 21,
    ["22"] = 22,
    ["23"] = 23,
    ["24"] = 24,
    ["25"] = 25,
    ["26"] = 26,
    ["27"] = 27,
    ["28"] = 28,
    ["29"] = 29,
    ["30"] = 30,
    ["31"] = 31,
    ["32"] = 32,
    ["33"] = 33,
    ["34"] = 34,
    ["35"] = 35,
    ["36"] = 36,
    ["37"] = 37,
    ["38"] = 38,
    ["39"] = 39,
    ["40"] = 40,
    ["41"] = 41,
    ["42"] = 42,
    ["43"] = 43,
    ["44"] = 44,
    ["45"] = 45,
    ["46"] = 46,
    ["47"] = 47,
    ["48"] = 48,
    ["49"] = 49,
    ["50"] = 50,
    ["51"] = 51,
    ["52"] = 52,
    ["53"] = 53,
    ["54"] = 54,
    ["55"] = 55,
    ["56"] = 56,
    ["57"] = 57,
    ["58"] = 58,
    ["59"] = 59,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
