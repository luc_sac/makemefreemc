local composer = require( "composer" )

-- Vars/Objects local to scene
local scene = composer.newScene()
local current = composer.getSceneName( "current" )
local prevScene = composer.getSceneName( "previous" )
--local prevPrevScene = composer.getSceneName(composer.getSceneName( "previous" ))   
print("Refrsh scena Precedente :",prevScene )

function scene:show( event )
  local phase = event.phase
  local options = { params = event.params}
  if ( phase == "will" ) then
    composer.removeScene(prevScene)
  elseif ( phase == "did" ) then
  	print("SHOW DID!!")
    composer.gotoScene(prevScene, options)
    --composer.removeScene(current)      
  end


end

function scene:destroy(event)
	--composer.removeScene( current,false )
end

scene:addEventListener( "destroy", scene )
scene:addEventListener( "show", scene )

return scene