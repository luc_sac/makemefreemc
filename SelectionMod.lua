local composer = require( "composer" )
local json = require "json"
local utility = require "loadsave"
local MyData = require "mydata"
local PrecedenteScena 
local scene = composer.newScene()
local GruppoRelax,BoardRelax,fromBoard,fromPlay

 
-- create()
function scene:create( event )
    local sceneGroup = self.view

    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        displayObject:scale( XscaleFactor, YscaleFactor )
    end
    
    GruppoRelax = display.newGroup( )
    GruppoSfida = display.newGroup()

    local BG = display.newImage("Large.jpg")
    BG.x = display.contentCenterX
    BG.y = display.contentCenterY
    fitImage(BG,display.actualContentWidth,display.actualContentHeight)
    sceneGroup:insert( BG) 
    
    local Header = display.newImage("HeaderSelectionLevel.png" ) 
    Header.x = display.contentCenterX
    Header.y = display.safeScreenOriginY+30
    fitImage(Header,display.actualContentWidth,60)
    sceneGroup:insert( Header )

    local TestoHeader = display.newText( sceneGroup, "Mode Selection", display.contentCenterX, Header.y, "VAG.ttf",34 )
    
--[[
    local RettangoloProva1 = display.newRect( sceneGroup, display.contentCenterX-70, display.contentCenterY, display.actualContentWidth/2.5, 250 )
    local RettangoloProva2 = display.newRect( sceneGroup, display.contentCenterX+70, display.contentCenterY, display.actualContentWidth/2.5, 250 )
--]]
    BoardRelax = display.newImage("SelezionaModBoard.png")
    BoardRelax.x = display.contentCenterX-70 
    BoardRelax.y = display.contentCenterY
    fitImage(BoardRelax,display.actualContentWidth/2.5, 250)
    GruppoRelax:insert( BoardRelax )


    local coffe = display.newImage( "coffee.png" )
    coffe.x = BoardRelax.x+5
    coffe.y = BoardRelax.y-10
    fitImage(coffe,90,90)
    GruppoRelax:insert( coffe )

    local Text1RelaxMode = display.newText( GruppoRelax, "Relaxing", BoardRelax.x, BoardRelax.contentBounds.yMin + 20 , "VAG.ttf",25 )
    local Text2RelaxMode = display.newText( GruppoRelax, "Mode", BoardRelax.x, BoardRelax.contentBounds.yMin + 40, "VAG.ttf",18 )
    local Text3RelaxMode = display.newText( GruppoRelax, "Relax and solve", BoardRelax.x, BoardRelax.contentBounds.yMax -78, "VAG.ttf",14 )
    local Text4RelaxMode = display.newText( GruppoRelax, "boards without", BoardRelax.x, BoardRelax.contentBounds.yMax -60, "VAG.ttf",14 )
    local Text5RelaxMode = display.newText( GruppoRelax, "any pressure", BoardRelax.x, BoardRelax.contentBounds.yMax -40, "VAG.ttf",14 )
 
    local BoardSfida =  display.newImage("SelezionaModBoard.png")
    BoardSfida.x = display.contentCenterX+70 
    BoardSfida.y = display.contentCenterY
    fitImage(BoardSfida,display.actualContentWidth/2.5, 250)
    GruppoSfida:insert( BoardSfida )

    local Text1SfidaMode = display.newText( GruppoSfida, "Challenge", BoardSfida.x, BoardSfida.contentBounds.yMin + 20 , "VAG.ttf",25 )
    local Text2SidaMode = display.newText( GruppoSfida, "Mode", BoardSfida.x, BoardRelax.contentBounds.yMin + 40, "VAG.ttf",18 )
    local Text3RelaxMode = display.newText( GruppoSfida, "See the minimum", BoardSfida.x, BoardRelax.contentBounds.yMax -80, "VAG.ttf",14 )
    local Text4RelaxMode = display.newText( GruppoSfida, "number of moves", BoardSfida.x, BoardRelax.contentBounds.yMax -60, "VAG.ttf",14 )
    local Text5RelaxMode = display.newText( GruppoSfida, "and compare it", BoardSfida.x, BoardRelax.contentBounds.yMax -40, "VAG.ttf",14 ) 
    local Text6RelaxMode = display.newText( GruppoSfida, "with yours", BoardSfida.x, BoardRelax.contentBounds.yMax -20, "VAG.ttf",14 ) 
    local CoppaSfida = display.newImage( "ModalitaSfida.png" )
    CoppaSfida.x = display.contentCenterX+70
    CoppaSfida.y = display.contentCenterY-10
    fitImage(CoppaSfida,90,90)
    GruppoSfida:insert( CoppaSfida )

    local OffScreenAncora = display.newRect( sceneGroup, BoardRelax.x, BoardRelax.y, 128, 250 )
    OffScreenAncora.alpha = 0

    sceneGroup:insert(GruppoRelax)
    sceneGroup:insert( GruppoSfida )
    
    local TurnBack = display.newImage("TurnBack.png" )
    TurnBack.x = 20
    TurnBack.y = Header.y
    fitImage(TurnBack,25,25)
    sceneGroup:insert( TurnBack )

    function TurnBackListener(event) 
        composer.gotoScene( "Home", {effect = "slideRight"} )
    end

    function OnTapSfida()
        transition.to( GruppoSfida, {time = 100 , x = 5} )
        timer.performWithDelay( 100, function () transition.to(GruppoSfida , {time = 100, x = 0}) end ) 
        if MyData.settings.Play and not MyData.settings.Board then
            MyData.settings.currentMode = "Sfida"
            utility.saveTable(MyData.settings, "settings9.json")
            timer.performWithDelay(200,function() composer.gotoScene( "gameBeginner" , {params={ id = tonumber(MyData.settings.currentBoardSfida)  }, effect = "fade"})  end)
        elseif MyData.settings.Board and not MyData.settings.Play  then
            MyData.settings.currentMode = "Sfida"
            utility.saveTable(MyData.settings, "settings9.json")
            timer.performWithDelay(200,function() composer.gotoScene( "level_selection", {effect = "slideLeft"}) end)
        end
    end

    function OnTapRelax( event)
            transition.to( GruppoRelax, {time = 100 , x = -5} )
            timer.performWithDelay( 100, function () transition.to(GruppoRelax , {time = 100, x = 0}) end )
            if MyData.settings.Play and not MyData.settings.Board then
                MyData.settings.currentMode = "Relax"
                utility.saveTable(MyData.settings, "settings9.json")
                timer.performWithDelay(200,function() composer.gotoScene( "gameBeginner", { params = { id = tonumber(MyData.settings.currentBoardRelax)  }, effect = "fade"})  end)
            elseif MyData.settings.Board and not MyData.settings.Play then
                MyData.settings.currentMode = "Relax"
                utility.saveTable(MyData.settings, "settings9.json")
                timer.performWithDelay(200,function() composer.gotoScene( "level_selection", {effect = "slideLeft"}) end)
            end 
    end


    
    TurnBack:addEventListener( "tap", TurnBackListener )
    BoardRelax:addEventListener( "tap", OnTapRelax )
    BoardSfida:addEventListener( "tap", OnTapSfida )

end
 








 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
    if ( phase == "will" ) then
        --PrecedenteScena = composer.getSceneName( "previous" )
        MyData.settings = utility.loadTable("settings9.json")
        
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene