local composer = require( "composer" )
local widget = require( "widget" )
local json = require "json"
local utility = require "loadsave"
local MyData = require "mydata"
--local dreamlo = require("plugin.dreamlo")

local scene = composer.newScene()
local text,FlagAdvanced 
local PrecedenteScena = composer.getSceneName( "previous" )

function scene:create( event )
    print( "CREATO AllBoard")
    FlagAdvanced = false
    local sceneGroup = self.view
    local PaginaCorrente = 1

    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        displayObject:scale( XscaleFactor, YscaleFactor )
    end
    
    MyData.settings = utility.loadTable("settings9.json")

    local background = display.newImage("Large.jpg")
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    fitImage(background,display.actualContentWidth,display.actualContentHeight)
    sceneGroup:insert( background )
    
    local Header = display.newImage( "HeaderSelectionLevel.png" )
    Header.x = display.contentCenterX
    Header.y = display.safeScreenOriginY+30
    fitImage(Header,display.actualContentWidth,60)
    sceneGroup:insert( Header)
 

    local TurnBack = display.newImage("TurnBack.png" )
    TurnBack.x = 20
    TurnBack.y = Header.y
    fitImage(TurnBack,25,25)
    sceneGroup:insert( TurnBack )


    --dreamlo.int("rONkuibBNUSPB85bEEeIRwZYe8Xrh9WEaCQT2XfmhiPg", "rONkuibBNUSPB85bEEeIRwZYe8Xrh9WEaCQT2XfmhiPg", true)


    local levelSelectGroup = widget.newScrollView({
        x = display.contentCenterX,
        y = display.safeScreenOriginY+display.contentCenterY+20,
        width = display.actualContentWidth-20,
        height = 310,
        hideBackground = true,
        horizontalScrollDisabled = false,
        verticalScrollDisabled = true,
        isLocked = true

    })

    if display.actualContentHeight > 480 then
        levelSelectGroup.y = display.contentCenterY+20
    end
    
    local TestoSelezione = display.newText( sceneGroup, "Select Board", display.contentCenterX,Header.y +60,"VAG.ttf",40 )
    
    if (MyData.settings.currentMode == "Sfida" and MyData.settings.currentLivelloSfida == "Advanced") or (MyData.settings.currentMode == "Relax" and MyData.settings.currentLivelloRelax == "Advanced") then
        FlagAdvanced = true
    else 
        FlagAdvanced = false
    end
    
    local DotCopertura, Dot1,Dot2,Dot3,Dot4,Dot5,Dot6

    if not FlagAdvanced then
        DotCopertura = display.newCircle( sceneGroup, display.contentCenterX-50,levelSelectGroup.contentBounds.yMax+12, 8 )
        Dot1 = display.newCircle( sceneGroup, DotCopertura.x,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot2 = display.newCircle( sceneGroup, Dot1.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot3 = display.newCircle( sceneGroup, Dot2.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot4 = display.newCircle( sceneGroup, Dot3.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot5 = display.newCircle( sceneGroup, Dot4.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot6 = display.newCircle( sceneGroup, Dot5.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
    else
        DotCopertura = display.newCircle( sceneGroup, display.contentCenterX-40,levelSelectGroup.contentBounds.yMax+12, 8 )
        Dot1 = display.newCircle( sceneGroup, DotCopertura.x,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot2 = display.newCircle( sceneGroup, Dot1.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot3 = display.newCircle( sceneGroup, Dot2.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot4 = display.newCircle( sceneGroup, Dot3.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
        Dot5 = display.newCircle( sceneGroup, Dot4.x +20,levelSelectGroup.contentBounds.yMax+12, 4 )
    end

    function Trans()
        DotCopertura.alpha = 1
    end 

    function ControllaSliderDots(PaginaToGo)
        if PaginaToGo == 2 then
            transition.to( DotCopertura, {time = 350, onStart = function() DotCopertura.alpha = 0.01 end , onComplete = Trans , x = Dot2.x} )

        elseif PaginaToGo == 3 then
            transition.to( DotCopertura, {time = 350, onStart = function() DotCopertura.alpha = 0.01 end , onComplete = Trans , x = Dot3.x} )
        
        elseif PaginaToGo == 1 then
            transition.to( DotCopertura, {time = 350 ,onStart = function() DotCopertura.alpha = 0.01 end , onComplete = Trans , x = Dot1.x} )
        

         elseif PaginaToGo == 4 then
            transition.to( DotCopertura, {time = 350 ,onStart = function() DotCopertura.alpha = 0.01 end , onComplete = Trans , x = Dot4.x} )
        
        elseif PaginaToGo == 5 then
            transition.to( DotCopertura, {time = 350 ,onStart = function() DotCopertura.alpha = 0.01 end , onComplete = Trans , x = Dot5.x} )
        
        elseif PaginaToGo == 6 then
            transition.to( DotCopertura, {time = 350 ,onStart = function() DotCopertura.alpha = 0.01 end , onComplete = Trans , x = Dot6.x} )
        end

    end


    function BottoneSinistra( )
        if PaginaCorrente == 1 then
            --levelSelectGroup:scrollToPosition( {x = 0} )
            ControllaSliderDots(PaginaCorrente)
        
        elseif PaginaCorrente ==2 then
            levelSelectGroup:scrollToPosition( {x = 0} )
            ControllaSliderDots(PaginaCorrente-1)--trasla alla prox indietro
            PaginaCorrente = PaginaCorrente -1
        
        elseif PaginaCorrente ==3 then
            levelSelectGroup:scrollToPosition( {x = -levelSelectGroup.width} )
            ControllaSliderDots(PaginaCorrente-1)--trasla alla prox indietro
            PaginaCorrente = PaginaCorrente -1
        
        elseif PaginaCorrente ==4 then
            levelSelectGroup:scrollToPosition( {x = -2*levelSelectGroup.width} )
            ControllaSliderDots(PaginaCorrente-1)--trasla alla prox indietro
            PaginaCorrente = PaginaCorrente -1
        
        elseif PaginaCorrente ==5 then
            levelSelectGroup:scrollToPosition( {x = -3*levelSelectGroup.width} )
            ControllaSliderDots(PaginaCorrente-1)--trasla alla prox indietro
            PaginaCorrente = PaginaCorrente -1
        
        elseif PaginaCorrente ==6 then
            levelSelectGroup:scrollToPosition( {x = -4*levelSelectGroup.width} )
            ControllaSliderDots(PaginaCorrente-1)--trasla alla prox indietro
            PaginaCorrente = PaginaCorrente -1
        end

    end
    
    function BottoneDestra()
        if PaginaCorrente == 1 then
            levelSelectGroup:scrollToPosition( {x = -levelSelectGroup.width} )
            ControllaSliderDots(PaginaCorrente + 1)
            PaginaCorrente = PaginaCorrente +1
        elseif PaginaCorrente == 2 then
            levelSelectGroup:scrollToPosition( {x = -2*levelSelectGroup.width } )
            ControllaSliderDots(PaginaCorrente+1)
            PaginaCorrente = PaginaCorrente +1
        elseif PaginaCorrente == 3 then
            levelSelectGroup:scrollToPosition( {x = -3*levelSelectGroup.width } )
            ControllaSliderDots(PaginaCorrente+1)
            PaginaCorrente = PaginaCorrente +1
        elseif PaginaCorrente == 4 then
            levelSelectGroup:scrollToPosition( {x = -4*levelSelectGroup.width } )
            ControllaSliderDots(PaginaCorrente+1)
            PaginaCorrente = PaginaCorrente +1
        elseif PaginaCorrente == 5 then
            if not FlagAdvanced then
                levelSelectGroup:scrollToPosition( {x = -5*levelSelectGroup.width } )
                ControllaSliderDots(PaginaCorrente+1)
                PaginaCorrente = PaginaCorrente +1
            end
        elseif PaginaCorrente == 6 then
            ControllaSliderDots(PaginaCorrente)
        end
    end




    local LeftArrow = display.newImage( "TurnBack.png" )
    LeftArrow.x = display.contentCenterX-30
    LeftArrow.y = levelSelectGroup.contentBounds.yMax + (display.safeScreenOriginY+ display.actualContentHeight - levelSelectGroup.contentBounds.yMax)/2 
    fitImage(LeftArrow,25,25)
    sceneGroup:insert( LeftArrow )

    local AroundLeftArrow = display.newRect( sceneGroup, LeftArrow.x, LeftArrow.y, 40, 40 )
    AroundLeftArrow.alpha = 0.1

    local DestraArrow = display.newImage( "TurnBack.png" )
    DestraArrow.x = display.contentCenterX+30
    DestraArrow.y = levelSelectGroup.contentBounds.yMax + (display.safeScreenOriginY+ display.actualContentHeight - levelSelectGroup.contentBounds.yMax)/2
    DestraArrow.rotation = 180
    fitImage(DestraArrow,25,25)
    sceneGroup:insert( DestraArrow )

    local AroundDestraArrow = display.newRect(sceneGroup,DestraArrow.x,DestraArrow.y,40,40)
    AroundDestraArrow.alpha = 0.1





    local function VerifyError(event)
        if (not event.error) then
            print( "SENT" )
        else
            print( "EERROR" )
        end 
    end 

    if MyData.settings.currentMode == "Sfida" then

        if MyData.settings.currentLivelloSfida == "Beginner" then

            local function myButtonTapFunction(event)
                    composer.removeScene( "gameBeginner" )
                    MyData.settings.currentBoardSfida = event.target.id
                    utility.saveTable(MyData.settings,"settings9.json")
                     -- Go to the game scene
                    composer.gotoScene( "gameBeginner", { params = {id = tonumber(event.target.id) }, effect="crossFade", time=333 } )
                    composer.removeScene( "AllBoard",true)
                
            end
            
            local memory = 0
            for indiceCicla =1,64 do
                if MyData.settings.levels.Beginner[indiceCicla].stars > 0 then
                    memory = memory + MyData.settings.levels.Beginner[indiceCicla].stars
                end
            end
            local yCord ,xCord
            if (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2 < 5 then
                yCord = Header.y
                xCord = display.contentCenterX +90
            else
                yCord =TestoSelezione.contentBounds.yMax+ (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
                xCord = display.contentCenterX-15
            end
            local InfoTick = display.newImage( "STAR1.png")
            InfoTick.x = xCord
            InfoTick.y = yCord
            fitImage(InfoTick,25,25)
            sceneGroup:insert(InfoTick)
            local InfoBoard = display.newText( sceneGroup, tostring(memory).."/".."192", InfoTick.x+40, InfoTick.y+2, "VAG.ttf",16 )

            text = "Beginner"
            local gbOffsetX = levelSelectGroup.x - ( levelSelectGroup.width * levelSelectGroup.anchorX )
            --local line = display.newLine(gbOffsetX, 10, (levelSelectGroup.width/3)/2, 10)
            -- 'xOffset', 'yOffset' and 'cellCount' are used to position the buttons in the grid.
            local xOffset = gbOffsetX + (levelSelectGroup.width/3)/2
            local xOffset2 = xOffset +3*(levelSelectGroup.width/3)
            local xOffset3 = xOffset2 + 3*(levelSelectGroup.width/3)
            local xOffset4 = xOffset3 + 3*(levelSelectGroup.width/3)
            local xOffset5 = xOffset4 + 3*(levelSelectGroup.width/3)
            local xOffset6 = xOffset5 + 3*(levelSelectGroup.width/3)
            local yOffset = 44
            local cellCount = 1
            local cellCount2 = 1
            -- Define the array to hold the buttons
            local buttons = {}
            local TextButtonBoard = {}
            local TextButtonNumber = {}
            -- Read 'maxLevels' from the 'myData' table. Loop over them and generating one button for each.
           for i = 1, 64 do
                -- Create a button
                buttons[i] = widget.newButton({
                    id = tostring( i  ),
                    defaultFile = "Button.png",
                    overFile = "ButtonOver.png",
                    width = 314,
                    height = 229,
                    font = "VAG.ttf",
                    fontSize = 48,
                    labelColor = { default = { 1, 1, 1 }, over = { 0.5, 0.5, 0.5 } },
                    cornerRadius = 6,
                    labelYOffset = -15,

                })
                -- Position the button in the grid and add it to the scrollView
                buttons[i].x = xOffset-10
                buttons[i].y = yOffset
                TextButtonBoard[i] = display.newText("Board:",buttons[i].x -10, buttons[i].y-12, "VAG.ttf", 14 )
                TextButtonNumber[i] = display.newText(tostring(i),buttons[i].x+25 , buttons[i].y-15,"VAG.ttf",20 )
                buttons[i].xScale = 0.27
                buttons[i].yScale = 0.25
                levelSelectGroup:insert( buttons[i] )

                local Fall
                local ArrayFall = {}
                --MyData.settings.levels.Beginner[i].stars
                for indiceStar = 0,2 do
                    Fall = display.newImage( "Buco.png")
                    Fall.x = buttons[i].x-25+ (25*indiceStar)
                    Fall.y = buttons[i].y +10
                    fitImage(Fall,25,25)
                    ArrayFall[indiceStar] = Fall
                    levelSelectGroup:insert( Fall )
                    
                    
                end

                if MyData.settings.levels.Beginner[i].stars > 0 then
                    for indiceStarJson = 1,MyData.settings.levels.Beginner[i].stars do
                        local StarButton = display.newImage( "STAR1Button.png" )
                        StarButton. x = ArrayFall[indiceStarJson-1].x
                        StarButton.y = ArrayFall[indiceStarJson-1].y
                        fitImage(StarButton,17,17)
                        levelSelectGroup:insert( StarButton )
                    end
                end
                
                levelSelectGroup:insert( TextButtonBoard[i] )
                levelSelectGroup:insert( TextButtonNumber[i] )

                buttons[i]:addEventListener( "tap", myButtonTapFunction )


                xOffset = xOffset+(levelSelectGroup.width/3)
                cellCount2 = cellCount2 + 1
                cellCount = cellCount + 1
                if ( cellCount > 3 and i+1<= 13 ) then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = (gbOffsetX + (levelSelectGroup.width/3)/2)
                        yOffset = yOffset + 75
                    end
                elseif cellCount > 3 and i+1 <= 25 then
                    print( "SI" )
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset2
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 37 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset3
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 49 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset4
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 61 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset5
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 73 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset6
                        yOffset = yOffset + 75
                    end


               end 
           end

        elseif MyData.settings.currentLivelloSfida == "Intermediate" then
            local function myButtonTapFunction(event) 
                    composer.removeScene( "gameBeginner" )
                    MyData.settings.currentBoardSfida = event.target.id
                     -- Go to the game scene
                    composer.gotoScene( "gameBeginner", { params = {id = tonumber(event.target.id) }, effect="crossFade", time=333 } )
                    composer.removeScene( "AllBoard",true)
                
            end
            local memory = 0
            for indiceCicla =1,64 do
                if MyData.settings.levels.Intermediate[indiceCicla].stars > 0 then
                    memory = memory + MyData.settings.levels.Intermediate[indiceCicla].stars
                end
            end
            local yCord ,xCord
            if (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2 < 5 then
                yCord = Header.y
                xCord = display.contentCenterX +90
            else
                yCord =TestoSelezione.contentBounds.yMax+ (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
                xCord = display.contentCenterX-15
            end
            local InfoTick = display.newImage( "STAR1.png")
            InfoTick.x = xCord
            InfoTick.y = yCord
            fitImage(InfoTick,25,25)
            sceneGroup:insert(InfoTick)
            local InfoBoard = display.newText( sceneGroup, tostring(memory).."/".."192", InfoTick.x+40, InfoTick.y+2, "VAG.ttf",16 )

            text = "Intermediate"
            local gbOffsetX = levelSelectGroup.x - ( levelSelectGroup.width * levelSelectGroup.anchorX )
            --local line = display.newLine(gbOffsetX, 10, (levelSelectGroup.width/3)/2, 10)
            -- 'xOffset', 'yOffset' and 'cellCount' are used to position the buttons in the grid.
            local xOffset = gbOffsetX + (levelSelectGroup.width/3)/2
            local xOffset2 = xOffset +3*(levelSelectGroup.width/3)
            local xOffset3 = xOffset2 + 3*(levelSelectGroup.width/3)
            local xOffset4 = xOffset3 + 3*(levelSelectGroup.width/3)
            local xOffset5 = xOffset4 + 3*(levelSelectGroup.width/3)
            local xOffset6 = xOffset5 + 3*(levelSelectGroup.width/3)
            local yOffset = 44
            local cellCount = 1
            local cellCount2 = 1
            -- Define the array to hold the buttons
            local buttons = {}
            local TextButtonBoard = {}
            local TextButtonNumber = {}
            -- Read 'maxLevels' from the 'myData' table. Loop over them and generating one button for each.
           for i = 1, 64 do
                -- Create a button
                buttons[i] = widget.newButton({
                    id = tostring( i  ),
                    defaultFile = "Button.png",
                    overFile = "ButtonOver.png",
                    width = 314,
                    height = 229,
                    font = "VAG.ttf",
                    fontSize = 48,
                    labelColor = { default = { 1, 1, 1 }, over = { 0.5, 0.5, 0.5 } },
                    cornerRadius = 6,
                    labelYOffset = -15,

                })
                -- Position the button in the grid and add it to the scrollView
                buttons[i].x = xOffset-10
                buttons[i].y = yOffset
                TextButtonBoard[i] = display.newText("Board:",buttons[i].x -10, buttons[i].y-12, "VAG.ttf", 14 )
                TextButtonNumber[i] = display.newText(tostring(i),buttons[i].x+25 , buttons[i].y-15,"VAG.ttf",20 )
                buttons[i].xScale = 0.27
                buttons[i].yScale = 0.25
                levelSelectGroup:insert( buttons[i] )

                local Fall
                local ArrayFall = {}
                --MyData.settings.levels.Intermediate[i].stars
                for indiceStar = 0,2 do
                    Fall = display.newImage( "Buco.png")
                    Fall.x = buttons[i].x-25+ (25*indiceStar)
                    Fall.y = buttons[i].y +10
                    fitImage(Fall,25,25)
                    ArrayFall[indiceStar] = Fall
                    levelSelectGroup:insert( Fall )
                    
                    
                end

                if MyData.settings.levels.Intermediate[i].stars > 0 then
                    for indiceStarJson = 1,MyData.settings.levels.Intermediate[i].stars do
                        local StarButton = display.newImage( "STAR1Button.png" )
                        StarButton. x = ArrayFall[indiceStarJson-1].x
                        StarButton.y = ArrayFall[indiceStarJson-1].y
                        fitImage(StarButton,17,17)
                        levelSelectGroup:insert( StarButton )
                    end
                end
                
                levelSelectGroup:insert( TextButtonBoard[i] )
                levelSelectGroup:insert( TextButtonNumber[i] )

                buttons[i]:addEventListener( "tap", myButtonTapFunction )


                xOffset = xOffset+(levelSelectGroup.width/3)
                cellCount2 = cellCount2 + 1
                cellCount = cellCount + 1
                if ( cellCount > 3 and i+1<= 13 ) then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = (gbOffsetX + (levelSelectGroup.width/3)/2)
                        yOffset = yOffset + 75
                    end
                elseif cellCount > 3 and i+1 <= 25 then
                    print( "SI" )
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset2
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 37 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset3
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 49 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset4
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 61 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset5
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 73 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset6
                        yOffset = yOffset + 75
                    end


               end 
           end

        elseif MyData.settings.currentLivelloSfida == "Advanced" then
            local function myButtonTapFunction(event) 
                    composer.removeScene( "gameBeginner" )
                    MyData.settings.currentBoardSfida = event.target.id
                     -- Go to the game scene
                    composer.gotoScene( "gameBeginner", { params = {id = tonumber(event.target.id) }, effect="crossFade", time=333 } )
                    composer.removeScene( "AllBoard",true)
                
            end
            local memory = 0
            for indiceCicla =1,50 do
                if MyData.settings.levels.Advanced[indiceCicla].stars > 0 then
                    memory = memory + MyData.settings.levels.Advanced[indiceCicla].stars
                end
            end
            local yCord ,xCord
            if (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2 < 5 then
                yCord = Header.y
                xCord = display.contentCenterX +90
            else
                yCord =TestoSelezione.contentBounds.yMax+ (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
                xCord = display.contentCenterX-15
            end
            local InfoTick = display.newImage( "STAR1.png")
            InfoTick.x = xCord
            InfoTick.y = yCord
            fitImage(InfoTick,25,25)
            sceneGroup:insert(InfoTick)
            local InfoBoard = display.newText( sceneGroup, tostring(memory).."/".."150", InfoTick.x+40, InfoTick.y+2, "VAG.ttf",16 )

            text = "Advanced"
            local gbOffsetX = levelSelectGroup.x - ( levelSelectGroup.width * levelSelectGroup.anchorX )
            --local line = display.newLine(gbOffsetX, 10, (levelSelectGroup.width/3)/2, 10)
            -- 'xOffset', 'yOffset' and 'cellCount' are used to position the buttons in the grid.
            local xOffset = gbOffsetX + (levelSelectGroup.width/3)/2
            local xOffset2 = xOffset +3*(levelSelectGroup.width/3)
            local xOffset3 = xOffset2 + 3*(levelSelectGroup.width/3)
            local xOffset4 = xOffset3 + 3*(levelSelectGroup.width/3)
            local xOffset5 = xOffset4 + 3*(levelSelectGroup.width/3)
            local xOffset6 = xOffset5 + 3*(levelSelectGroup.width/3)
            local yOffset = 44
            local cellCount = 1
            local cellCount2 = 1
            -- Define the array to hold the buttons
            local buttons = {}
            local TextButtonBoard = {}
            local TextButtonNumber = {}
            -- Read 'maxLevels' from the 'myData' table. Loop over them and generating one button for each.
           for i = 1, 50 do
                -- Create a button
                buttons[i] = widget.newButton({
                    id = tostring( i  ),
                    defaultFile = "Button.png",
                    overFile = "ButtonOver.png",
                    width = 314,
                    height = 229,
                    font = "VAG.ttf",
                    fontSize = 48,
                    labelColor = { default = { 1, 1, 1 }, over = { 0.5, 0.5, 0.5 } },
                    cornerRadius = 6,
                    labelYOffset = -15,

                })
                -- Position the button in the grid and add it to the scrollView
                buttons[i].x = xOffset-10
                buttons[i].y = yOffset
                TextButtonBoard[i] = display.newText("Board:",buttons[i].x -10, buttons[i].y-12, "VAG.ttf", 14 )
                TextButtonNumber[i] = display.newText(tostring(i),buttons[i].x+25 , buttons[i].y-15,"VAG.ttf",20 )
                buttons[i].xScale = 0.27
                buttons[i].yScale = 0.25
                levelSelectGroup:insert( buttons[i] )

                local Fall
                local ArrayFall = {}
                --MyData.settings.levels.Advanced[i].stars
                for indiceStar = 0,2 do
                    Fall = display.newImage( "Buco.png")
                    Fall.x = buttons[i].x-25+ (25*indiceStar)
                    Fall.y = buttons[i].y +10
                    fitImage(Fall,25,25)
                    ArrayFall[indiceStar] = Fall
                    levelSelectGroup:insert( Fall )
                    
                    
                end

                if MyData.settings.levels.Advanced[i].stars > 0 then
                    for indiceStarJson = 1,MyData.settings.levels.Advanced[i].stars do
                        local StarButton = display.newImage( "STAR1Button.png" )
                        StarButton. x = ArrayFall[indiceStarJson-1].x
                        StarButton.y = ArrayFall[indiceStarJson-1].y
                        fitImage(StarButton,17,17)
                        levelSelectGroup:insert( StarButton )
                    end
                end
                
                levelSelectGroup:insert( TextButtonBoard[i] )
                levelSelectGroup:insert( TextButtonNumber[i] )

                buttons[i]:addEventListener( "tap", myButtonTapFunction )


                xOffset = xOffset+(levelSelectGroup.width/3)
                cellCount2 = cellCount2 + 1
                cellCount = cellCount + 1
                if ( cellCount > 3 and i+1<= 13 ) then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = (gbOffsetX + (levelSelectGroup.width/3)/2)
                        yOffset = yOffset + 75
                    end
                elseif cellCount > 3 and i+1 <= 25 then
                    print( "SI" )
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset2
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 37 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset3
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 49 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset4
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 61 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset5
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 73 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset6
                        yOffset = yOffset + 75
                    end


               end 
           end
        end

    elseif MyData.settings.currentMode == "Relax" then

        if MyData.settings.currentLivelloRelax == "Beginner" then

            local function myButtonTapFunction(event) 
                    composer.removeScene( "gameBeginner" )
                    MyData.settings.currentBoardRelax = event.target.id
                    utility.saveTable(MyData.settings,"settings9.json")
                     -- Go to the game scene
                    composer.gotoScene( "gameBeginner", { params = {id = tonumber(event.target.id) }, effect="crossFade", time=333 } )
                    composer.removeScene( "AllBoard",true)
            end
            local memory = 0
            for indiceCicla =1,64 do
                if MyData.settings.levels.Beginner[indiceCicla].score > 0 then
                memory = memory +1 
                end
            end
            local yCord ,xCord
            if (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2 < 5 then
                yCord = Header.y+3
                xCord = display.contentCenterX +90
            else
                yCord =TestoSelezione.contentBounds.yMax+ (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
                xCord = display.contentCenterX-15
            end

            local CC = TestoSelezione.contentBounds.yMax + (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
            local InfoTick = display.newImage( "Tick.png")
            InfoTick.x = xCord
            InfoTick.y = yCord
            fitImage(InfoTick,35,35)
            sceneGroup:insert(InfoTick)
            local InfoBoard = display.newText( sceneGroup, tostring(memory).."/".."64", InfoTick.x+30, InfoTick.y, "VAG.ttf",16 )

            text = "Beginner"
            local gbOffsetX = levelSelectGroup.x - ( levelSelectGroup.width * levelSelectGroup.anchorX )
            --local line = display.newLine(gbOffsetX, 10, (levelSelectGroup.width/3)/2, 10)
            -- 'xOffset', 'yOffset' and 'cellCount' are used to position the buttons in the grid.
            local xOffset = gbOffsetX + (levelSelectGroup.width/3)/2
            local xOffset2 = xOffset +3*(levelSelectGroup.width/3)
            local xOffset3 = xOffset2 + 3*(levelSelectGroup.width/3)
            local xOffset4 = xOffset3 + 3*(levelSelectGroup.width/3)
            local xOffset5 = xOffset4 + 3*(levelSelectGroup.width/3)
            local xOffset6 = xOffset5 + 3*(levelSelectGroup.width/3)
            local yOffset = 44
            local cellCount = 1
            local cellCount2 = 1
            -- Define the array to hold the buttons
            local buttons = {}
            local TextButtonBoard = {}
            local TextButtonNumber = {}
            -- Read 'maxLevels' from the 'myData' table. Loop over them and generating one button for each.
           for i = 1, 64 do
                -- Create a button
                buttons[i] = widget.newButton({
                    id = tostring( i  ),
                    defaultFile = "Button.png",
                    overFile = "ButtonOver.png",
                    width = 314,
                    height = 229,
                    font = "VAG.ttf",
                    fontSize = 48,
                    labelColor = { default = { 1, 1, 1 }, over = { 0.5, 0.5, 0.5 } },
                    cornerRadius = 6,
                    labelYOffset = -15,

                })
                -- Position the button in the grid and add it to the scrollView
                buttons[i].x = xOffset-10
                buttons[i].y = yOffset
                TextButtonBoard[i] = display.newText("Board:",buttons[i].x -10, buttons[i].y-12, "VAG.ttf", 14 )
                TextButtonNumber[i] = display.newText(tostring(i),buttons[i].x+25 , buttons[i].y-15,"VAG.ttf",20 )
                buttons[i].xScale = 0.27
                buttons[i].yScale = 0.25
                levelSelectGroup:insert( buttons[i] )

                local Fall = display.newImage( "Buco.png")
                Fall.x = buttons[i].x
                Fall.y = buttons[i].y +10
                fitImage(Fall,25,25)
                levelSelectGroup:insert( Fall )

                if MyData.settings.levels.Beginner[i].score > 0 then
                    local tickButton = display.newImage( "Tick.png" )
                    tickButton.x = Fall.x
                    tickButton.y = Fall.y+2
                    fitImage(tickButton,25,25)
                    levelSelectGroup:insert( tickButton )
                end
                
                levelSelectGroup:insert( TextButtonBoard[i] )
                levelSelectGroup:insert( TextButtonNumber[i] )

                buttons[i]:addEventListener( "tap", myButtonTapFunction )


                xOffset = xOffset+(levelSelectGroup.width/3)
                cellCount2 = cellCount2 + 1
                cellCount = cellCount + 1
                if ( cellCount > 3 and i+1<= 13 ) then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = (gbOffsetX + (levelSelectGroup.width/3)/2)
                        yOffset = yOffset + 75
                    end
                elseif cellCount > 3 and i+1 <= 25 then
                    print( "SI" )
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset2
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 37 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset3
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 49 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset4
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 61 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset5
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 73 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset6
                        yOffset = yOffset + 75
                    end


               end 
           end

        elseif MyData.settings.currentLivelloRelax == "Intermediate" then

            local function myButtonTapFunction(event) 
                    composer.removeScene( "gameBeginner" )
                    MyData.settings.currentBoardRelax = event.target.id
                    utility.saveTable(MyData.settings,"settings9.json")
                     -- Go to the game scene
                    composer.gotoScene( "gameBeginner", { params = {id = tonumber(event.target.id) }, effect="crossFade", time=333 } )
                    composer.removeScene( "AllBoard",true)
            end
            local memory = 0
            for indiceCicla =1,64 do
                if MyData.settings.levels.Intermediate[indiceCicla].score > 0 then
                memory = memory +1 
                end
            end
            local yCord ,xCord
            if (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2 < 5 then
                yCord = Header.y+3
                xCord = display.contentCenterX +90
            else
                yCord =TestoSelezione.contentBounds.yMax+ (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
                xCord = display.contentCenterX-15
            end

            local CC = TestoSelezione.contentBounds.yMax + (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
            local InfoTick = display.newImage( "Tick.png")
            InfoTick.x = xCord
            InfoTick.y = yCord
            fitImage(InfoTick,35,35)
            sceneGroup:insert(InfoTick)
            local InfoBoard = display.newText( sceneGroup, tostring(memory).."/".."64", InfoTick.x+30, InfoTick.y, "VAG.ttf",16 )

            text = "Intermediate"
            local gbOffsetX = levelSelectGroup.x - ( levelSelectGroup.width * levelSelectGroup.anchorX )
            --local line = display.newLine(gbOffsetX, 10, (levelSelectGroup.width/3)/2, 10)
            -- 'xOffset', 'yOffset' and 'cellCount' are used to position the buttons in the grid.
            local xOffset = gbOffsetX + (levelSelectGroup.width/3)/2
            local xOffset2 = xOffset +3*(levelSelectGroup.width/3)
            local xOffset3 = xOffset2 + 3*(levelSelectGroup.width/3)
            local xOffset4 = xOffset3 + 3*(levelSelectGroup.width/3)
            local xOffset5 = xOffset4 + 3*(levelSelectGroup.width/3)
            local xOffset6 = xOffset5 + 3*(levelSelectGroup.width/3)
            local yOffset = 44
            local cellCount = 1
            local cellCount2 = 1
            -- Define the array to hold the buttons
            local buttons = {}
            local TextButtonBoard = {}
            local TextButtonNumber = {}
            -- Read 'maxLevels' from the 'myData' table. Loop over them and generating one button for each.
           for i = 1, 64 do
                -- Create a button
                buttons[i] = widget.newButton({
                    id = tostring( i  ),
                    defaultFile = "Button.png",
                    overFile = "ButtonOver.png",
                    width = 314,
                    height = 229,
                    font = "VAG.ttf",
                    fontSize = 48,
                    labelColor = { default = { 1, 1, 1 }, over = { 0.5, 0.5, 0.5 } },
                    cornerRadius = 6,
                    labelYOffset = -15,

                })
                -- Position the button in the grid and add it to the scrollView
                buttons[i].x = xOffset-10
                buttons[i].y = yOffset
                TextButtonBoard[i] = display.newText("Board:",buttons[i].x -10, buttons[i].y-12, "VAG.ttf", 14 )
                TextButtonNumber[i] = display.newText(tostring(i),buttons[i].x+25 , buttons[i].y-15,"VAG.ttf",20 )
                buttons[i].xScale = 0.27
                buttons[i].yScale = 0.25
                levelSelectGroup:insert( buttons[i] )

                local Fall = display.newImage( "Buco.png")
                Fall.x = buttons[i].x
                Fall.y = buttons[i].y +10
                fitImage(Fall,25,25)
                levelSelectGroup:insert( Fall )

                if MyData.settings.levels.Intermediate[i].score > 0 then
                    local tickButton = display.newImage( "Tick.png" )
                    tickButton.x = Fall.x
                    tickButton.y = Fall.y+2
                    fitImage(tickButton,25,25)
                    levelSelectGroup:insert( tickButton )
                end
                
                levelSelectGroup:insert( TextButtonBoard[i] )
                levelSelectGroup:insert( TextButtonNumber[i] )

                buttons[i]:addEventListener( "tap", myButtonTapFunction )


                xOffset = xOffset+(levelSelectGroup.width/3)
                cellCount2 = cellCount2 + 1
                cellCount = cellCount + 1
                if ( cellCount > 3 and i+1<= 13 ) then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = (gbOffsetX + (levelSelectGroup.width/3)/2)
                        yOffset = yOffset + 75
                    end
                elseif cellCount > 3 and i+1 <= 25 then
                    print( "SI" )
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset2
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 37 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset3
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 49 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset4
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 61 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset5
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 73 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset6
                        yOffset = yOffset + 75
                    end


               end 
           end

        elseif MyData.settings.currentLivelloRelax == "Advanced" then

            local function myButtonTapFunction(event) 
                    composer.removeScene( "gameBeginner" )
                    MyData.settings.currentBoardRelax = event.target.id
                    utility.saveTable(MyData.settings,"settings9.json")
                     -- Go to the game scene
                    composer.gotoScene( "gameBeginner", { params = {id = tonumber(event.target.id) }, effect="crossFade", time=333 } )
                    composer.removeScene( "AllBoard",true)
            end
            local memory = 0
            for indiceCicla =1,50 do
                if MyData.settings.levels.Advanced[indiceCicla].score > 0 then
                memory = memory +1 
                end
            end
            local yCord ,xCord
            if (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2 < 5 then
                yCord = Header.y+3
                xCord = display.contentCenterX +90
            else
                yCord =TestoSelezione.contentBounds.yMax+ (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
                xCord = display.contentCenterX-15
            end

            local CC = TestoSelezione.contentBounds.yMax + (levelSelectGroup.contentBounds.yMin-TestoSelezione.contentBounds.yMax)/2
            local InfoTick = display.newImage( "Tick.png")
            InfoTick.x = xCord
            InfoTick.y = yCord
            fitImage(InfoTick,35,35)
            sceneGroup:insert(InfoTick)
            local InfoBoard = display.newText( sceneGroup, tostring(memory).."/".."50", InfoTick.x+30, InfoTick.y, "VAG.ttf",16 )

            text = "Advanced"
            local gbOffsetX = levelSelectGroup.x - ( levelSelectGroup.width * levelSelectGroup.anchorX )
            --local line = display.newLine(gbOffsetX, 10, (levelSelectGroup.width/3)/2, 10)
            -- 'xOffset', 'yOffset' and 'cellCount' are used to position the buttons in the grid.
            local xOffset = gbOffsetX + (levelSelectGroup.width/3)/2
            local xOffset2 = xOffset +3*(levelSelectGroup.width/3)
            local xOffset3 = xOffset2 + 3*(levelSelectGroup.width/3)
            local xOffset4 = xOffset3 + 3*(levelSelectGroup.width/3)
            local xOffset5 = xOffset4 + 3*(levelSelectGroup.width/3)
            local xOffset6 = xOffset5 + 3*(levelSelectGroup.width/3)
            local yOffset = 44
            local cellCount = 1
            local cellCount2 = 1
            -- Define the array to hold the buttons
            local buttons = {}
            local TextButtonBoard = {}
            local TextButtonNumber = {}
            -- Read 'maxLevels' from the 'myData' table. Loop over them and generating one button for each.
           for i = 1, 50 do
                -- Create a button
                buttons[i] = widget.newButton({
                    id = tostring( i  ),
                    defaultFile = "Button.png",
                    overFile = "ButtonOver.png",
                    width = 314,
                    height = 229,
                    font = "VAG.ttf",
                    fontSize = 48,
                    labelColor = { default = { 1, 1, 1 }, over = { 0.5, 0.5, 0.5 } },
                    cornerRadius = 6,
                    labelYOffset = -15,

                })
                -- Position the button in the grid and add it to the scrollView
                buttons[i].x = xOffset-10
                buttons[i].y = yOffset
                TextButtonBoard[i] = display.newText("Board:",buttons[i].x -10, buttons[i].y-12, "VAG.ttf", 14 )
                TextButtonNumber[i] = display.newText(tostring(i),buttons[i].x+25 , buttons[i].y-15,"VAG.ttf",20 )
                buttons[i].xScale = 0.27
                buttons[i].yScale = 0.25
                levelSelectGroup:insert( buttons[i] )

                local Fall = display.newImage( "Buco.png")
                Fall.x = buttons[i].x
                Fall.y = buttons[i].y +10
                fitImage(Fall,25,25)
                levelSelectGroup:insert( Fall )

                if MyData.settings.levels.Advanced[i].score > 0 then
                    local tickButton = display.newImage( "Tick.png" )
                    tickButton.x = Fall.x
                    tickButton.y = Fall.y+2
                    fitImage(tickButton,25,25)
                    levelSelectGroup:insert( tickButton )
                end
                
                levelSelectGroup:insert( TextButtonBoard[i] )
                levelSelectGroup:insert( TextButtonNumber[i] )

                buttons[i]:addEventListener( "tap", myButtonTapFunction )


                xOffset = xOffset+(levelSelectGroup.width/3)
                cellCount2 = cellCount2 + 1
                cellCount = cellCount + 1
                if ( cellCount > 3 and i+1<= 13 ) then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = (gbOffsetX + (levelSelectGroup.width/3)/2)
                        yOffset = yOffset + 75
                    end
                elseif cellCount > 3 and i+1 <= 25 then
                    print( "SI" )
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset2
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 37 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset3
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 49 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset4
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 61 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset5
                        yOffset = yOffset + 75
                    end
                elseif cellCount >3 and i+1 <= 73 then
                    if cellCount2 == 13 then
                        yOffset = 44
                        cellCount = 1
                        cellCount2 = 1
                    else
                        cellCount = 1
                        xOffset = xOffset6
                        yOffset = yOffset + 75
                    end


               end 
           end
        end
    end

    local TestoHeaderTreCasi = display.newText( sceneGroup, text, display.contentCenterX, Header.y, "VAG.ttf",34  )
    






    sceneGroup:insert( levelSelectGroup)

    LeftArrow:addEventListener( "tap", BottoneSinistra )
    DestraArrow:addEventListener( "tap", BottoneDestra )


    local function TurnBackListener(event)
        composer.gotoScene(PrecedenteScena,{time = 400,effect = "slideRight"})
        timer.performWithDelay( 400, function()composer.removeScene( "AllBoard",true )end ) 
    end




    TurnBack:addEventListener( "tap", TurnBackListener )


     function enterFrame(event )
        --ControllaSliderDots()
    end

    Runtime:addEventListener( "enterFrame", enterFrame )
end









 

 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene