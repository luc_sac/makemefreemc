--
-- created with TexturePacker - https://www.codeandweb.com/texturepacker
--
-- $TexturePacker:SmartUpdate:6c73da2cd55bb95bfddb29020e03f5ee:81b112da26124ed7666ff11445facd87:08cba1e171a6753f326da2b212312827$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 1
            x=0,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 2
            x=100,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 3
            x=200,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 4
            x=300,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 5
            x=400,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 6
            x=500,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 7
            x=600,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 8
            x=700,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 9
            x=800,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 10
            x=900,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 11
            x=1000,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 12
            x=1100,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 13
            x=1200,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 14
            x=1300,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 15
            x=1400,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 16
            x=1500,
            y=0,
            width=100,
            height=100,

        },
        {
            -- 17
            x=1600,
            y=0,
            width=100,
            height=100,

        },
    },

    sheetContentWidth = 1700,
    sheetContentHeight = 100
}

SheetInfo.frameIndex =
{

    ["1"] = 1,
    ["2"] = 2,
    ["3"] = 3,
    ["4"] = 4,
    ["5"] = 5,
    ["6"] = 6,
    ["7"] = 7,
    ["8"] = 8,
    ["9"] = 9,
    ["10"] = 10,
    ["11"] = 11,
    ["12"] = 12,
    ["13"] = 13,
    ["14"] = 14,
    ["15"] = 15,
    ["16"] = 16,
    ["17"] = 17,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
