local composer = require "composer"
local utility = require "loadsave"
local MyData = require "mydata"
local WinnerSoundEffect,FlagNonHint

print("sonoWin")
-- Variables local to scene
local scene = composer.newScene()

local WinTable,TextWin1,TextWin2,Restart,NextLevel,NextLevelText,TextWin3,TurnToMenu,TurnToMenuText,RestartText,TextWin4
local IntornoMenu,IntornoNext,IntornoRestart
local StelleDaStampare,Number,RecordPrecedente,ArraySpriteParentesiWinVero,ArraySpriteArrowVero

function scene:create( event )
  

    local sceneGroup = self.view 

    WinnerSoundEffect = audio.loadSound( "WinnerSound.wav")
    audio.play( WinnerSoundEffect )
    ArraySpriteParentesiWinVero = event.params.ArraySpriteParentesiWin
    ArraySpriteArrowVero = event.params.ArraySpriteArrowWin
    FlagNonHint = event.params.FlagNonAggHint

    for indiceCiclo1 = 1,#ArraySpriteParentesiWinVero do
        transition.cancel( ArraySpriteParentesiWinVero[indiceCiclo1])
    end
    
    for indiceCiclo2 = 1,#ArraySpriteArrowVero do
        transition.cancel( ArraySpriteArrowVero[indiceCiclo2])
    end
    childGroup = display.newGroup()

    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
      local YscaleFactor = fitHeight / displayObject.height 
      local XscaleFactor = fitWidth / displayObject.width 
      displayObject:scale( XscaleFactor, YscaleFactor )
    end
    local function fitImage2( displayObject, fitWidth, fitHeight, enlarge )
      local YscaleFactor = fitHeight / displayObject.height 
      local XscaleFactor = fitWidth / displayObject.width 
      return XscaleFactor
    end
    local function fitImage3( displayObject, fitWidth, fitHeight, enlarge )
      local YscaleFactor = fitHeight / displayObject.height 
      local XscaleFactor = fitWidth / displayObject.width 
      return YscaleFactor
    end

    function controlEOF(LastLevel)
        if tonumber(LastLevel) == 4 then
          return true
        else
          return false
        end
    end

  Number = event.params.level
  MyData.settings = utility.loadTable("settings9.json")
  
  GridWin = display.newRoundedRect(childGroup,display.contentCenterX,display.contentCenterY, event.params.checkWidth/1.5, event.params.checkHeight/1.5,6 )
  GridWin.alpha = 0
  
  local Oscure = display.newRect( childGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight )
  Oscure.alpha = 0.75
  Oscure:setFillColor( 0,0,0 )
  WinTable = display.newImage( "GridPauseTest.png")
  WinTable.x = display.contentCenterX
  WinTable.y = display.contentCenterY
  fitImage(WinTable,event.params.checkWidth-10, event.params.checkHeight/1.2)
  childGroup:insert( WinTable )


    


  function onTapTurnAllLevel(event)
      if not controlEOF(Number) then
        MyData.settings.currentBoardSfida = Number +1
      end
      utility.saveTable(MyData.settings,"settings9.json")
      composer.hideOverlay()
      composer.removeScene( "gameBeginner")
      composer.gotoScene( "Home",{effect = "zoomInOutFade", time = 500})
      return true
    end




    function OnTapRestart(event)
        utility.saveTable(MyData.settings,"settings9.json")
        composer.hideOverlay("slideLeft",500)
        composer.gotoScene( "refresh", { params = {id = Number } } )
        return true 
      end

    function OnTapNextLevelSfida(event)
      if controlEOF(Number) then
        if MyData.settings.currentLivelloSfida == "Beginner" then
          MyData.settings.currentLivelloSfida = "Intermediate" 
        elseif MyData.settings.currentLivelloSfida == "Intermediate" then
          MyData.settings.currentLivelloSfida = "Advanced" 
        elseif MyData.settings.currentLivelloSfida == "Advanced" then
          MyData.settings.currentLivelloSfida = "Beginner"
        end
        utility.saveTable(MyData.settings,"settings9.json")
        composer.hideOverlay("slideLeft",400)
        composer.gotoScene( "refresh", { params = {id = 1}})
      else
        utility.saveTable(MyData.settings,"settings9.json")
        composer.hideOverlay("slideLeft",400)
        composer.gotoScene( "refresh", { params = {id = (Number+1)}})
      end
      return true
    end

     function OnTapNextLevelRelax(event)
      if controlEOF(Number) then
        if MyData.settings.currentLivelloRelax == "Beginner" then
          MyData.settings.currentLivelloRelax = "Intermediate" 
        elseif MyData.settings.currentLivelloRelax == "Intermediate" then
          MyData.settings.currentLivelloRelax = "Advanced" 
        elseif MyData.settings.currentLivelloRelax == "Advanced" then
          MyData.settings.currentLivelloRelax = "Beginner"
        end
        MyData.settings.currentBoardRelax = 1
        utility.saveTable(MyData.settings,"settings9.json")
        composer.hideOverlay("slideLeft",400)
        composer.gotoScene( "refresh", { params = {id = 1}})
      else
        MyData.settings.currentBoardRelax = Number +1
        utility.saveTable(MyData.settings,"settings9.json")
        composer.hideOverlay("slideLeft",400)
        composer.gotoScene( "refresh", { params = {id = (Number+1)}})
      end
      return true
    end

  if MyData.settings.currentMode == "Sfida" then

    if MyData.settings.currentLivelloSfida == "Beginner" then
      RecordPrecedente = MyData.settings.levels.Beginner[event.params.level].Best
      if MyData.settings.levels.Beginner[event.params.level].Best == "--" then
        MyData.settings.levels.Beginner[event.params.level].Best = event.params.myScore
      elseif event.params.myScore < MyData.settings.levels.Beginner[event.params.level].Best then
        MyData.settings.levels.Beginner[event.params.level].Best = event.params.myScore
      end
    
    elseif MyData.settings.currentLivelloSfida == "Intermediate" then
      RecordPrecedente = MyData.settings.levels.Intermediate[event.params.level].Best
      if MyData.settings.levels.Intermediate[event.params.level].Best == "--" then
        MyData.settings.levels.Intermediate[event.params.level].Best = event.params.myScore
      elseif event.params.myScore < MyData.settings.levels.Intermediate[event.params.level].Best then
        MyData.settings.levels.Intermediate[event.params.level].Best = event.params.myScore
      end

    elseif MyData.settings.currentLivelloSfida == "Advanced" then
      RecordPrecedente = MyData.settings.levels.Advanced[event.params.level].Best
      if MyData.settings.levels.Advanced[event.params.level].Best == "--" then
        MyData.settings.levels.Advanced[event.params.level].Best = event.params.myScore
      elseif event.params.myScore < MyData.settings.levels.Advanced[event.params.level].Best then
        MyData.settings.levels.Advanced[event.params.level].Best = event.params.myScore
      end
    end

    
    ---AGGIUNGI ANIMAZIONE STELLE
    StelleDaStampare = event.params.starToPrint


    local gbOffsetX = GridWin.x - ( GridWin.width * GridWin.anchorX )
    local xOffset = gbOffsetX + (GridWin.width/4)
    local xOffset1 = gbOffsetX + (GridWin.width/4)

    for ki = 1,3 do
      local StarEmptyPNG = display.newImage( "StarEmpty.png" )
      fitImage(StarEmptyPNG,50,50)
      StarEmptyPNG.x = xOffset1
      StarEmptyPNG.y = GridWin.y -30
      childGroup:insert( StarEmptyPNG )
      xOffset1 = xOffset1+60
    end
    
    for i = 1,tonumber(StelleDaStampare) do
      local StrarOnWin = display.newImage( "STAR1.png" )
      StrarOnWin.alpha = 0
      StrarOnWin.x = xOffset 
      StrarOnWin.y = GridWin.y -20
      fitImage(StrarOnWin,200,200,true)
      transition.to( StrarOnWin, {onStart = function() StrarOnWin.alpha = 1 end, delay =250*(i-1) , time = 500, x = xOffset, y = GridWin.y -30, xScale = fitImage2(StrarOnWin,40,40),yScale = fitImage3(StrarOnWin,40,40)  } )
      xOffset = xOffset+60
      childGroup:insert( StrarOnWin ) 
    end


    
    --transition.to( GridWin, {x = display.contentCenterX, y = display.contentCenterY, time = 1000}  )
    if StelleDaStampare == 3 and not FlagNonHint then
      TextWin1 = display.newText(childGroup, "Perfect!", display.contentCenterX, display.contentCenterY - 96 , "VAG.ttf",19 )
      TextWin1:setFillColor(1,1,1 )
      --se in corso Sover devi soltanto non aggiungere e non mostrare lampada
      
      MyData.settings.hint = MyData.settings.hint + 1 
      TurnToMenu = display.newImage( "MenuIcon.png" )
      TurnToMenu.x = display.contentCenterX - 100
      TurnToMenu.y = display.contentCenterY+95
      fitImage(TurnToMenu,45,40)
      childGroup:insert( TurnToMenu )
      
      TurnToMenuText = display.newText( childGroup, "Menu", TurnToMenu.x+35, display.contentCenterY+95, "VAG.ttf",18 )
      IntornoMenu = display.newRect( childGroup, TurnToMenu.x+22, TurnToMenu.y, 70, 30 )
      IntornoMenu.alpha = 0.01

      local vertices = {0,-12,0,12,15,0 }
      NextLevel = display.newPolygon( childGroup, display.contentCenterX-20, TurnToMenu.y, vertices )
      NextLevel:setFillColor( 1,1,1 )
      NextLevelText = display.newText( childGroup,"Next", display.contentCenterX+8 , TurnToMenu.y, "VAG.ttf",18)
      IntornoNext = display.newRect( childGroup, NextLevel.x+18, TurnToMenu.y, 58, 30 )
      IntornoNext.alpha = 0.01

    
     
      TextWin2 = display.newText(childGroup,"You solved this board in "..event.params.myScore.." moves", display.contentCenterX, display.contentCenterY+8, "VAG.ttf",17)
      TextWin2:setFillColor(1,1,1 )

      TextWin3 = display.newText(childGroup,"Minimum moves: "..event.params.ScoreMin, display.contentCenterX , display.contentCenterY+33,"VAG.ttf",16)
      TextWin3:setFillColor(1,1,1 )

      TextWin4 = display.newText(childGroup,"Previus record: "..RecordPrecedente, display.contentCenterX-10 , display.contentCenterY+52,"VAG.ttf",16)
      TextWin4:setFillColor(1,1,1 )

      local TextWin5 = display.newText(childGroup,"Hint awarded: 1x " , display.contentCenterX-6 , display.contentCenterY+71,"VAG.ttf",16)
      TextWin4:setFillColor(1,1,1 )
      local inconaLampadina = display.newImage( "risolvi1.png")
      fitImage(inconaLampadina,25,25)
      inconaLampadina.x = TextWin5.x +70
      inconaLampadina.y = TextWin5.y-5
      childGroup:insert( inconaLampadina )



      Restart = display.newImage("back12.png" )
      Restart.x = NextLevelText.x + 43 
      Restart.y = NextLevelText.y
      fitImage(Restart,23,23)
      childGroup:insert( Restart )
      IntornoRestart = display.newRect( childGroup, Restart.x+28, TurnToMenu.y, 80, 30 )
      IntornoRestart.alpha = 0.01
      RestartText = display.newText( childGroup, "Restart", Restart.x+40, Restart.y, "VAG.ttf", 18 )

      IntornoNext:addEventListener( "tap", OnTapNextLevelSfida)
      IntornoRestart:addEventListener( "tap", OnTapRestart)
    else
      TextWin1 = display.newText(childGroup, "Good Work!", display.contentCenterX, display.contentCenterY - 96 , "VAG.ttf",18 )
      TextWin1:setFillColor(1,1,1 )
      TurnToMenu = display.newImage( "MenuIcon.png" )
      TurnToMenu.x = display.contentCenterX - 100
      TurnToMenu.y = display.contentCenterY+85
      fitImage(TurnToMenu,45,40)
      childGroup:insert( TurnToMenu )
      
      TurnToMenuText = display.newText( childGroup, "Menu", TurnToMenu.x+35, display.contentCenterY+85, "VAG.ttf",18 )
      IntornoMenu = display.newRect( childGroup, TurnToMenu.x+22, TurnToMenu.y, 70, 30 )
      IntornoMenu.alpha = 0.01

      local vertices = {0,-12,0,12,15,0 }
      NextLevel = display.newPolygon( childGroup, display.contentCenterX-20, TurnToMenu.y, vertices )
      NextLevel:setFillColor( 1,1,1 )
      NextLevelText = display.newText( childGroup,"Next", display.contentCenterX+8 , TurnToMenu.y, "VAG.ttf",18)
      IntornoNext = display.newRect( childGroup, NextLevel.x+18, TurnToMenu.y, 58, 30 )
      IntornoNext.alpha = 0.01

    
     
      TextWin2 = display.newText(childGroup,"You solved this board in "..event.params.myScore.." moves", display.contentCenterX, display.contentCenterY+8, "VAG.ttf",17)
      TextWin2:setFillColor(1,1,1 )

      TextWin3 = display.newText(childGroup,"Minimum moves: "..event.params.ScoreMin, display.contentCenterX , display.contentCenterY+33,"VAG.ttf",16)
      TextWin3:setFillColor(1,1,1 )

      TextWin4 = display.newText(childGroup,"Previus record: "..RecordPrecedente, display.contentCenterX-10 , display.contentCenterY+52,"VAG.ttf",16)
      TextWin4:setFillColor(1,1,1 )



      Restart = display.newImage("back12.png" )
      Restart.x = NextLevelText.x + 43 
      Restart.y = NextLevelText.y
      fitImage(Restart,23,23)
      childGroup:insert( Restart )
      IntornoRestart = display.newRect( childGroup, Restart.x+28, TurnToMenu.y, 80, 30 )
      IntornoRestart.alpha = 0.01
      RestartText = display.newText( childGroup, "Restart", Restart.x+40, Restart.y, "VAG.ttf", 18 )

      IntornoNext:addEventListener( "tap", OnTapNextLevelSfida)
      IntornoRestart:addEventListener( "tap", OnTapRestart)
    end

  else

    TurnToMenu = display.newImage( "MenuIcon.png" )
    TurnToMenu.x = display.contentCenterX - 80
    TurnToMenu.y = display.contentCenterY+65
    fitImage(TurnToMenu,40,40)
    childGroup:insert( TurnToMenu )
    TextWin1 = display.newText(childGroup, "Good Work!", display.contentCenterX, display.contentCenterY - 96 , "VAG.ttf",18 )
    TextWin1:setFillColor(1,1,1 )
    TurnToMenuText = display.newText( childGroup, "Menu", TurnToMenu.x+35, display.contentCenterY+65, "VAG.ttf",18)
    IntornoMenu = display.newRect( childGroup, TurnToMenu.x+25, TurnToMenu.y, 70, 30 )
    IntornoMenu.alpha = 0.01

    local verticesRelax = {0,-12,0,12,18,0 }
    NextLevel = display.newPolygon( childGroup, display.contentCenterX+37, TurnToMenu.y, verticesRelax )
    NextLevel:setFillColor( 1,1,1 )
    NextLevelText = display.newText( childGroup,"Next", display.contentCenterX+67 , TurnToMenu.y, "VAG.ttf",18)
    IntornoNext = display.newRect( childGroup, NextLevel.x+20, TurnToMenu.y, 60, 30 )
    IntornoNext.alpha = 0.01
    TextWin2 =  display.newImage("SOLVEDNEW.png")
    TextWin2.x = display.contentCenterX
    TextWin2.y = display.contentCenterY
    TextWin2.rotation = -4
    fitImage(TextWin2,400,200,true)
    childGroup:insert(TextWin2)
    transition.to( TextWin2, {x = display.contentCenterX , y = display.contentCenterY, time = 500, xScale = fitImage2(TextWin2,170.6,58), yScale = fitImage3(TextWin2,170.6,58)} )
    --[[
    if controlEOF(Number) then
      if MyData.settings.currentLivelloRelax == "Beginner" then
        MyData.settings.currentLivelloRelax = "Intermediate"
      elseif MyData.settings.currentLivelloRelax == "Intermediate" then
        MyData.settings.currentLivelloRelax = "Advanced"
      elseif MyData.settings.currentLivelloRelax == "Advanced" then
        MyData.settings.currentLivelloRelax = "Beginner"
      end
      timer.performWithDelay( 2000, function() composer.hideOverlay("slideLeft",700) composer.gotoScene( "refresh", { params = {id = 1}} ) end )
      MyData.settings.currentBoardRelax = 1
      utility.saveTable(MyData.settings,"settings9.json")
    else
      timer.performWithDelay( 2000, function() composer.hideOverlay("slideLeft",700) composer.gotoScene( "refresh", { params = {id = (Number+1)}})end )
      MyData.settings.currentBoardRelax = Number +1
      utility.saveTable(MyData.settings,"settings9.json")
    end--]]
    IntornoNext:addEventListener( "tap", OnTapNextLevelRelax) 
end--if se relax o sfida

  

 
  IntornoMenu:addEventListener( "tap", onTapTurnAllLevel )


  sceneGroup:insert( childGroup )  
   
end



  local function enterFrame(event)
  end





function scene:show(event)
  local phase = event.phase
  if ( phase == "will" ) then
    Runtime:addEventListener("enterFrame", enterFrame)
  elseif ( phase == "did" ) then
    --print(event.params)
  end
end

function scene:hide( event )
  local phase = event.phase
  if ( phase == "will" ) then
    --audio.fadeOut( { time = 1000 })
  elseif ( phase == "did" ) then
    print("HIDE WINLUA")
    Runtime:removeEventListener("enterFrame", enterFrame)
      --print("HIDE: "..hide)
  end
end

function scene:destroy( event )
  --composer.removeScene( current,false )
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene