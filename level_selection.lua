local composer = require( "composer" )
local widget = require "widget"
local utility = require "loadsave"
local MyData = require "mydata"

local scene = composer.newScene()
local PrecedenteScena,PrecedentedelPrecedenteScena

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 
 
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
 
    local sceneGroup = self.view
    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        displayObject:scale( XscaleFactor, YscaleFactor )
    end

    local background = display.newImage("Large.jpg")
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    fitImage(background,display.actualContentWidth,display.actualContentHeight)
    sceneGroup:insert( background )

    local Header = display.newImage("HeaderSelectionLevel.png")
    Header.x = display.contentCenterX
    Header.y = display.safeScreenOriginY+30
    fitImage(Header,display.actualContentWidth,60)
    sceneGroup:insert( Header ) 

    local TextHeader = display.newText( sceneGroup, "Level Selection", display.contentCenterX, Header.y, "VAG.ttf",34 )

    local TurnBack = display.newImage("TurnBack.png" )
    TurnBack.x = 20
    TurnBack.y = Header.y
    fitImage(TurnBack,25,25)
    sceneGroup:insert( TurnBack )

    --[[local Titolo = display.newImage( "Titolo.png")
    Titolo.x = display.contentCenterX
    Titolo.y = display.contentCenterY+10
    fitImage(Titolo,display.actualContentWidth-100,80)--]]
    local IntornoNovice = display.newImage( "OnPauseResumeBot.png")
    IntornoNovice.x = display.contentCenterX
    IntornoNovice.y = display.actualContentHeight/4
    fitImage(IntornoNovice,227.5,42.6)
    IntornoNovice.id = "Beginner"
    sceneGroup:insert(IntornoNovice)

    local Novice = display.newImage("Novice.png")
    Novice.x = display.contentCenterX
    Novice.y = display.actualContentHeight/4
    fitImage(Novice,153,32.5)
    sceneGroup:insert(Novice)

    local TestoNovice = display.newText( sceneGroup, "Beginner",Novice.x+5, Novice.y , "VAG.ttf",18 )

    local IntornoIntermediate = display.newImage( "OnPauseResumeBot.png")
    IntornoIntermediate.x = display.contentCenterX
    IntornoIntermediate.y = display.actualContentHeight/4 + 70
    fitImage(IntornoIntermediate,227.5,42.6)
    IntornoIntermediate.id = "Intermediate"
    sceneGroup:insert(IntornoIntermediate)

    local Intermediate = display.newImage("Intermediate.png")
    Intermediate.x = display.contentCenterX
    Intermediate.y = display.actualContentHeight/4 + 70
    fitImage(Intermediate,153,32.5)
    sceneGroup:insert(Intermediate)

    local TestoIntemediate = display.newText( sceneGroup, "Intermediate", Intermediate.x+22, Intermediate.y,"VAG.ttf",18 )


    local IntornoAdvanced = display.newImage( "OnPauseResumeBot.png")
    IntornoAdvanced.x = display.contentCenterX
    IntornoAdvanced.y = display.actualContentHeight/4 + 140
    fitImage(IntornoAdvanced,227.5,42.6)
    IntornoAdvanced.id = "Advanced"
    sceneGroup:insert(IntornoAdvanced)


    local Advanced = display.newImage( "Difficult.png")
    Advanced.x = display.contentCenterX
    Advanced.y = Intermediate.y+70
    fitImage(Advanced,153,32.5)
    sceneGroup:insert( Advanced )

    local TestoAdvanced = display.newText( sceneGroup, "Advanced", Advanced.x +10 , Advanced.y , "VAG.ttf",18 )


    local function TurnBackListener() 
        composer.gotoScene( PrecedenteScena, {effect = "fade"} )
        return true
    end
    
    local function HandlerGo(event)
        if MyData.settings.currentMode == "Relax" then
            MyData.settings.currentLivelloRelax = event.target.id
        elseif MyData.settings.currentMode == "Sfida" then
            MyData.settings.currentLivelloSfida = event.target.id
        end
        utility.saveTable(MyData.settings,"settings9.json")
        composer.gotoScene( "AllBoard" ,{effect = "fade", params = {LevelDifficult = event.target.id}} )
    end 

    IntornoNovice:addEventListener( "tap", HandlerGo )
    IntornoIntermediate:addEventListener( "tap", HandlerGo  )
    IntornoAdvanced:addEventListener( "tap", HandlerGo  )
    TurnBack:addEventListener( "tap", TurnBackListener )
end
 
 
-- show()
function scene:show( event )
    print("SHOW LEVEL SELECTION")
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        PrecedenteScena = composer.getSceneName( "previous" )
        MyData.settings = utility.loadTable("settings9.json")
        print( PrecedenteScena )
        if PrecedenteScena == "AllBoard" and MyData.settings.Play and not MyData.settings.Board then
            print( "1" )
            PrecedenteScena = "gameBeginner"
        elseif PrecedenteScena == "gameBeginner" and MyData.settings.Board and not MyData.settings.Play then
            print( "2" )
            PrecedentedelPrecedenteScena = "gameBeginner"
        elseif PrecedenteScena == "SelectionMod" and MyData.settings.Board and not MyData.settings.Play then
            print( "3" )
            PrecedentedelPrecedenteScena = "SelectionMod"
        elseif PrecedenteScena == "AllBoard" and MyData.settings.Board and not MyData.settings.Play and PrecedentedelPrecedenteScena == "SelectionMod" then
            print( "4" )
            PrecedenteScena = "SelectionMod"
        elseif PrecedenteScena == "AllBoard" and MyData.settings.Board and not MyData.settings.Play and PrecedentedelPrecedenteScena == "gameBeginner" then
            PrecedenteScena = "gameBeginner"
        end

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene