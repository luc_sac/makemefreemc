local composer = require( "composer" )
local widget = require "widget"
 
local scene = composer.newScene()
local GridWinTapToFinish,WinTable,TextWin1 ,TextWin2,TextWin3,Warning,TextYES,TextNO,SoundEffectGenerale

local FlagYES 
 
-- create()
function scene:create( event )
    print("CREATO TAPTOFINISCH")
    SoundEffectGenerale = audio.loadSound( "ClickGenerale.wav")
    local sceneGroup = self.view
    FlagYES = false
    local function fitImage( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        displayObject:scale( XscaleFactor, YscaleFactor )
    end
    local function fitImage2( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        return XscaleFactor
    end
    local function fitImage3( displayObject, fitWidth, fitHeight, enlarge )
        local YscaleFactor = fitHeight / displayObject.height 
        local XscaleFactor = fitWidth / displayObject.width 
        return YscaleFactor
    end

    function ListenerYES(event)        
        FlagYES = true
        composer.hideOverlay("slideRight" )
    end
    function ListenerNO(event)
        composer.hideOverlay( "slideRight" )
    end


    GridWin = display.newRoundedRect(sceneGroup,display.contentCenterX,display.contentCenterY, event.params.checkWidth/1.5, event.params.checkHeight/1.5,6 )
    GridWin.alpha = 0
    local Oscure = display.newRect( sceneGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight )
    Oscure.alpha = 0.75
    Oscure:setFillColor( 0,0,0 )
    WinTable = display.newImage( "onSolvingGrid.png")
    WinTable.x = display.contentCenterX
    WinTable.y = display.contentCenterY
    fitImage(WinTable,event.params.checkWidth-10, event.params.checkHeight/1.2)
    sceneGroup:insert( WinTable )
    Warning = display.newImage( "Warning.png" )
    fitImage(Warning,70,70)
    sceneGroup:insert(Warning)
    Warning.x = display.contentCenterX
    Warning.y = GridWin.y -30
    TextWin1 = display.newText(sceneGroup,"Solving the puzzle", display.contentCenterX, display.contentCenterY - 96 , "VAG.ttf",20)
    TextWin1:setFillColor(1,1,1 )
    TextWin2 = display.newText(sceneGroup,"Are you sure you want to disable", display.contentCenterX-2, display.contentCenterY+20, "VAG.ttf",16)
    TextWin2:setFillColor(1,1,1 )
    TextWin3 = display.newText(sceneGroup,"the current hint ?", display.contentCenterX, display.contentCenterY+40, "VAG.ttf",16)
    TextWin3:setFillColor( 1,1,1 )
    
    local opzYes = {
        width = 151,
        height = 75,
        defaultFile = "YesButton.png",
        overFile = "YesButtonOver.png",
        onEvent = LanciaYes,
        isEnabled = true

    }
    local opzNo = {
        width = 151,
        height = 75,
        defaultFile = "NoButton.png",
        overFile = "NoButtonOver.png",
        onEvent = LanciaNo,
        isEnabled = true

    }
    TextYES = widget.newButton( opzYes )
    TextYES.x = display.contentCenterX-80
    TextYES.y = TextWin3.y+50
    fitImage(TextYES,40,20)
    sceneGroup:insert(TextYES)
    --TextYES = display.newText(sceneGroup,"Yes", display.contentCenterX-80, TextWin3.y+30, "VAG.ttf",20)
    --TextYES:setFillColor( 1,1,1 )
    TextNO = widget.newButton( opzNo )
    TextNO.x = display.contentCenterX+80
    TextNO.y = TextWin3.y+50
    fitImage(TextNO,40,20)
    sceneGroup:insert(TextNO)

    --[[TextNO = display.newText(sceneGroup,"No", display.contentCenterX+80, TextWin3.y+30, "VAG.ttf",20)
    TextNO:setFillColor( 1,1,1 )
    TextYES:addEventListener( "tap", ListenerYES )
    TextNO:addEventListener( "tap", ListenerNO )--]]
 
end
 
function LanciaYes(event)
    if event.phase == "ended" or event.phase == "cancelled" then
        audio.play( SoundEffectGenerale )
        ListenerYES()
    end 
end

function LanciaNo(event)
    if event.phase == "ended" or event.phase == "cancelled" then
        audio.play( SoundEffectGenerale )
        ListenerNO()
    end 
end

-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
    local parent = event.parent
 
    if ( phase == "will" ) then
        if FlagYES then
            parent:resumeAfterYes()
        end
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene