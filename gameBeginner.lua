-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
local composer = require "composer"
local widget  = require "widget"
local json = require "json" 
local utility = require "loadsave"
local MyData = require "mydata"
local FlagToEmptyTable
local li
local sheetO2 = require "SheetOrizzontale2"
local sheetO3 = require "SheetOrizzontale3"
local ArraySprite = {}
local ArraySpriteArrow = {}
local ContaOccorrenzeVERS2
local AttivaDisattivaTap = false
local BGperLoading
local TestoAccogli
local ContaOccorrenze 
local buttonPause, buttonResolve,testoDentroLampadina,buttonRestart
local FlagInCorsoSolver = false


local isRed, TouchEnabled, ScorePunt
local scene = composer.newScene()  
local GRID_WIDTH = 6
local GRID_HEIGHT = 6
--
print( "CAZOOOOOOOOOOOOOOOOOOOOOOOOO" )

local precedenteScena =  composer.getSceneName( "previous" )
 
local livello
local numLivello
local buttonBack
local AroundBack 
local minMosse,scoreBoard
local ScoreLevel
local RisoltoPrec
local checkerBoard 
local SoundEffect,SoundEffectGenerale


function scene:create(event)

local sceneGroup = self.view

print("PRECEDENTE====")
print( precedenteScena )


GruppoScenaBase = display.newGroup()
sceneGroup:insert(GruppoScenaBase )

--AUDIO SECTION 
SoundEffect = audio.loadSound( "TacSoundWood.wav") 
SoundEffectGenerale = audio.loadSound( "ClickGenerale.wav")




MyData.settings = utility.loadTable("settings9.json")


if event.params == nil then
	print( "siccome non ci sono..Primo" )
	numLivello = 1
else
	numLivello = event.params.id
end

--MyData.settings = utility.saveTable("settings9.json")

local function fitImage( displayObject, fitWidth, fitHeight, enlarge )

	local YscaleFactor = fitHeight / displayObject.height 
	local XscaleFactor = fitWidth / displayObject.width 
	displayObject:scale( XscaleFactor, YscaleFactor )
end





--getmetatable('').__index = function(str,a)  return string.sub(str,tonumber(a),tonumber(a)) end

local function at1 (stringa,a)  return string.sub(stringa,tonumber(a),tonumber(a)) end





Stack = {}

 grid = {}
for i = 1, 6 do
	grid[i] = {}
end

grid2 = {}
for i =1,6 do
		grid2[i]= {}
end



local GRID_WIDTH_PX = display.actualContentWidth-25
local GRID_HEIGHT_PX = GRID_WIDTH_PX

local CELL_WIDTH_V = GRID_WIDTH_PX/6
local CELL_HEIGHT_V = GRID_HEIGHT_PX/3

local CELL_WIDTH_O = GRID_WIDTH_PX/3
local CELL_HEIGHT_O = GRID_HEIGHT_PX/6

local CELL_WIDTH_V3 = GRID_WIDTH_PX/6
local CELL_HEIGHT_V3 = GRID_HEIGHT_PX/2

local CELL_WIDTH_O3 = GRID_WIDTH_PX/2
local CELL_HEIGHT_O3 = GRID_HEIGHT_PX/6

local background = display.newImageRect( GruppoScenaBase, "Leggerwood.png", display.actualContentWidth, display.actualContentHeight )
background.x = display.contentCenterX
background.y = display.contentCenterY


local RettangoloProva  = display.newImage("check_woodP.png")
RettangoloProva.x = display.contentCenterX+GRID_WIDTH_PX/2-10
RettangoloProva.y = display.contentCenterY 
fitImage(RettangoloProva,200,CELL_HEIGHT_O+10 , true)
GruppoScenaBase:insert(RettangoloProva)

local background = display.newImageRect( GruppoScenaBase, "Leggerwood.png", display.actualContentWidth, display.actualContentHeight )
background.x = display.contentCenterX-13
background.y = display.contentCenterY


local check  = display.newImage("check_wood2.png")
check.x = display.contentCenterX
check.y = display.contentCenterY
fitImage(check,GRID_WIDTH_PX, GRID_HEIGHT_PX)
GruppoScenaBase:insert(check)




 checkerBoard = display.newRoundedRect(GruppoScenaBase, display.contentCenterX,display.contentCenterY,GRID_WIDTH_PX, GRID_HEIGHT_PX,20)
 checkerBoard.alpha = 0


score = 0
local cacca = (6/10) *GRID_WIDTH_PX
local cacca2 = cacca/2
--print(display.screenOriginY)
--local line = display.newLine(display.contentCenterX,((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2),display.contentCenterX, (display.contentCenterY-(GRID_WIDTH_PX/2))-10)
--line:setStrokeColor(0,0,0 )
meta = (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2

--local ScoreLevel = display.newRoundedRect(GruppoScenaBase, display.contentCenterX -((GRID_WIDTH_PX/2)-cacca2) , (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2, cacca,((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2 ,6)
if MyData.settings.currentMode == "Sfida" then
	ScoreLevel  = display.newImage("BoardText1.png")
	ScoreLevel.x = display.contentCenterX -((GRID_WIDTH_PX/2)-cacca2)
	ScoreLevel.y = (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2
	fitImage(ScoreLevel,cacca,((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2,true)
	GruppoScenaBase:insert( ScoreLevel)
else
	ScoreLevel  = display.newImage("BoardText1.png")
	ScoreLevel.x = display.contentCenterX 
	ScoreLevel.y = (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2
	fitImage(ScoreLevel,checkerBoard.width,((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2,true)
	GruppoScenaBase:insert( ScoreLevel)
end

local opzioniNumeroBoxGrande = 
	{
		parent = GruppoScenaBase,
	    text = numLivello,     
	    x = ScoreLevel.x,
	    y = ScoreLevel.y+3,
	    font = "VAG.ttf",   
	    fontSize = (ScoreLevel.contentBounds.yMax -ScoreLevel.contentBounds.yMin)/2,
	    align = "center"  -- Alignment parameter
	}

local Text3 = display.newText(opzioniNumeroBoxGrande)
Text3:setFillColor( 1,1,1 )


local opzioniTestoBoxGrande = 
{
	parent = GruppoScenaBase,
    text = "Board:",     
    x = ScoreLevel.x,
    y = (ScoreLevel.y)-((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2)),
    font = "VAG.ttf",   
    fontSize = 20,
    align = "center"  -- Alignment parameter
}

local TextBoard = display.newText(opzioniTestoBoxGrande)
TextBoard:setFillColor( 1,1,1 )
TextBoard.y = TextBoard.y+(TextBoard.height)/2+4

local TestoPerOpzioniBoxGrandeSotto
if MyData.settings.currentMode == "Relax" then
	TestoPerOpzioniBoxGrandeSotto = MyData.settings.currentLivelloRelax
	if TestoPerOpzioniBoxGrandeSotto == "Beginner" then
		if MyData.settings.levels.Beginner[numLivello].score ~= 0 then
			RisoltoPrec = true
		else
			RisoltoPrec = false
		end
	elseif TestoPerOpzioniBoxGrandeSotto == "Advanced" then
		if MyData.settings.levels.Advanced[numLivello].score ~= 0 then
			RisoltoPrec = true
		else
			RisoltoPrec = false
		end  
	elseif TestoPerOpzioniBoxGrandeSotto == "Intermediate" then
		if MyData.settings.levels.Intermediate[numLivello].score ~= 0 then
			RisoltoPrec = true
		else
			RisoltoPrec = false
		end
	end
elseif MyData.settings.currentMode == "Sfida" then
	TestoPerOpzioniBoxGrandeSotto = MyData.settings.currentLivelloSfida
	if TestoPerOpzioniBoxGrandeSotto == "Beginner" then
		if MyData.settings.levels.Beginner[numLivello].score ~= 0 then
			RisoltoPrec = true
		else
			RisoltoPrec = false
		end
	elseif TestoPerOpzioniBoxGrandeSotto == "Advanced" then
		if MyData.settings.levels.Advanced[numLivello].score ~= 0 then
			RisoltoPrec = true
		else
			RisoltoPrec = false
		end  
	elseif TestoPerOpzioniBoxGrandeSotto == "Intermediate" then
		if MyData.settings.levels.Intermediate[numLivello].score ~= 0 then
			RisoltoPrec = true
		else
			RisoltoPrec = false
		end
	end
end

	local opzioniTestoBoxGrandeSotto = 
	{
		parent = GruppoScenaBase,
	    text = TestoPerOpzioniBoxGrandeSotto,     
	    x = ScoreLevel.x,
	    y =(ScoreLevel.y)+((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2)),
	    font = "VAG.ttf",   
	    fontSize = 14,
	    align = "center"  -- Alignment parameter
	}
	local TextBoardSotto = display.newText( opzioniTestoBoxGrandeSotto)
	TextBoardSotto.y = -4 +TextBoardSotto.y - (TextBoardSotto.height)/2


	if MyData.settings.currentMode == "Sfida" then
		MyData.settings.currentBoardSfida = event.params.id
		ScorePunt = display.newRoundedRect( GruppoScenaBase,display.contentCenterX+((GRID_WIDTH_PX)/3), (display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2, GRID_WIDTH_PX/3, ((display.contentCenterY-(GRID_WIDTH_PX/2) -10) - ((display.screenOriginY+ display.contentCenterY-(GRID_WIDTH_PX/2))/2))*2 , 6 )
		ScorePunt.alpha = 0

		local proca2 = display.newRect( GruppoScenaBase, ScorePunt.x, ScoreLevel.y, (ScorePunt.width/2), (ScorePunt.height/2) )
		proca2:setFillColor( 0,0,0 )
		proca2.alpha = 0

		local proca3 = display.newRect( GruppoScenaBase, ScorePunt.x, proca2.y-(proca2.height/2)-(proca2.height/4), (ScorePunt.width/2), (ScorePunt.height/6) )
		proca3:setFillColor( 0,0,0 )
		proca3.alpha = 0

		local YourScoreBox = display.newImage("BoardText1.png")
		YourScoreBox.x = ScorePunt.x
		YourScoreBox.y = ScorePunt.y
		fitImage(YourScoreBox,(ScorePunt.width),(ScorePunt.height))
		GruppoScenaBase:insert( YourScoreBox)

		options0 = {
			parent = GruppoScenaBase,
			text = score,
			x = ScorePunt.x,
			y = ScorePunt.y,
			--width = ScorePunt.width/2,
			--height = ScorePunt.height/2,
			font = "VAG.ttf",
			fontSize = (ScoreLevel.contentBounds.yMax -ScoreLevel.contentBounds.yMin)/2,
			align = "center" 
		}
		local Text2 = display.newText( GruppoScenaBase,"Your Score :", ScorePunt.x, TextBoard.y, "VAG.ttf",15 )
		Text2:setFillColor( 1,1,1 )

		scoreBoard = display.newText(options0)
		scoreBoard:setFillColor( 1,1,1 )
	else 
		MyData.settings.currentBoardRelax = event.params.id
	end

	local CerchioChange2
	local Change2
	local CerchioChange1
	local Change1


	if numLivello == 1 then
		CerchioChange2 = display.newImage( "ChangeBoardCircle.png" )
		CerchioChange2.x = ScoreLevel.contentBounds.xMax - 20
		CerchioChange2.y = ScoreLevel.y
		fitImage(CerchioChange2,25,25)
		GruppoScenaBase:insert(CerchioChange2)
		Change2 =  display.newImage( "TurnBack.png" )
		Change2.x = ScoreLevel.contentBounds.xMax - 20
		Change2.y = ScoreLevel.y
		Change2.rotation = 180
		fitImage(Change2,15,15)
		GruppoScenaBase:insert(Change2)
	elseif numLivello == 4 then
		CerchioChange1 = display.newImage( "ChangeBoardCircle.png" )
		CerchioChange1.x = ScoreLevel.contentBounds.xMin + 20
		CerchioChange1.y = ScoreLevel.y
		fitImage(CerchioChange1,25,25)
		GruppoScenaBase:insert(CerchioChange1)
		Change1 =  display.newImage( "TurnBack.png" )
		Change1.x = ScoreLevel.contentBounds.xMin + 20
		Change1.y = ScoreLevel.y
		fitImage(Change1,15,15)
		GruppoScenaBase:insert(Change1)
	elseif numLivello >1 or numLivello <4 then
		CerchioChange2 = display.newImage( "ChangeBoardCircle.png" )
		CerchioChange2.x = ScoreLevel.contentBounds.xMax - 20
		CerchioChange2.y = ScoreLevel.y
		fitImage(CerchioChange2,25,25)
		GruppoScenaBase:insert(CerchioChange2)
		Change2 =  display.newImage( "TurnBack.png" )
		Change2.x = ScoreLevel.contentBounds.xMax - 20
		Change2.y = ScoreLevel.y
		Change2.rotation = 180
		fitImage(Change2,15,15)
		GruppoScenaBase:insert(Change2)
		CerchioChange1 = display.newImage( "ChangeBoardCircle.png" )
		CerchioChange1.x = ScoreLevel.contentBounds.xMin + 20
		CerchioChange1.y = ScoreLevel.y
		fitImage(CerchioChange1,25,25)
		GruppoScenaBase:insert(CerchioChange1)
		Change1 =  display.newImage( "TurnBack.png" )
		Change1.x = ScoreLevel.contentBounds.xMin + 20
		Change1.y = ScoreLevel.y
		fitImage(Change1,15,15)
		GruppoScenaBase:insert(Change1)
	end

	




	local Risolto = display.newImage( "solved1.png" )
	Risolto.x = ScoreLevel.x 
	Risolto.y = ScoreLevel.y
	fitImage(Risolto,93.75,32.5)
	Risolto.alpha = 0.6
	GruppoScenaBase:insert(Risolto)






	local gbOffsetX = checkerBoard.x - ( checkerBoard.width * checkerBoard.anchorX ) 
	local gbOffsetY = checkerBoard.y - ( checkerBoard.height * checkerBoard.anchorY )




	function addScore()
		score = score+1
		if MyData.settings.currentMode == "Sfida" then
			scoreBoard.text = tostring(score)
		end
		return true
	end

	function subScore()
		score = score-1
		if MyData.settings.currentMode == "Sfida" then
			scoreBoard.text = tostring(score)
		end
	end


	function Stack:Create()

	  -- stack table
	  local t = {}
	  -- entry table
	  t._et = {}

	  -- push a value on to the stack
	  function t:push(...)
	    if ... then
	      local targs = {...}
	      -- add values
	      for _,v in ipairs(targs) do
	        table.insert(self._et, v)
	      end
	    end
	  end

	  -- pop a value from the stack
	  function t:pop(num)

	    -- get num values from stack
	    local num = num or 1

	    -- return table
	    local entries = {}

	    -- get values into entries
	    for i = 1, num do
	      -- get last entry
	      if #self._et ~= 0 then
	        table.insert(entries, self._et[#self._et])
	        -- remove last value
	        table.remove(self._et)
	      else
	        break
	      end
	    end
	    -- return unpacked entries
	    return unpack(entries)
	  end

	  -- get entries
	  function t:getn()
	    return #self._et
	  end

	  -- list values
	  function t:list()
	    for i,v in pairs(self._et) do
	      print(i, v)
	    end
	  end
	  return t
	end

	stack = Stack:Create()



	--[[AroundBack = display.newImage("PRIMO.png")
	AroundBack.x = (display.contentCenterX + (3/8)+ (checkerBoard.width/4 - (3/4))/2)
	AroundBack.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
	fitImage(AroundBack,checkerBoard.width/4 - (3/4), checkerBoard.width/6 - (1/6) )
	AroundBack:scale(0.96,0.8) 
	GruppoScenaBase:insert(AroundBack)--]]

	 
	--[[local AroundRestart = display.newImage("PRIMO.png")
	AroundRestart.x = (display.contentCenterX + (3/8)+ (checkerBoard.width/4 - (3/4))/2)  + (3/4)+ (checkerBoard.width/4 - (3/4))
	AroundRestart.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
	fitImage(AroundRestart,checkerBoard.width/4 - (3/4),checkerBoard.width/6 - (1/6))
	AroundRestart:scale(0.96,0.8)
	GruppoScenaBase:insert(AroundRestart)--]]

	--[[local AroundResolve = display.newImage( "PRIMOTEST.png")
	AroundResolve.x = display.contentCenterX-(3/8)- (checkerBoard.width/4 - (3/4))/2
	AroundResolve.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
	fitImage(AroundResolve,checkerBoard.width/4 - (3/4),checkerBoard.width/6 - (1/6))
	AroundResolve:scale(0.96,0.8)
	GruppoScenaBase:insert(AroundResolve)--]]

--[[AroundMenu = display.newImage( "PRIMO.png")
AroundMenu.x = AroundResolve.x - (1/4)-2*((checkerBoard.width/4 - (3/4))/2)
AroundMenu.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
fitImage(AroundMenu,checkerBoard.width/4 - (3/4), checkerBoard.width/6- (1/6))
AroundMenu:scale( 0.96, 0.8 )
GruppoScenaBase:insert(AroundMenu)--]]


	local options3 = {
		width = 207,
		height = 107,
		defaultFile = "BackDefault.png",
		overFile = "BackOver.png",
		onEvent = LanciaMoveUndo,
		isEnabled = false
		}
	local options4 = {
		width = 207,
		height = 107,
		defaultFile = "RefreshDefault.png",
		overFile = "RefreshOver.png",
		isEnabled = true,
		onEvent = LanciaRefresh
		}
	local options2 = {
		width = 207,
		height = 107,
		defaultFile = "LAMPADINA.png",
		overFile = "LAMPADINAOVER.png",
		isEnabled = true,
		onEvent = LanciaAccogli
		}
	local options1 = {
		width = 207,
		height = 107,
		defaultFile = "PauseDefault.png",
		overFile  = "PauseOver.png",
		isEnabled = true,
		onEvent = Pause
		}

	buttonBack = widget.newButton( options3 )
	buttonBack.x = display.contentCenterX + (3/8)+ (checkerBoard.width/4 - (3/4))/2
	buttonBack.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
	buttonBack.xScale = 0.35
	buttonBack.yScale = 0.379
	GruppoScenaBase:insert( buttonBack)

	buttonRestart = widget.newButton(options4)
	buttonRestart.x = (display.contentCenterX + (3/8)+ (checkerBoard.width/4 - (3/4))/2)  + (3/4)+ (checkerBoard.width/4 - (3/4))
	buttonRestart.y = buttonBack.y
	buttonRestart.xScale = 0.35
	buttonRestart.yScale = 0.379
	GruppoScenaBase:insert( buttonRestart)

	buttonResolve = widget.newButton( options2 )
	buttonResolve.x = (display.contentCenterX-(3/8)- (checkerBoard.width/4 - (3/4))/2)
	buttonResolve.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
	buttonResolve.xScale = 0.35
	buttonResolve.yScale = 0.379
	GruppoScenaBase:insert( buttonResolve)

	buttonPause = widget.newButton( options1 )
	buttonPause.x = (display.contentCenterX-(3/8)- (checkerBoard.width/4 - (3/4))/2) - (1/4)-2*((checkerBoard.width/4 - (3/4))/2)
	buttonPause.y = (meta-display.screenOriginY)+(checkerBoard.height/2 + display.contentCenterY)
	buttonPause.xScale = 0.35
	buttonPause.yScale = 0.379
	GruppoScenaBase:insert( buttonPause)

	testoDentroLampadina = display.newText( GruppoScenaBase, tostring(MyData.settings.hint), buttonResolve.x+1.5 , buttonResolve.y-2, "VAG.ttf",16 )
	testoDentroLampadina:setFillColor( 0,0,0 )

	if MyData.settings.hint == 0 then
		buttonResolve:setEnabled( false )
	end



	local paint = {
	    type = "image",
	    filename = "perfettoVert.jpg" ,
	}

	local paintO = {
	    type = "image",
	    filename = "perfettoOrizz.jpg" ,
	}

	local paintRosso = {
	    type = "image",
	    filename = "rosso.jpg" ,
	}





	objects = {}
	local StringaJson

	if MyData.settings.currentMode == "Sfida" then
		if MyData.settings.currentLivelloSfida == "Beginner" then
			StringaJson = "prova2/Beginner.json"
		elseif MyData.settings.currentLivelloSfida == "Intermediate" then
			StringaJson = "prova2/Intermediate.json"
		elseif MyData.settings.currentLivelloSfida == "Advanced" then
			StringaJson = "prova2/Advanced.json"
		end
	elseif MyData.settings.currentMode == "Relax" then
		if MyData.settings.currentLivelloRelax == "Beginner" then
			StringaJson = "prova2/Beginner.json"
		elseif MyData.settings.currentLivelloRelax == "Intermediate" then
			StringaJson = "prova2/Intermediate.json"
		elseif MyData.settings.currentLivelloRelax == "Advanced" then
			StringaJson = "prova2/Advanced.json"
		end
	end


	local filename = system.pathForFile( StringaJson, system.ResourceDirectory )
	local tabella  = json.decodeFile(filename)




	livello = "livello"..tostring(numLivello)


	local tabellaStringa = tabella[livello]["state"]
	minMosse = tabella[livello]["minMosse"]


	function Abilita()
		print("Abilitato!!")
		buttonBack:setEnabled( true )
		--AroundBack.alpha = 1
	end

	function Disabilita()
		print( "disabilitato!!" )
		buttonBack:setEnabled( false )
	end

	if MyData.settings.currentMode == "Sfida" then
		local TestoRecord
		if MyData.settings.currentLivelloSfida == "Beginner" then
			TestoRecord = display.newText( GruppoScenaBase, "Record: "..MyData.settings.levels.Beginner[tonumber(numLivello)].Best.."/"..minMosse, ScorePunt.x, ScorePunt.contentBounds.yMax-10 , "VAG.ttf",14 )
		elseif MyData.settings.currentLivelloSfida == "Intermediate" then
			TestoRecord = display.newText( GruppoScenaBase, "Record: "..MyData.settings.levels.Intermediate[tonumber(numLivello)].Best.."/"..minMosse, ScorePunt.x, ScorePunt.contentBounds.yMax-10 , "VAG.ttf",14 )
		elseif MyData.settings.currentLivelloSfida == "Advanced" then
			TestoRecord = display.newText( GruppoScenaBase, "Record: "..MyData.settings.levels.Advanced[tonumber(numLivello)].Best.."/"..minMosse, ScorePunt.x, ScorePunt.contentBounds.yMax-10 , "VAG.ttf",14 )
		end
	end




	function MoveUndo(event)
		local arrayMoveUndo = stack:pop(1)
		if arrayMoveUndo == nil then

			timer.performWithDelay(300, function ()buttonBack:setEnabled( false )end)
				--AroundBack.alpha = 0.75
		else
			buttonBack:setEnabled( true )
			if arrayMoveUndo[1].What == "Orizz2" or arrayMoveUndo[1].What == "Orizz3" then 
				transition.to(arrayMoveUndo[1], { time = 100 ,onStart = Disabilita, onComplete = function() timer.performWithDelay(250,Abilita) end , x = arrayMoveUndo[2]})
				subScore()
			elseif arrayMoveUndo[1].What == "Vert2" or arrayMoveUndo[1].What == "Vert3" then
				transition.to(arrayMoveUndo[1], { time = 100,onStart = Disabilita ,onComplete= function() timer.performWithDelay(250,Abilita) end, y = arrayMoveUndo[3]})
				subScore()
			end
		end	
	end


	local LimitatoriY = {}
	for i = 1,6 do
		LimitatoriY[i] = {}
	end

	local LimitatoriX = {}
	for i = 1,6 do
		LimitatoriX[i] = {}
	end








	l =1
	r = 1
	for i =1,6 do
		for j = 1,6 do
			local XPos,Ypos
			cell = display.newRoundedRect( GruppoScenaBase,0,0,CELL_WIDTH_V,CELL_WIDTH_V,6)
			cell.x = (j-1) *CELL_WIDTH_V + (CELL_WIDTH_V*0.5) +gbOffsetX
			cell.y = (i-1) * CELL_WIDTH_V + (CELL_WIDTH_V*0.5) + gbOffsetY
			cell.alpha = 0
			--cell:setFillColor( 0,0,0 )
			grid2[i][j]=cell
			if l<7 then
				LimitatoriX[l].X = cell.width
				LimitatoriX[l].Centro = cell.x
			end 
			l = l+1
		end
			LimitatoriY[i].Y = cell.height
			LimitatoriY[i].Centro = cell.y
	end


	RettangoloProva.y = grid2[3][6].y



	local ZonaLimitatoriY = {}
	for i =1,6 do
		ZonaLimitatoriY[i] = display.newRect( GruppoScenaBase, checkerBoard.x, LimitatoriY[i].Centro, checkerBoard.width, LimitatoriY[i].Y )
		ZonaLimitatoriY[i]:setFillColor( 0,0,0 )
		ZonaLimitatoriY[i].alpha = 0
	end

	local ZonaLimitatoriX = {}
	for i =1,6 do
		ZonaLimitatoriX[i] = display.newRect( GruppoScenaBase, LimitatoriX[i].Centro, checkerBoard.y, LimitatoriX[i].X, checkerBoard.height )
		ZonaLimitatoriX[i]:setFillColor( 1,1,1 )
		ZonaLimitatoriX[i].alpha = 0
	end


	local DelimitatoriWinTable = display.newImage( GruppoScenaBase,"DelimitatoriWin.png" )
	fitImage(DelimitatoriWinTable,10,CELL_WIDTH_V-5)
	DelimitatoriWinTable.x = ZonaLimitatoriX[6].contentBounds.xMax +5
	DelimitatoriWinTable.y = ZonaLimitatoriY[3].y


	function WhoIsVert(MinZonaY,MaxZonaY,MinZonaX,MaxZonaX)
		local flag = true
		local flag2 = true
		local GiustaYMin
		local GiustaYMax
		local i = (MinZonaY-1)
		while (i >=1 and flag) do
			for p=1,#objects2 do
				local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[p])
				local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[p])
				--print( true and (ZonaXMinOggetto == MinZonaX or ZonaXMaxOggetto == MinZonaX ))
				if (i >= ZonaYMinOggetto and  i <= ZonaYMaxOggetto) and (MinZonaX >= ZonaXMinOggetto and  MaxZonaX <= ZonaXMaxOggetto)  then
					flag = false
					GiustaYMin = i
					break
				end
			end
			i = i-1
		end

		local j = (MaxZonaY)+1
		while (j<7 and flag2) do
			for k=1,#objects2 do
				local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[k])
				local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[k])
				if (j >= ZonaYMinOggetto and  j <= ZonaYMaxOggetto) and (MinZonaX >= ZonaXMinOggetto and  MaxZonaX <= ZonaXMaxOggetto) then
					GiustaYMax = j
					flag2 = false
					break
				end
			end
			j = j+1
		end

		return GiustaYMin,GiustaYMax
	end

	function WhoIsOriz(MinZonaX,MaxZonaX,MinZonaY,MaxZonaY)
		local flag = true
		local flag2 = true
		local GiustaXMax
		local GiustaXMin
		local i = MinZonaX-1
		while (i>=1 and flag) do
			for p = 1,#objects2 do
				local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[p])
				local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[p])
				if ((i >= ZonaXMinOggetto and  i <= ZonaXMaxOggetto) and (MinZonaY >= ZonaYMinOggetto and  MaxZonaY <= ZonaYMaxOggetto)) then
					flag = false
					GiustaXMin= i
					break
				end
			end
			i = i-1
		end
		local j = (MaxZonaX+1)
		while (j<7 and flag2) do
			for k =1, #objects2 do
				local ZonaYMinOggetto,ZonaYMaxOggetto = StampaQuadratiDoveMiTrovoY(objects2[k])
				local ZonaXMinOggetto,ZonaXMaxOggetto = StampaQuadratiDoveMiTrovoX(objects2[k])
				if ((j >= ZonaXMinOggetto and  j <= ZonaXMaxOggetto) and (MinZonaY >= ZonaYMinOggetto and  MaxZonaY <= ZonaYMaxOggetto)) then
					flag2 = false
					GiustaXMax= j
					break
				end
			end
			j=j+1
		end

		return GiustaXMin,GiustaXMax
	end




	function DammiPosLibereV(ZonaMinX,ZonaMaxX,ZonaMinY,ZonaMaxY,What,Lenght)
		local YmaxDrag,YminDrag,XmaxDrag,XminDrag 
		if (What == "Vert2" or What == "Vert3") then
			YminDrag,YmaxDrag = WhoIsVert(ZonaMinY,ZonaMaxY,ZonaMinX,ZonaMaxX)
			if YminDrag == nil then
				YminDrag = checkerBoard.y- checkerBoard.height/2
			end
			if YmaxDrag == nil then
				YmaxDrag = checkerBoard.y+checkerBoard.height/2
			end

		end

		return YminDrag,YmaxDrag
	end


	function DammiPosLibereO(ZonaMinX,ZonaMaxX,ZonaMinY,ZonaMaxY,What,Lenght)
		local XmaxDrag,XminDrag 
		if (What == "Orizz2" or What == "Orizz3" ) then
			XminDrag,XmaxDrag = WhoIsOriz(ZonaMinX,ZonaMaxX,ZonaMinY,ZonaMaxY)
			if XminDrag == nil then
				XminDrag = checkerBoard.x- checkerBoard.width/2
			end
			if XmaxDrag == nil then
				XmaxDrag = checkerBoard.x+checkerBoard.width/2
			end
		end
		return XminDrag,XmaxDrag
	end



	function StampaQuadratiDoveMiTrovoX(rettangolo)
		local ZonaXMin, ZonaXMax
		if (rettangolo.What == "Vert2" or rettangolo.What == "Vert3" ) then
			if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[1].contentBounds.xMax ) then
				ZonaXmin = 1
				ZonaXMax = 1
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[2].contentBounds.xMax ) then
				ZonaXmin = 2
				ZonaXMax = 2
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax) then
				ZonaXmin = 3
				ZonaXMax = 3
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax) then
				ZonaXmin = 4
				ZonaXMax = 4
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
				ZonaXmin = 5
				ZonaXMax = 5
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax) then
				ZonaXmin = 6
				ZonaXMax = 6
			end
		
		elseif (rettangolo.What == "Orizz2") then
			if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[2].contentBounds.xMax ) then
				ZonaXmin = 1
				ZonaXMax = 2
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax ) then
				ZonaXmin = 2
				ZonaXMax = 3
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax ) then
				ZonaXmin = 3
				ZonaXMax = 4
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
				ZonaXmin = 4
				ZonaXMax = 5
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax ) then
				ZonaXmin = 5
				ZonaXMax = 6
			end

		elseif (rettangolo.What == "Orizz3") then
			if (rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMax ) then
				ZonaXmin = 1
				ZonaXMax = 3
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMax ) then
				ZonaXmin = 2
				ZonaXMax = 4
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMax ) then
				ZonaXmin = 3
				ZonaXMax = 5
			elseif (rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMax ) then
				ZonaXmin = 4
				ZonaXMax = 6
			end

		end
		return ZonaXmin,ZonaXMax
	end




	function StampaQuadratiDoveMiTrovoY(rettangolo)
		
		local ZonaYMin, ZonaYMax 
		if (rettangolo.What == "Vert2") then
				if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[2].contentBounds.yMax ) then
					ZonaYMin =1
					ZonaYMax = 2 
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
					ZonaYMin =2
					ZonaYMax = 3
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax ) then
					ZonaYMin =3
					ZonaYMax = 4
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
					ZonaYMin =4
					ZonaYMax = 5
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
					ZonaYMin =5
					ZonaYMax = 6
				end
		elseif (rettangolo.What == "Vert3") then
				if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
					ZonaYMin =1
					ZonaYMax = 3
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax) then
					ZonaYMin =2
					ZonaYMax = 4
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
					ZonaYMin =3
					ZonaYMax = 5
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
					ZonaYMin =4
					ZonaYMax = 6
				end
		elseif (rettangolo.What == "Orizz2" or rettangolo.What == "Orizz3") then
				if (rettangolo.contentBounds.yMax < ZonaLimitatoriY[1].contentBounds.yMax) then
					ZonaYMin =1
					ZonaYMax = 1
				elseif(rettangolo.contentBounds.yMax < ZonaLimitatoriY[2].contentBounds.yMax ) then
					ZonaYMin =2
					ZonaYMax = 2
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMax ) then
					ZonaYMin =3
					ZonaYMax = 3
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMax ) then
					ZonaYMin =4
					ZonaYMax = 4
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMax ) then
					ZonaYMin =5
					ZonaYMax = 5
				elseif (rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMax ) then
					ZonaYMin =6
					ZonaYMax = 6
				end
		
		end
		return ZonaYMin,ZonaYMax
	end


	function StampaSottoZoneV(rettangolo)
		if (rettangolo.What == "Vert2") then
			if (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[2].contentBounds.yMin + (ZonaLimitatoriY[2].height)/2 ) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[1].contentBounds.yMax ) then
				print( "Zona = 2.1 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMax})
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[2].contentBounds.yMin + (ZonaLimitatoriY[2].height)/2 ) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[3].contentBounds.yMin ) then
				print( "Zona = 2.2 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMax})
				ListenerOrizz(rettangolo)


			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height)/2 ) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[2].contentBounds.yMax ) then
				print( "Zona = 3.1 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMin ) then
				print( "Zona = 3.2 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[2].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[3].contentBounds.yMax ) then
				print( "Zona = 4.1 Max" )
				transition.to(rettangolo,{ time =100, y =ZonaLimitatoriY[2].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMin ) then
				print( "Zona = 4.2 Max" )
				transition.to(rettangolo,{ time=100, y =ZonaLimitatoriY[3].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2))  and  rettangolo.contentBounds.yMax > ZonaLimitatoriY[4].contentBounds.yMax) then
				print( "Zona = 5.1 Max" )
				transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[3].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMin ) then
				print( "Zona = 5.2 Max" )
				transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[4].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[5].contentBounds.yMax) then
				print( "Zona = 6.1 Max" )
				transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[4].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2))) then
				print( "Zona = 6.2 Max" )
				transition.to(rettangolo,{ time = 100, y =ZonaLimitatoriY[5].contentBounds.yMax   })
				ListenerOrizz(rettangolo)
			end

		elseif (rettangolo.What == "Vert3") then
			if (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height)/2 ) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[2].contentBounds.yMax ) then
				print( "Zona = 3.1 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[1].contentBounds.yMin + (ZonaLimitatoriY[1].height)/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[3].contentBounds.yMin + (ZonaLimitatoriY[3].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[4].contentBounds.yMin ) then
				print( "Zona = 3.2 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[2].contentBounds.yMin + ZonaLimitatoriY[2].height/2    })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[3].contentBounds.yMax ) then
				print( "Zona = 4.1 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[2].contentBounds.yMin +(ZonaLimitatoriY[2].height/2)   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[4].contentBounds.yMin + (ZonaLimitatoriY[4].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[5].contentBounds.yMin ) then
				print( "Zona = 4.2 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[3].contentBounds.yMin + ZonaLimitatoriY[3].height/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2))  and  rettangolo.contentBounds.yMax > ZonaLimitatoriY[4].contentBounds.yMax) then
				print( "Zona = 5.1 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[3].contentBounds.yMin + ZonaLimitatoriY[3].height/2 })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[5].contentBounds.yMin + (ZonaLimitatoriY[5].height/2)) and rettangolo.contentBounds.yMax < ZonaLimitatoriY[6].contentBounds.yMin ) then
				print( "Zona = 5.2 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[4].contentBounds.yMin +ZonaLimitatoriY[4].height/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax < (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2)) and rettangolo.contentBounds.yMax > ZonaLimitatoriY[5].contentBounds.yMax) then
				print( "Zona = 6.1 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[4].contentBounds.yMin + ZonaLimitatoriY[4].height/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.yMax > (ZonaLimitatoriY[6].contentBounds.yMin + (ZonaLimitatoriY[6].height/2))) then
				print( "Zona = 6.2 Max" )
				transition.to(rettangolo,{time = 100, y =ZonaLimitatoriY[5].contentBounds.yMin + ZonaLimitatoriY[5].height/2    })
				ListenerOrizz(rettangolo)
			end

		end
	end



	centroX = 0
	centroY = 0

	function ListenerOrizz(oggetto)
		if oggetto.What == "Vert2" or oggetto.What == "Vert3" then
			timer.performWithDelay(100,function()  if not ControllaDueCentriV(oggetto) then
			audio.play( SoundEffect )
			local array = {}
			array[1] = oggetto
			array[2] = centroX
			array[3] = centroY
			--print( centroX )
			if not FlagInCorsoSolver then
				buttonBack:setEnabled( true )
			end 
			--AroundBack.alpha = 1
			stack:push(array)
				end
				end)
		
		elseif oggetto.What == "Orizz2" or oggetto.What=="Orizz3" then
			timer.performWithDelay(100,function()  if not ControllaDueCentriO(oggetto) then
			audio.play( SoundEffect )
			local array = {}
			array[1] = oggetto
			array[2] = centroX
			array[3] = centroY
			--print( centroX )
			if not FlagInCorsoSolver then	
				buttonBack:setEnabled( true )
			end 
			--AroundBack.alpha = 1
			stack:push(array)
				end
				end)
		end
	end

	function ControllaDueCentriO( rettangolo)
		local flagControlla = false
		print( "CENTRIX:"..centroX,rettangolo.x )
		if centroX <= (rettangolo.x+0.5) and centroX >= (rettangolo.x -0.5)  then
			flagControlla = true
		end
		print( flagControlla )
		return flagControlla
	end



	function ControllaDueCentriV( rettangolo)
		local flagControlla = false
		print( "CENTRI:"..centroY,rettangolo.y )
		if centroY <= (rettangolo.y+0.5) and centroY >= (rettangolo.y -0.5)  then
			flagControlla = true
		end
		return flagControlla
	end

	function StampaSottoZoneO(rettangolo)
		print("NON DEVO STARE QUAAAAAAAAAAA")
		if (rettangolo.What == "Orizz2") then
			if (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[2].contentBounds.xMin + (ZonaLimitatoriX[2].width)/2 ) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[1].contentBounds.xMax ) then
				print( "Zona = 2.1 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMax})
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[2].contentBounds.xMin + (ZonaLimitatoriX[2].width)/2 ) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[3].contentBounds.xMin ) then
				print( "Zona = 2.2 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMax})
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width)/2 ) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[2].contentBounds.xMax ) then
				print( "Zona = 3.1 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMax})
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMin ) then
				print( "Zona = 3.2 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[2].contentBounds.xMax   })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[3].contentBounds.xMax ) then
				print( "Zona = 4.1 Max" )
				transition.to(rettangolo,{ time =100, x =ZonaLimitatoriX[2].contentBounds.xMax })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMin ) then
				print( "Zona = 4.2 Max" )
				transition.to(rettangolo,{ time=100, x =ZonaLimitatoriX[3].contentBounds.xMax })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2))  and  rettangolo.contentBounds.xMax > ZonaLimitatoriX[4].contentBounds.xMax) then
				print( "Zona = 5.1 Max" )
				transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[3].contentBounds.xMax})
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMin ) then
				print( "Zona = 5.2 Max" )
				transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[4].contentBounds.xMax })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[5].contentBounds.xMax) then
				transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[4].contentBounds.xMax })
				ListenerOrizz(rettangolo)
			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2))) then 
				print( "Zona = 6.1 Max" )
				print( "Zona = 6.2 Max" )
				transition.to(rettangolo,{ time = 100, x =ZonaLimitatoriX[5].contentBounds.xMax})
				ListenerOrizz(rettangolo)
				
			end

		elseif (rettangolo.What == "Orizz3") then
			if (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width)/2 ) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[2].contentBounds.xMax ) then
				print( "Zona = 3.1 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[1].contentBounds.xMin + (ZonaLimitatoriX[1].width)/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[3].contentBounds.xMin + (ZonaLimitatoriX[3].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[4].contentBounds.xMin ) then
				print( "Zona = 3.2 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[2].contentBounds.xMin + ZonaLimitatoriX[2].width/2    })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[3].contentBounds.xMax ) then
				print( "Zona = 4.1 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[2].contentBounds.xMin +(ZonaLimitatoriX[2].width/2)   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[4].contentBounds.xMin + (ZonaLimitatoriX[4].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[5].contentBounds.xMin ) then
				print( "Zona = 4.2 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[3].contentBounds.xMin + ZonaLimitatoriX[3].width/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2))  and  rettangolo.contentBounds.xMax > ZonaLimitatoriX[4].contentBounds.xMax) then
				print( "Zona = 5.1 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[3].contentBounds.xMin + ZonaLimitatoriX[3].width/2 , time = 300 })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[5].contentBounds.xMin + (ZonaLimitatoriX[5].width/2)) and rettangolo.contentBounds.xMax < ZonaLimitatoriX[6].contentBounds.xMin ) then
				print( "Zona = 5.2 Max" )
				transition.to(rettangolo,{time = 100, x =ZonaLimitatoriX[4].contentBounds.xMin +ZonaLimitatoriX[4].width/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax < (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2)) and rettangolo.contentBounds.xMax > ZonaLimitatoriX[5].contentBounds.xMax) then
				print( "Zona = 6.1 Max" )
				transition.to(rettangolo,{time =100, x =ZonaLimitatoriX[4].contentBounds.xMin + ZonaLimitatoriX[4].width/2   })
				ListenerOrizz(rettangolo)

			elseif (rettangolo.contentBounds.xMax > (ZonaLimitatoriX[6].contentBounds.xMin + (ZonaLimitatoriX[6].width/2))) then
				print( "Zona = 6.2 Max" )
				transition.to(rettangolo,{time =100, x =ZonaLimitatoriX[5].contentBounds.xMin + ZonaLimitatoriX[5].width/2    })
				ListenerOrizz(rettangolo)
			end

		end
	end



	function onTouchO(event)
		print( "LISTEN O " )
		if event.phase == "began" then
			objects2 = {}
			for i=1, #objects do
				objects2[i] = objects[i]
			end
			flagNilXMin = false
			flagNilXMax = false
			centroX = event.target.x
			--flagVert3 = false
			table.remove(objects2, event.target.MyID)
			--YMINIMODRAG,YMASSIMODRAG 
			--print( "began" )
			local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
			local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
			XMINIMODRAG,XMASSIMODRAG = DammiPosLibereO(arg1,arg2,arg3,arg4,event.target.What,#objects2)
			print( XMINIMODRAG,XMASSIMODRAG )
			if XMASSIMODRAG == checkerBoard.x + checkerBoard.width/2 then
				flagNilXMax = true
			end
			if XMINIMODRAG == checkerBoard.x - checkerBoard.width/2 then
				flagNilXMin = true
			end
			display.getCurrentStage( ):setFocus( event.target)
			event.target.isFocus = true
			event.target.xStart = event.x-event.target.x

		elseif event.target.isFocus then
			if event.phase == "moved" then
				event.target.x = event.x-event.target.xStart
				if event.target.What == "Orizz3" then

					if flagNilXMin and not flagNilXMax then
						print( "1" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-2].contentBounds.xMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMax -CELL_HEIGHT_O/2
						end

					elseif flagNilXMax and not flagNilXMin then
						print( "2" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+2].contentBounds.xMax - CELL_HEIGHT_O/2
						end 
					elseif flagNilXMin and flagNilXMax then
						print( "3" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMax -CELL_HEIGHT_O/2
						end
					else 
						print( "4" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-2].contentBounds.xMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+2].contentBounds.xMax - CELL_HEIGHT_O/2
						end
					end
				

				else
					if flagNilXMin and not flagNilXMax then
						print( "1e" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMin 
						end

					elseif flagNilXMax and not flagNilXMin then
						print( "2e" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMax
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
						end 
					elseif flagNilXMin and flagNilXMax then
						print( "3e" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMax 
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMin
						end
					else 
						print( "4e" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
						end
					end
				end


			elseif event.phase =="ended" or event.phase == "cancelled" then
				StampaSottoZoneO(event.target)
				timer.performWithDelay( 100, function( ) if not ControllaDueCentriO(event.target) then addScore() end end  ) 
				display.getCurrentStage( ):setFocus( event.target,nil )
				event.target.isFocus = false
			end

		end
		return true
	end


	function onTouchOX(event)
		print( "LISTEN OX " )
		if event.phase == "began" then
			objects2 = {}
			for i=1, #objects do
				objects2[i] = objects[i]
			end
			flagNilXMin = false
			flagNilXMax = false
			centroX = event.target.x
			--flagVert3 = false
			table.remove(objects2, event.target.MyID)
			--YMINIMODRAG,YMASSIMODRAG 
			--print( "began" )
			local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
			local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
			XMINIMODRAG,XMASSIMODRAG = DammiPosLibereO(arg1,arg2,arg3,arg4,event.target.What,#objects2)
			print( XMINIMODRAG,XMASSIMODRAG )
			if XMASSIMODRAG == checkerBoard.x + checkerBoard.width/2 then
				flagNilXMax = true
			end
			if XMINIMODRAG == checkerBoard.x - checkerBoard.width/2 then
				flagNilXMin = true
			end
			isRed = true
			TouchEnabled = true
			display.getCurrentStage( ):setFocus( event.target)
			event.target.isFocus = true
			event.target.xStart = event.x-event.target.x

		elseif event.target.isFocus then

			if event.phase == "moved" and TouchEnabled then
				print("MOVEEEE")
				event.target.x = event.x-event.target.xStart
					if flagNilXMin and not flagNilXMax then
						print( "1e" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMin 
						end

					elseif flagNilXMax and not flagNilXMin then
						print( "2e" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-40) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMax
							TouchEnabled = false
							print("appenaMessoFalse")
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax  
						end 

					elseif flagNilXMin and flagNilXMax then
						print( "3e" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-40) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMax 
							TouchEnabled = false
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMin
						end

					else 
						print( "4e" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
						end
					end

			elseif event.phase =="ended" or event.phase == "cancelled" then
				audio.play( SoundEffect )
				if  TouchEnabled then
					print("NNON CI DEVO STARE!!")
					StampaSottoZoneO(event.target)
					timer.performWithDelay( 100, function( ) if not ControllaDueCentriO(event.target) then addScore() end end  ) 
					display.getCurrentStage( ):setFocus( event.target,nil )
					event.target.isFocus = false
				else
					timer.performWithDelay( 100, function( ) if not ControllaDueCentriO(event.target) then  end end  ) 
					display.getCurrentStage( ):setFocus( event.target,nil )
					event.target.isFocus = false
				end

			end
		end
		return true
	end

	function onTouchV(event)
		print( "LISTEN V " )
		if event.phase == "began" then
			objects2 = {}
			for i=1, #objects do
				objects2[i] = objects[i]
			end
			flagNilYMin = false
			flagNilYMax = false
			--flagVert3 = false
			centroY = event.target.y
			table.remove(objects2, event.target.MyID)
			--YMINIMODRAG,YMASSIMODRAG 
			--print( "began" )
			local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
			local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
			YMINIMODRAG,YMASSIMODRAG = DammiPosLibereV(arg1,arg2,arg3,arg4,event.target.What,#objects2)
			if YMASSIMODRAG == checkerBoard.y + checkerBoard.height/2 then
				flagNilYMax = true
			end
			if YMINIMODRAG == checkerBoard.y - checkerBoard.height/2 then
				flagNilYMin = true
			end
			print( flagNilYMax,flagNilYMin )
			display.getCurrentStage( ):setFocus( event.target)
			event.target.isFocus = true
			event.target.yStart = event.y-event.target.y

		elseif event.target.isFocus then
			if event.phase == "moved" then
				event.target.y = event.y-event.target.yStart
				if event.target.What == "Vert3" then

					if flagNilYMin and not flagNilYMax then
						print( "1" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-2].contentBounds.yMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMax -CELL_HEIGHT_O/2
						end

					elseif flagNilYMax and not flagNilYMin then
						print( "2" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+2].contentBounds.yMax - CELL_HEIGHT_O/2
						end 
					elseif flagNilYMin and flagNilYMax then
						print( "3" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMax -CELL_HEIGHT_O/2
						end
					else 
						print( "4" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-2].contentBounds.yMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+2].contentBounds.yMax - CELL_HEIGHT_O/2
						end
					end
				

				else
					if flagNilYMin and not flagNilYMax then
						print( "1e" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-1].contentBounds.yMin 
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMin 
						end

					elseif flagNilYMax and not flagNilYMin then
						print( "2e" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMax
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+1].contentBounds.yMax 
						end 
					elseif flagNilYMin and flagNilYMax then
						print( "3e" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMax 
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMin
						end
					else 
						print( "4e" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-1].contentBounds.yMin 
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+1].contentBounds.yMax 
						end
					end
				end


			elseif event.phase =="ended" or event.phase == "cancelled" then
				--audio.play( SoundEffect )
				print( "ended" )
				StampaSottoZoneV(event.target)
				 timer.performWithDelay( 100, function( ) if not ControllaDueCentriV(event.target) then addScore() end end  )  
				

				display.getCurrentStage( ):setFocus( event.target,nil )
				event.target.isFocus = false
			end

		end
		return true
	end


	Passi1 = {}
	for ul =1,60 do
		Passi1[ul] = {}
	end




	----DA MODIFICARE IN BASE AL NUMERO MAX!!!
	if numLivello==1 then
		CerchioChange2:addEventListener( "tap", ChangeBoardDX )
	elseif numLivello == 4 then
		CerchioChange1:addEventListener( "tap", ChangeBoardSX )
	elseif numLivello >1 or numLivello<4 then
		CerchioChange1:addEventListener( "tap", ChangeBoardSX )
		CerchioChange2:addEventListener( "tap", ChangeBoardDX )
	end

	
	li = 1
	
	
	function LoopTransizioni(obj,isOriz2,isOriz3,isVert2,isVert3)
		
		if isOriz3 then
			local function MainLoopOriz3()
				if obj.rotation == -180 then --VERSO SX
					local function loop1Oriz3()
						print( "VERSO SX" )
						transition.to( obj, {time = 100, x = ZonaLimitatoriX[Passi1[li].xMin1].x + CELL_WIDTH_O/2,onComplete = function() 
																													obj.alpha = 1
																													MainLoopOriz3() end } )
					end
					transition.to( obj, { x = ZonaLimitatoriX[Passi1[li].xMin1].x, alpha = 0, time = 1500, onComplete = loop1Oriz3} )
				
				else --VERSO DX
					local function loop2Oriz3()
						print( "VERSO DX" )
						--posizionePartenza
						transition.to( obj, { time =100,x = ZonaLimitatoriX[Passi1[li].xMin1].x + CELL_WIDTH_O/2,onComplete = function() 
																													obj.alpha = 1
																													MainLoopOriz3() end } )
					end
					transition.to( obj, { x = ZonaLimitatoriX[Passi1[li].xMax1].x, alpha = 0, time = 1500, onComplete = loop2Oriz3} )
				end
			end
			MainLoopOriz3()

		elseif isOriz2 then
			local function MainLoopOriz2()
				if obj.rotation == -180 then --VERSO SX
					print( "VERSO SX" )
					local function loop1Oriz2()
						transition.to( obj, { time = 100,x = ZonaLimitatoriX[Passi1[li].xMin1].x + CELL_HEIGHT_O/2 ,onComplete = function() 
																													obj.alpha = 1
																													MainLoopOriz2() end } )
					end
					transition.to( obj, { x = ZonaLimitatoriX[Passi1[li].xMin1].x, alpha = 0, time = 1500, onComplete = loop1Oriz2} )
				
				else --VERSO DX
					local function loop2Oriz2()
						print( "VERSO DX" )
						transition.to( obj, { time =100,x = ZonaLimitatoriX[Passi1[li].xMin1].x + CELL_HEIGHT_O/2 ,onComplete = function() 
																													obj.alpha = 1
																													MainLoopOriz2() end } )
					end
					transition.to( obj, { x = ZonaLimitatoriX[Passi1[li].xMax1].x, alpha = 0, time = 1500, onComplete = loop2Oriz2} )
				end
			end
			MainLoopOriz2()

		elseif isVert2 then
			local function MainLoopVert2()
				if obj.rotation == -90 then --VERSO ALTO
					print( "VERSO ALTO" )
					local function loop1Vert2()
						transition.to( obj, {time = 100, y = ZonaLimitatoriY[Passi1[li].yMin1].y + CELL_HEIGHT_O3/2 ,onComplete = function() 
																													obj.alpha = 1
																													MainLoopVert2() end } )
					end
					transition.to( obj, { y = ZonaLimitatoriY[Passi1[li].yMin1].y, alpha = 0, time = 1500, onComplete = loop1Vert2} )
				else --VERSO BASSO
					local function loop2Vert2()
						print( "VERSO BASSO" )
						transition.to( obj, { time =100,y = ZonaLimitatoriY[Passi1[li].yMin1].y + CELL_HEIGHT_O3/2 ,onComplete = function() 
																													obj.alpha = 1
																													MainLoopVert2() end } )
					end
					transition.to( obj, { y = ZonaLimitatoriY[Passi1[li].yMax1].y, alpha = 0, time = 1500, onComplete = loop2Vert2} )
				end
			end
			MainLoopVert2()
		
		elseif isVert3 then
			local function MainLoopVert3()
				if obj.rotation == -90 then --VERSO ALTO
					local function loop1Vert3()
						transition.to( obj, { time =100,y = ZonaLimitatoriY[Passi1[li].yMin1].y + CELL_HEIGHT_O3,onComplete = function() 
																													obj.alpha = 1
																													MainLoopVert3() end } )
					end
					transition.to( obj, { y  = ZonaLimitatoriY[Passi1[li].yMin1].y, alpha = 0, time = 1500, onComplete = loop1Vert3} )
				else --VERSO BASSO
					local function loop2Vert3()
						transition.to( obj, { time =100,y = ZonaLimitatoriY[Passi1[li].yMin1].y + CELL_HEIGHT_O3,onComplete = function() 
																													obj.alpha = 1
																													MainLoopVert3() end } )
					end
					transition.to( obj, { y  = ZonaLimitatoriY[Passi1[li].yMax1].y, alpha = 0, time = 1500, onComplete = loop2Vert3} )
				end
			end
			MainLoopVert3()	

		end
	end


	--ListerNuovoRettangoloDaMuovere
	function MoveRectOrizz(event) 
		if event.phase == "began" then
			objects2 = {}
			for i=1, #objects do
				objects2[i] = objects[i]
			end
			flagNilXMin = false
			flagNilXMax = false
			centroX = event.target.x
			--flagVert3 = false
			table.remove(objects2, event.target.MyID)
			--YMINIMODRAG,YMASSIMODRAG 
			--print( "began" )
			local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
			local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
			XMINIMODRAG,XMASSIMODRAG = DammiPosLibereO(arg1,arg2,arg3,arg4,event.target.What,#objects2)
			print( XMINIMODRAG,XMASSIMODRAG )
			if XMASSIMODRAG == checkerBoard.x + checkerBoard.width/2 then
				flagNilXMax = true
			end
			if XMINIMODRAG == checkerBoard.x - checkerBoard.width/2 then
				flagNilXMin = true
			end
			display.getCurrentStage( ):setFocus( event.target)
			event.target.isFocus = true
			event.target.xStart = event.x-event.target.x

		elseif event.target.isFocus then
			if event.phase == "moved" then
				event.target.x = event.x-event.target.xStart
				if event.target.What == "Orizz3" then

					if flagNilXMin and not flagNilXMax then
						print( "1" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-2].contentBounds.xMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMax -CELL_HEIGHT_O/2
						end

					elseif flagNilXMax and not flagNilXMin then
						print( "2" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+2].contentBounds.xMax - CELL_HEIGHT_O/2
						end 
					elseif flagNilXMin and flagNilXMax then
						print( "3" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMax -CELL_HEIGHT_O/2
						end
					else 
						print( "4" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-2].contentBounds.xMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+2].contentBounds.xMax - CELL_HEIGHT_O/2
						end
					end
				

				else
					if flagNilXMin and not flagNilXMax then
						print( "1e" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMin 
						end

					elseif flagNilXMax and not flagNilXMin then
						print( "2e" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMax
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
						end 
					elseif flagNilXMin and flagNilXMax then
						print( "3e" )
						if(event.target.contentBounds.xMax > XMASSIMODRAG-2) then 
							event.target.x = ZonaLimitatoriX[5].contentBounds.xMax 
						elseif(event.target.contentBounds.xMin < XMINIMODRAG+2) then
							event.target.x = ZonaLimitatoriX[2].contentBounds.xMin
						end
					else 
						print( "4e" )
						if(event.target.contentBounds.xMax > ZonaLimitatoriX[XMASSIMODRAG].x-26) then 
							event.target.x = ZonaLimitatoriX[XMASSIMODRAG-1].contentBounds.xMin 
						elseif(event.target.contentBounds.xMin < ZonaLimitatoriX[XMINIMODRAG].x+26) then
							event.target.x = ZonaLimitatoriX[XMINIMODRAG+1].contentBounds.xMax 
						end
					end
				end


			elseif event.phase =="ended" or event.phase == "cancelled" then
				StampaSottoZoneO(event.target)
				timer.performWithDelay( 100, function( ) 
												if not ControllaDueCentriO(event.target) then addScore() 
												end
												local posizioneCorrentexMin,posizioneCorrentexMax = StampaQuadratiDoveMiTrovoX(event.target)
												local posizioneCorrenteyMin,posizioneCorrenteyMax = StampaQuadratiDoveMiTrovoY(event.target)
												if Passi1[li].xMin2 == posizioneCorrentexMin and  Passi1[li].xMax2 == posizioneCorrentexMax and  Passi1[li].yMin2 == posizioneCorrenteyMin and Passi1[li].yMax2 == posizioneCorrenteyMax then
													ArraySprite[li].alpha = 0
													print( "ArraySprite[li] = "..ArraySprite[li].alpha)
													ArraySpriteArrow[li].alpha = 0
													transition.cancel( ArraySpriteArrow[li] )
													li = li +1
													ArraySprite[li].alpha = 0.7
													IniziaMostraMosse()
													event.target:removeEventListener( "touch", MoveRectOrizz )
												end
											end  
										)

				display.getCurrentStage( ):setFocus( event.target,nil )
				event.target.isFocus = false
			end

		end
	
		return true
	end

	function MoveRectVert(event)
		if event.phase == "began" then
			objects2 = {}
			for i=1, #objects do
				objects2[i] = objects[i]
			end
			flagNilYMin = false
			flagNilYMax = false
			--flagVert3 = false
			centroY = event.target.y
			table.remove(objects2, event.target.MyID)
			--YMINIMODRAG,YMASSIMODRAG 
			--print( "began" )
			local arg1,arg2 = StampaQuadratiDoveMiTrovoX(event.target)
			local arg3,arg4 = StampaQuadratiDoveMiTrovoY(event.target)
			YMINIMODRAG,YMASSIMODRAG = DammiPosLibereV(arg1,arg2,arg3,arg4,event.target.What,#objects2)
			if YMASSIMODRAG == checkerBoard.y + checkerBoard.height/2 then
				flagNilYMax = true
			end
			if YMINIMODRAG == checkerBoard.y - checkerBoard.height/2 then
				flagNilYMin = true
			end
			print( flagNilYMax,flagNilYMin )
			display.getCurrentStage( ):setFocus( event.target)
			event.target.isFocus = true
			event.target.yStart = event.y-event.target.y

		elseif event.target.isFocus then
			if event.phase == "moved" then
				event.target.y = event.y-event.target.yStart
				if event.target.What == "Vert3" then

					if flagNilYMin and not flagNilYMax then
						print( "1" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-2].contentBounds.yMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMax -CELL_HEIGHT_O/2
						end

					elseif flagNilYMax and not flagNilYMin then
						print( "2" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMin + CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+2].contentBounds.yMax - CELL_HEIGHT_O/2
						end 
					elseif flagNilYMin and flagNilYMax then
						print( "3" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMax -CELL_HEIGHT_O/2
						end
					else 
						print( "4" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-2].contentBounds.yMin +CELL_HEIGHT_O/2
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+2].contentBounds.yMax - CELL_HEIGHT_O/2
						end
					end
				

				else
					if flagNilYMin and not flagNilYMax then
						print( "1e" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-1].contentBounds.yMin 
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMin 
						end

					elseif flagNilYMax and not flagNilYMin then
						print( "2e" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMax
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+1].contentBounds.yMax 
						end 
					elseif flagNilYMin and flagNilYMax then
						print( "3e" )
						if(event.target.contentBounds.yMax > YMASSIMODRAG-2) then 
							event.target.y= ZonaLimitatoriY[5].contentBounds.yMax 
						elseif(event.target.contentBounds.yMin < YMINIMODRAG+2) then
							event.target.y = ZonaLimitatoriY[2].contentBounds.yMin
						end
					else 
						print( "4e" )
						if(event.target.contentBounds.yMax > ZonaLimitatoriY[YMASSIMODRAG].y-26) then 
							event.target.y= ZonaLimitatoriY[YMASSIMODRAG-1].contentBounds.yMin 
						elseif(event.target.contentBounds.yMin < ZonaLimitatoriY[YMINIMODRAG].y+26) then
							event.target.y = ZonaLimitatoriY[YMINIMODRAG+1].contentBounds.yMax 
						end
					end
				end








			
			elseif event.phase =="ended" or event.phase == "cancelled" then
				print( "ended" )
				StampaSottoZoneV(event.target)
				 timer.performWithDelay( 100, function( ) 
				 									if not ControllaDueCentriV(event.target) then 
				 										addScore() 
				 									end
				 									local posizioneCorrentexMin,posizioneCorrentexMax = StampaQuadratiDoveMiTrovoX(event.target)
													local posizioneCorrenteyMin,posizioneCorrenteyMax = StampaQuadratiDoveMiTrovoY(event.target)
													if Passi1[li].xMin2 == posizioneCorrentexMin and  Passi1[li].xMax2 == posizioneCorrentexMax and  Passi1[li].yMin2 == posizioneCorrenteyMin and Passi1[li].yMax2 == posizioneCorrenteyMax then
														ArraySprite[li].alpha = 0
														print( "ArraySprite[li] = "..ArraySprite[li].alpha)
														ArraySpriteArrow[li].alpha = 0
														transition.cancel( ArraySpriteArrow[li] )
														li = li +1
														ArraySprite[li].alpha = 0.7
														IniziaMostraMosse()
														event.target:removeEventListener( "touch", MoveRectVert )
													end 
				 								end  )  
				

				display.getCurrentStage( ):setFocus( event.target,nil )
				event.target.isFocus = false
			end

		end
		return true
	end
	
	function listenerSprite(event )
		print(event.phase)
	end
	
	function Accogli(event )

		print( "Li Alla seconda Esecuzione non so quanto sia = "..li )
		if not FlagInCorsoSolver then 
			FlagInCorsoSolver = true
			BGperLoading = display.newRoundedRect( GruppoScenaBase, display.contentCenterX, display.contentCenterY, display.actualContentWidth-25, checkerBoard.height+2, 6 )
			BGperLoading:setFillColor( 0.96,0.8,0.61 )
			BGperLoading.alpha =0
			TestoAccogli = display.newText( GruppoScenaBase, "Solving Puzzle...", display.contentCenterX, display.contentCenterY - 10, "VAG.ttf",35 )
			TestoAccogli:setFillColor( 1,1,1 )
			TestoAccogli.alpha = 0
			transition.to( BGperLoading, {onStart = function()
														if numLivello==1 then 
															CerchioChange2:removeEventListener( "tap", ChangeBoardDX )
														elseif numLivello ==4 then
															CerchioChange1:removeEventListener( "tap", ChangeBoardSX )  
														else 
															CerchioChange1:removeEventListener( "tap", ChangeBoardSX )
															CerchioChange2:removeEventListener( "tap", ChangeBoardDX )
														end
															buttonBack:setEnabled( false )
															buttonRestart:setEnabled( false )
															buttonPause:setEnabled( false ) 
														end ,x = display.contentCenterX, time = 1000,alpha = 0.8} )
			transition.to( TestoAccogli, {x = display.contentCenterX, time = 1000,alpha = 1} )
			for indeOgetto = 1,#objects do
				if objects[indeOgetto].What == "Orizz2" or objects[indeOgetto].What == "Orizz3"  then
					if objects[indeOgetto].Red ~= nil and objects[indeOgetto].Red == "Red" then
						objects[indeOgetto]:removeEventListener( "touch", onTouchOX )
					else 
						objects[indeOgetto]:removeEventListener( "touch", onTouchO )
					end
				elseif	objects[indeOgetto].What == "Vert2" or objects[indeOgetto].What == "Vert3" then
					objects[indeOgetto]:removeEventListener( "touch", onTouchV )
				end
			end
			timer.performWithDelay( 2000, CostruisciStringa )

		else
			buttonResolve:setEnabled( true )
			composer.showOverlay("TapToFinishSolve", { isModal = true, effect = "slideRight", time = 500 , params = {checkWidth = checkerBoard.width , checkHeight = checkerBoard.height}} )
		end

	end

	--Listener Bottone "SOLVE"
	function CostruisciStringa(event)

		 
			

			local function replace_char(pos, str, r)
		    	return str:sub(1, pos-1) .. r .. str:sub(pos+1)
			end

			local Griglia = {}
			for indice = 1,6 do
				Griglia[indice] = {}
			end
			local Stringa = "ciao"
			for i=1,#objects do
				if objects[i].What == "Orizz2" and objects[i].Red == "Red" then
					local xMinima,xMassima = StampaQuadratiDoveMiTrovoX(objects[i])
					local yMinima,yMassima = StampaQuadratiDoveMiTrovoY(objects[i])
					Griglia[yMinima][xMinima] = "X"
					Griglia[yMinima][xMassima] = "X"

				elseif objects[i].What == "Orizz2" then
					local xMinima,xMassima = StampaQuadratiDoveMiTrovoX(objects[i])
					local yMinima,yMassima = StampaQuadratiDoveMiTrovoY(objects[i])
					Griglia[yMinima][xMinima] = "2"
					Griglia[yMinima][xMassima] = "2"

				elseif objects[i].What == "Orizz3" then
					local xMinima,xMassima = StampaQuadratiDoveMiTrovoX(objects[i])
					local yMinima,yMassima = StampaQuadratiDoveMiTrovoY(objects[i])
					Griglia[yMinima][xMinima] = "3"
					Griglia[yMinima][xMinima+1] = "3"
					Griglia[yMinima][xMassima] = "3"

				elseif objects[i].What == "Vert2" then
					local xMinima,xMassima = StampaQuadratiDoveMiTrovoX(objects[i])
					local yMinima,yMassima = StampaQuadratiDoveMiTrovoY(objects[i])
					Griglia[yMinima][xMinima] = "B"
					Griglia[yMassima][xMinima] = "B"

				elseif objects[i].What == "Vert3" then
					local xMinima,xMassima = StampaQuadratiDoveMiTrovoX(objects[i])
					local yMinima,yMassima = StampaQuadratiDoveMiTrovoY(objects[i])
					Griglia[yMinima][xMinima] = "C"
					Griglia[yMinima+1][xMinima] = "C"
					Griglia[yMassima][xMinima] = "C"
				end
			end

			local indiceStringa = 1
			for rigaGriglia=1,6 do
				for colonnaGriglia = 1,6 do
					if Griglia[rigaGriglia][colonnaGriglia] == nil then
						Stringa = replace_char(indiceStringa,Stringa,".") 
					elseif Griglia[rigaGriglia][colonnaGriglia] == "B" then
						Stringa = replace_char(indiceStringa,Stringa,"B")
					elseif Griglia[rigaGriglia][colonnaGriglia] == "C" then
						Stringa = replace_char(indiceStringa,Stringa,"C")
					elseif Griglia[rigaGriglia][colonnaGriglia] == "2" then
						Stringa = replace_char(indiceStringa,Stringa,"2")
					elseif Griglia[rigaGriglia][colonnaGriglia] == "3" then
						Stringa = replace_char(indiceStringa,Stringa,"3")
					elseif Griglia[rigaGriglia][colonnaGriglia] == "X" then
						Stringa = replace_char(indiceStringa,Stringa,"X")
					end

					indiceStringa = indiceStringa + 1
				end
			end

			for indiceFreeshPassi1 = 1,#Passi1 do
				Passi1[indiceFreeshPassi1] = nil
				Passi1[indiceFreeshPassi1] = {} 
			end

			
			composer.showOverlay("ProvaNuovo",{params = {StringaStato = Stringa}})

			
			
			transition.to( BGperLoading, {onComplete= function()
														if numLivello==1 then 
															CerchioChange2:addEventListener( "tap", ChangeBoardDX )
														elseif numLivello ==4 then
															CerchioChange1:addEventListener( "tap", ChangeBoardSX )  
														else 
															CerchioChange1:addEventListener( "tap", ChangeBoardSX )
															CerchioChange2:addEventListener( "tap", ChangeBoardDX )
														end
														buttonResolve:setEnabled( true ) buttonPause:setEnabled( true ) end , time = 1500, x = display.contentCenterX, alpha = 0} )
			transition.to( TestoAccogli, {time = 1500, x = display.contentCenterX, alpha = 0} )

			 
			ContaOccorrenze = 0
			local indicePerOcc = 1
			while Passi1[indicePerOcc].xMin1 ~= nil do
				ContaOccorrenze = ContaOccorrenze+1
				indicePerOcc = indicePerOcc +1
			end
			
			print( "TIENI OCCHIO ContaOccorrenze : "..ContaOccorrenze )
			for indiceArraySprite = 1,ContaOccorrenze do
				print( "indice = "..indiceArraySprite )
				local indiceArraySpriteXMin = Passi1[indiceArraySprite].xMin2
				local indiceArraySpriteXMax = Passi1[indiceArraySprite].xMax2
				local indiceArraySpriteYMin = Passi1[indiceArraySprite].yMin2
				local indiceArraySpriteYMax = Passi1[indiceArraySprite].yMax2
				if (indiceArraySpriteXMin == indiceArraySpriteXMax) and (indiceArraySpriteYMax-indiceArraySpriteYMin == 1)  then --Vert2
					local ImageSheetOrizz2 = graphics.newImageSheet( "SheetOrizzontale2.png",  sheetO2:getSheet() )
					local sequenceData = {
						start = 1,
						count = 58,
						time = 2300,
						loopDirection = "bounce"
					}
					local SpriteOriz2 = display.newSprite( GruppoScenaBase, ImageSheetOrizz2, sequenceData )
					SpriteOriz2.rotation = 90
					SpriteOriz2.alpha = 0
					SpriteOriz2:play( )
					local YscaleFactor = CELL_HEIGHT_O3 / SpriteOriz2.height 
					local XscaleFactor = CELL_WIDTH_O / SpriteOriz2.width
					SpriteOriz2:scale( XscaleFactor, YscaleFactor )
					SpriteOriz2.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin2].x
					SpriteOriz2.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin2].y + CELL_HEIGHT_O/2
					table.insert( ArraySprite,SpriteOriz2)


					local Direzione = display.newImage( "270.png" )
					fitImage(Direzione,CELL_WIDTH_O/4,CELL_HEIGHT_O/2)
					Direzione.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin2].x 
					Direzione.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin1].y + CELL_WIDTH_V3/2
					local aDX = false
					local aSX = false
					if Passi1[indiceArraySprite].yMin2 < Passi1[indiceArraySprite].yMin1 then --VERSO ALTO
						Direzione.rotation = -90
						aSX = true
					elseif Passi1[indiceArraySprite].yMin2 > Passi1[indiceArraySprite].yMin1 then --VERSO BASSO
						Direzione.rotation = 90
						print( "Direzione.rotation: "..Direzione.rotation )
						aDX = true
					end
					Direzione.alpha = 0
					GruppoScenaBase:insert(Direzione)
					table.insert( ArraySpriteArrow, Direzione )

				elseif (indiceArraySpriteXMin == indiceArraySpriteXMax) and (indiceArraySpriteYMax-indiceArraySpriteYMin == 2)  then --Vert3
					local ImageSheetOrizz3 = graphics.newImageSheet( "SheetOrizzontale3.png",  sheetO3:getSheet() )
					local sequenceData = {
						start = 1,
						count = 58,
						time = 2300,
						loopDirection = "bounce"
					}
					local SpriteOriz3 = display.newSprite( GruppoScenaBase, ImageSheetOrizz3, sequenceData )
					SpriteOriz3.rotation = 90
					SpriteOriz3.alpha = 0
					SpriteOriz3:play( )
					local YscaleFactor = CELL_HEIGHT_O3 / SpriteOriz3.height 
					local XscaleFactor = CELL_WIDTH_O3 / SpriteOriz3.width 
					--fitImage(SpriteOrizz2,CELL_HEIGHT_O+10,CELL_HEIGHT_V)
					SpriteOriz3:scale( XscaleFactor, YscaleFactor )
					SpriteOriz3.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin2].x
					SpriteOriz3.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin2].y + CELL_HEIGHT_O
					table.insert( ArraySprite,SpriteOriz3)

					local Direzione = display.newImage( "270.png" )
					fitImage(Direzione,CELL_WIDTH_O/4,CELL_HEIGHT_O/2)
					Direzione.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin2].x 
					Direzione.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin1].y + CELL_WIDTH_V3
					local aDX = false
					local aSX = false
					if Passi1[indiceArraySprite].yMin2 < Passi1[indiceArraySprite].yMin1 then --VERSO ALTO
						Direzione.rotation = -90
						
					elseif Passi1[indiceArraySprite].yMin2 > Passi1[indiceArraySprite].yMin1 then --VERSO BASSO
						Direzione.rotation = 90
						
					end
					Direzione.alpha = 0
					GruppoScenaBase:insert(Direzione)
					table.insert( ArraySpriteArrow, Direzione )

				elseif (indiceArraySpriteYMin == indiceArraySpriteYMax) and (indiceArraySpriteXMax-indiceArraySpriteXMin == 1)  then --Oriz2
					local ImageSheetOrizz2 = graphics.newImageSheet( "SheetOrizzontale2.png",  sheetO2:getSheet() )
					local sequenceData = {
						start = 1,
						count = 58,
						time = 2300,
						loopDirection = "bounce"
					}
					local SpriteOriz2 = display.newSprite( GruppoScenaBase, ImageSheetOrizz2, sequenceData )
					SpriteOriz2.alpha = 0
					SpriteOriz2:play( )
					local YscaleFactor = CELL_HEIGHT_O3 / SpriteOriz2.height 
					local XscaleFactor = CELL_WIDTH_O / SpriteOriz2.width 
					--fitImage(SpriteOrizz2,CELL_HEIGHT_O+10,CELL_HEIGHT_V)
					SpriteOriz2:scale( XscaleFactor, YscaleFactor )
					SpriteOriz2.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin2].x + CELL_HEIGHT_O/2
					SpriteOriz2.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin2].y 
					table.insert( ArraySprite, SpriteOriz2)

					local Direzione = display.newImage( "270.png" )
					fitImage(Direzione,CELL_WIDTH_O/4,CELL_HEIGHT_O/2)
					Direzione.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin1].x + CELL_HEIGHT_O3/2
					Direzione.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin2].y
					local aDX = false
					local aSX = false
					if Passi1[indiceArraySprite].xMin2 < Passi1[indiceArraySprite].xMin1 then --VERSO SX
						Direzione.rotation = -180
						aSX = true
					elseif Passi1[indiceArraySprite].xMin2 > Passi1[indiceArraySprite].xMin1 then --VERSO DX
						aDX = true
					end
					Direzione.alpha = 0
					GruppoScenaBase:insert(Direzione)
					table.insert( ArraySpriteArrow, Direzione )

				elseif (indiceArraySpriteYMin == indiceArraySpriteYMax) and (indiceArraySpriteXMax-indiceArraySpriteXMin == 2)  then --Oriz3

					local ImageSheetOrizz3 = graphics.newImageSheet( "SheetOrizzontale3.png",  sheetO3:getSheet() )
					local sequenceData = {
						start = 1,
						count = 58,
						time = 2300,
						loopDirection = "bounce"
					}
					local SpriteOriz3 = display.newSprite( GruppoScenaBase, ImageSheetOrizz3, sequenceData )
					SpriteOriz3.alpha = 0
					SpriteOriz3:play( )
					local YscaleFactor = CELL_HEIGHT_O3 / SpriteOriz3.height 
					local XscaleFactor = CELL_WIDTH_O3 / SpriteOriz3.width 
					SpriteOriz3:scale( XscaleFactor, YscaleFactor )
					SpriteOriz3.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin2].x +CELL_HEIGHT_O
					SpriteOriz3.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin2].y 
					table.insert( ArraySprite,SpriteOriz3)
					--termine SpriteParentesi
					
					local Direzione = display.newImage( "270.png" )
					fitImage(Direzione,CELL_WIDTH_O/4,CELL_HEIGHT_O/2)
					Direzione.x = ZonaLimitatoriX[Passi1[indiceArraySprite].xMin1].x + CELL_HEIGHT_O
					Direzione.y = ZonaLimitatoriY[Passi1[indiceArraySprite].yMin2].y
					local aDX = false
					local aSX = false
					if Passi1[indiceArraySprite].xMin2 < Passi1[indiceArraySprite].xMin1 then --VERSO SX
						Direzione.rotation = -180
						aSX = true
					elseif Passi1[indiceArraySprite].xMin2 > Passi1[indiceArraySprite].xMin1 then --VERSO DX
						aDX = true
					end
					Direzione.alpha = 0
					GruppoScenaBase:insert(Direzione)
					table.insert( ArraySpriteArrow, Direzione )
			
				end
				print( "DIMENSIONE ArraySprite:"..#ArraySprite )
			end
			
			


			function IniziaMostraMosse()
				for inde=1,#objects do
					local isRedMostraMosse = false
					local posXminI,posXmaxI = StampaQuadratiDoveMiTrovoX(objects[inde])
					local posYminI,posYmaxI = StampaQuadratiDoveMiTrovoY(objects[inde])
					if posXminI == Passi1[li].xMin1 and posXmaxI == Passi1[li].xMax1 and posYminI == Passi1[li].yMin1 and posYmaxI == Passi1[li].yMax1 then
						if objects[inde].What == "Orizz2" or objects[inde].What == "Orizz3" then 
							if li ==1 then --PrimoPasso allora mostra lo Sprite
								ArraySprite[li].alpha = 0.8
							end
							ArraySpriteArrow[li].alpha = 1

							if objects[inde].What == "Orizz2" then
								--se è l'ultimo ed è il Rosso metti il suo Listener Originale (per Win)
								if objects[inde].Red ~= nil and objects[inde].Red == "Red" and li == ContaOccorrenze then
									print( "IMPOSTATO LISTEN RED AL VECCHIO ORIGINALE, SEI ALL'ULTIMO" )
									isRedMostraMosse = true
								end
								LoopTransizioni(ArraySpriteArrow[li],true,false,false,false)
							elseif objects[inde].What == "Orizz3" then
								LoopTransizioni(ArraySpriteArrow[li],false,true,false,false)
							end
							
							if isRedMostraMosse then
								objects[inde]:addEventListener( "touch", onTouchOX )
							else	
								objects[inde]:addEventListener( "touch", MoveRectOrizz )
							end
						elseif objects[inde].What == "Vert2" or objects[inde].What == "Vert3" then 
							if li ==1 then--PrimoPasso allora mostra lo Sprite
								ArraySprite[li].alpha = 0.8
							end
							ArraySpriteArrow[li].alpha = 1
							if objects[inde].What == "Vert2" then
								LoopTransizioni(ArraySpriteArrow[li],false,false,true,false)
							elseif objects[inde].What == "Vert3" then
								LoopTransizioni(ArraySpriteArrow[li],false,false,false,true)
							end

							objects[inde]:addEventListener( "touch", MoveRectVert )
						end
						break
					end
				end
			end

			IniziaMostraMosse()
	end --CostruisciStringa

	

	function Refresh(event)
		local opzioni = {params = {id = numLivello}}
		composer.gotoScene( "reload", opzioni )
	end


	k =1
	j = 1	
	for riga =1,6 do
		for colonna = 1,6 do

			if(at1(tabellaStringa,k) == 'B' and grid[riga][colonna] == nil ) then
				piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_V, CELL_HEIGHT_V,6 )
				piece.x = (colonna - 1) * CELL_WIDTH_V + (CELL_WIDTH_V * 0.5) + gbOffsetX
				piece.y = (riga) * CELL_HEIGHT_O + gbOffsetY 
				piece.xScale = 0.93
				piece.yScale = 0.95
				--piece:setFillColor(0.13,0.29,0.41)
				piece.MyID = j
				piece.What = "Vert2"
				piece.fill = paint
				grid[riga][colonna] = piece
				grid[riga+1][colonna] = piece
				objects[j] = piece
				--print( piece.height )
				j = j+1
				piece:addEventListener("touch",onTouchV)	

				elseif(at1(tabellaStringa,k) == 'C' and grid[riga][colonna] == nil ) then
					piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_V3, CELL_HEIGHT_V3,6 )
					piece.x = (colonna - 1) * CELL_WIDTH_V3+ (CELL_WIDTH_V3 * 0.5) + gbOffsetX
					piece.y = (riga ) * CELL_HEIGHT_O + (CELL_WIDTH_V3 * 0.5)+gbOffsetY 
					piece.xScale = 0.93
					piece.yScale = 0.97
					--piece:setFillColor( 0.13,0.29,0.41)
					piece.MyID = j
					piece.What = "Vert3"
					piece.fill = paint
					grid[riga][colonna] = piece
					grid[riga+1][colonna] = piece
					grid[riga+2][colonna] = piece
					objects[j] = piece
					j = j+1
					piece:addEventListener("touch",onTouchV)	
			
				elseif(at1(tabellaStringa,k) == '2' and grid[riga][colonna] == nil) then
					piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
					piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
					piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
					piece.xScale = 0.95
					piece.yScale = 0.90
					--piece:setFillColor(0.13,0.29,0.41)
					piece.MyID = j
					piece.What = "Orizz2"
					piece.fill = paintO
					grid[riga][colonna] = piece
					grid[riga][colonna+1] = grid[riga][colonna]
					objects[j] = piece
					j = j+1
					piece:addEventListener("touch",onTouchO)	
			
				elseif(at1(tabellaStringa,k) == '3' and grid[riga][colonna] == nil) then
					piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_O3, CELL_HEIGHT_O3,6 )
					piece.x = (colonna -1) * CELL_HEIGHT_O3 + (CELL_WIDTH_O3 * 0.5) + gbOffsetX
					piece.y = (riga - 1) * CELL_HEIGHT_O3 + (CELL_HEIGHT_O3 * 0.5) + gbOffsetY 
					piece.xScale = 0.97
					piece.yScale = 0.92
					--piece:setFillColor(0.13,0.29,0.41)
					piece.MyID = j
					piece.What = "Orizz3"
					piece.fill = paintO
					grid[riga][colonna] = piece
					grid[riga][colonna+1] = grid[riga][colonna]
					grid[riga][colonna+2] = grid[riga][colonna]
					objects[j] = piece
					j = j+1
					piece:addEventListener("touch",onTouchO)	

				elseif (at1(tabellaStringa,k) == 'X'and grid[riga][colonna] == nil ) then
					piece = display.newRoundedRect(GruppoScenaBase, 0,0, CELL_WIDTH_O, CELL_HEIGHT_O,6 )
					piece.x = (colonna -1) * CELL_HEIGHT_O + (CELL_WIDTH_O * 0.5) + gbOffsetX
					piece.y = (riga - 1) * CELL_HEIGHT_O + (CELL_HEIGHT_O * 0.5) + gbOffsetY 
					piece.xScale = 0.95
					piece.yScale = 0.90
					piece:setFillColor( 0.90, 0.22, 0.22)
					piece.MyID = j
					piece.What = "Orizz2"
					piece.Red = "Red"
					piece.fill = paintRosso
					grid[riga][colonna] = piece
					grid[riga][colonna+1] = grid[riga][colonna]
					objects[j] = piece
					
					piece.isToucheEnable = true
					Rossa = j
					print( "dichRossa:"..Rossa )
					j = j+1
					piece:addEventListener("touch",onTouchOX)				

			end
			k = k+1
		end
	end


	--AroundMenu:addEventListener( "tap", Pause )
	--AroundBack:addEventListener( "tap", MoveUndo)
	--buttonRestart:addEventListener( "tap", Refresh)
	--AroundResolve:addEventListener( "tap", Accogli )



end ---scene:create

function scene:resumeAfterOverlay( event )
	ContinuaDopoOverlay()
end

function scene:FinishedSolving( ... )
	
end


function scene:resumeAfterYes(event)
	testoDentroLampadina.text = tonumber( testoDentroLampadina.text)-1
	MyData.settings.hint = MyData.settings.hint-1
	if tonumber( testoDentroLampadina.text) == 0 then
		buttonResolve:setEnabled( false )
	end
    for indexYes = 1,#objects do
    	if objects[indexYes].What == "Orizz2" or objects[indexYes].What == "Orizz3"then
    		if objects[indexYes].Red ~= nil and objects[indexYes].Red == "Red" then
    			objects[indexYes]:removeEventListener( "touch", MoveRectOrizz )
    			objects[indexYes]:addEventListener( "touch", onTouchOX )
    		else
    			objects[indexYes]:removeEventListener( "touch", MoveRectOrizz )
				objects[indexYes]:addEventListener( "touch", onTouchO )
			end
		elseif objects[indexYes].What =="Vert2" or objects[indexYes].What =="Vert3" then
			objects[indexYes]:removeEventListener( "touch", MoveRectVert )
			objects[indexYes]:addEventListener( "touch", onTouchV )
		end
    end
    for indiceYesArraySprite =1,#ArraySprite do
    	transition.cancel( ArraySprite[indiceYesArraySprite] )
    	display.remove( ArraySprite[indiceYesArraySprite])
    	ArraySprite[indiceYesArraySprite] = nil

    end
    for indiceYesArrayArrow =1,#ArraySpriteArrow do
    	transition.cancel(ArraySpriteArrow[indiceYesArrayArrow] )
    	display.remove( ArraySpriteArrow[indiceYesArrayArrow])
    	ArraySpriteArrow[indiceYesArrayArrow] = nil
    end 
    li = 1 --Ricomincio senza Suggerimenti
    FlagInCorsoSolver = false
    buttonBack:setEnabled( true )
	buttonRestart:setEnabled( true )
end

function DentroEnterFrame()
	if MyData.settings.currentMode == "Sfida" then
		if MyData.settings.currentLivelloSfida == "Beginner" then
			if tostring( score ) == minMosse then
				MyData.settings.levels.Beginner[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) <= 2 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Beginner[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) > 2 then
				MyData.settings.levels.Beginner[tonumber(numLivello)].stars = 1
			end
			if FlagInCorsoSolver then
				MyData.settings.hint = MyData.settings.hint-1
			end 
			MyData.settings.levels.Beginner[tonumber(numLivello)].score = score	--salva il punteggio di questa board
			utility.saveTable(MyData.settings, "settings9.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {FlagNonAggHint = FlagInCorsoSolver,ArraySpriteParentesiWin=ArraySprite,ArraySpriteArrowWin = ArraySpriteArrow, ScoreMin = minMosse, starToPrint = MyData.settings.levels.Beginner[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score }} )
		
		elseif MyData.settings.currentLivelloSfida == "Intermediate" then
			if tostring( score ) == minMosse then
				MyData.settings.levels.Intermediate[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) <= 2 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Intermediate[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) > 2 then
				MyData.settings.levels.Intermediate[tonumber(numLivello)].stars = 1
			end
			if FlagInCorsoSolver then
				MyData.settings.hint = MyData.settings.hint-1
			end 
			MyData.settings.levels.Intermediate[tonumber(numLivello)].score = score	--salva il punteggio di questa board
			utility.saveTable(MyData.settings, "settings9.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {FlagNonAggHint = FlagInCorsoSolver,ArraySpriteParentesiWin=ArraySprite,ArraySpriteArrowWin = ArraySpriteArrow, ScoreMin = minMosse, starToPrint = MyData.settings.levels.Intermediate[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score }} )
		
		elseif MyData.settings.currentLivelloSfida == "Advanced" then
			if tostring( score ) == minMosse then
				MyData.settings.levels.Advanced[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) <= 2 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Advanced[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) > 2 then
				MyData.settings.levels.Advanced[tonumber(numLivello)].stars = 1
			end
			if FlagInCorsoSolver then
				MyData.settings.hint = MyData.settings.hint-1
			end 
			MyData.settings.levels.Advanced[tonumber(numLivello)].score = score	--salva il punteggio di questa board
			utility.saveTable(MyData.settings, "settings9.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {FlagNonAggHint = FlagInCorsoSolver, ArraySpriteParentesiWin=ArraySprite,ArraySpriteArrowWin = ArraySpriteArrow, ScoreMin = minMosse, starToPrint = MyData.settings.levels.Advanced[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score }} )
		end

	elseif MyData.settings.currentMode == "Relax" then
		if MyData.settings.currentLivelloRelax == "Beginner" then
			if tostring( score ) == minMosse then
				MyData.settings.levels.Beginner[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) <= 2 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Beginner[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) > 2 then
				MyData.settings.levels.Beginner[tonumber(numLivello)].stars = 1
			end
			if FlagInCorsoSolver then
				MyData.settings.hint = MyData.settings.hint-1
			end 
			MyData.settings.levels.Beginner[tonumber(numLivello)].score = score	--salva il punteggio di questa board
			utility.saveTable(MyData.settings, "settings9.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {FlagNonAggHint = false,ArraySpriteParentesiWin=ArraySprite,ArraySpriteArrowWin = ArraySpriteArrow, ScoreMin = minMosse, starToPrint = MyData.settings.levels.Beginner[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score }} )
		
		elseif MyData.settings.currentLivelloRelax == "Intermediate" then
			if tostring( score ) == minMosse then
				MyData.settings.levels.Intermediate[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) <= 2 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Intermediate[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) > 2 then
				MyData.settings.levels.Intermediate[tonumber(numLivello)].stars = 1
			end
			if FlagInCorsoSolver then
				MyData.settings.hint = MyData.settings.hint-1
			end 
			MyData.settings.levels.Intermediate[tonumber(numLivello)].score = score	--salva il punteggio di questa board
			utility.saveTable(MyData.settings, "settings9.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {FlagNonAggHint = false,ArraySpriteParentesiWin=ArraySprite,ArraySpriteArrowWin = ArraySpriteArrow, ScoreMin = minMosse, starToPrint = MyData.settings.levels.Intermediate[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score }} )
		
		elseif MyData.settings.currentLivelloRelax == "Advanced" then
			if tostring( score ) == minMosse then
				MyData.settings.levels.Advanced[tonumber(numLivello)].stars = 3
				
			elseif score-tonumber(minMosse) <= 2 and score-tonumber(minMosse) >=1 then
			  	MyData.settings.levels.Advanced[tonumber(numLivello)].stars = 2
			  	
			elseif score-tonumber(minMosse) > 2 then
				MyData.settings.levels.Advanced[tonumber(numLivello)].stars = 1
			end
			if FlagInCorsoSolver then
				MyData.settings.hint = MyData.settings.hint-1
			end
			print( "SCOREEEEEEE"..score ) 
			MyData.settings.levels.Advanced[tonumber(numLivello)].score = score	--salva il punteggio di questa board
			utility.saveTable(MyData.settings, "settings9.json")
			transition.to(objects[Rossa],{x = display.actualContentWidth+50})
			
			composer.showOverlay("Win", { isModal = true, effect = "slideRight", time = 500 , params = {FlagNonAggHint = false,ArraySpriteParentesiWin=ArraySprite,ArraySpriteArrowWin = ArraySpriteArrow, ScoreMin = minMosse, starToPrint = MyData.settings.levels.Advanced[tonumber(numLivello)].stars  , level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height, myScore = score }} )
		end
	end

end

local function enterFrame(event)
	--print("TouchEnabled:")
	if FlagToEmptyTable then
		print( "StoTogliendoLaTable" )
		for ol = 1,ContaOccorrenzeVERS2 do
			Passi1[ol].xMin1 = nil
			Passi1[ol].xMax1 = nil
			Passi1[ol].xMin2 = nil
			Passi1[ol].xMax2 = nil
		end
		FlagToEmptyTable = false
	end

	if(TouchEnabled == false ) then
		Runtime:removeEventListener("enterFrame", enterFrame)
		addScore()
		timer.performWithDelay(200, DentroEnterFrame)
											 
	end
	 

	if score == 0 then
		buttonBack:setEnabled( false )
		--AroundBack.alpha = 0.7
	end
end

function scene:ProvaEnterFrame()
	Runtime:addEventListener("enterFrame",enterFrame)
end

function ChangeBoardSX(event)
	audio.play( SoundEffectGenerale )
	utility.saveTable(MyData.settings, "settings9.json")
	Runtime:removeEventListener("enterFrame",enterFrame)
	for indiceLocaleChangeBoardSX=1,#ArraySpriteArrow do
		transition.cancel( ArraySpriteArrow[indiceLocale2Refresh])
	end
	composer.gotoScene( "reload" , { params = {id = numLivello-1} })
end

function ChangeBoardDX(event)
	audio.play( SoundEffectGenerale )
	utility.saveTable(MyData.settings, "settings9.json")
	Runtime:removeEventListener("enterFrame",enterFrame)
	for indiceLocaleChangeBoardSX=1,#ArraySpriteArrow do
		transition.cancel( ArraySpriteArrow[indiceLocale2Refresh])
	end
	composer.gotoScene( "reload" ,{params = {id = numLivello+1} })
end

function LanciaAccogli(event )
	if event.phase == "began" then

	elseif 	event.phase == "moved" then

	elseif event.phase == "ended" or event.phase == "cancelled" then
		event.target:setEnabled( false )
		audio.play( SoundEffectGenerale) 
		Accogli()
	end
end

function LanciaMoveUndo(event)
	if event.phase == "began" then

	elseif 	event.phase == "moved" then

	elseif event.phase == "ended" or event.phase == "cancelled" then
		event.target:setEnabled( false )
		audio.play( SoundEffectGenerale) 
		MoveUndo()
	end
end

function LanciaRefresh(event)
	if event.phase == "began" then

	elseif 	event.phase == "moved" then

	elseif event.phase == "ended" or event.phase == "cancelled" then
		audio.play( SoundEffectGenerale)
		Refresh()
	end
end

function Pause(event)
	if event.phase == "began" then
		utility.saveTable(MyData.settings, "settings9.json")

	elseif event.phase == "moved" then

	elseif event.phase == "ended" or event.phase == "cancelled" then
		audio.play( SoundEffectGenerale)
		Runtime:removeEventListener("enterFrame", enterFrame)
		composer.showOverlay("OnPause", { isModal = true, effect = "slideRight", time = 600 , params = {inResolve = FlagInCorsoSolver, ArraySpriteParentesi = ArraySprite,ArraySpritePause = ArraySpriteArrow, level = numLivello, checkWidth = checkerBoard.width , checkHeight = checkerBoard.height}} )
	end
end


function scene:show(event)
	local phase = event.phase
	if ( phase == "will" ) then
		Runtime:addEventListener("enterFrame", enterFrame)
	elseif ( phase == "did" ) then
		local scenaDaTrovare = composer.getSceneName( "previous" )
		if scenaDaTrovare == "level_selection" and FlagInCorsoSolver then
			for indiceShow = 1,#objects do
				if objects[indiceShow].What == "Orizz2" or objects[indiceShow].What == "Orizz3" then
					if objects[indiceShow].Red ~= nil and objects[indiceShow].Red == "Red" then
						print( "Valori = li:ContaOccorrenze "..li.." "..ContaOccorrenze ) 
						if li == ContaOccorrenze then

						else
							objects[indiceShow]:removeEventListener( "touch", MoveRectOrizz )
							objects[indiceShow]:addEventListener( "touch", onTouchOX )
						end
						--objects[indiceShow]:addEventListener( "touch", onTouchOX )
					else
						objects[indiceShow]:removeEventListener( "touch", MoveRectOrizz )
						objects[indiceShow]:addEventListener( "touch", onTouchO )
					end
				else 
					objects[indiceShow]:removeEventListener( "touch", MoveRectVert )
					objects[indiceShow]:addEventListener( "touch", onTouchV )
				end
			end
			testoDentroLampadina.text = tonumber( testoDentroLampadina.text)-1
			MyData.settings.hint = MyData.settings.hint-1
				if tonumber( testoDentroLampadina.text) == 0 then
					buttonResolve:setEnabled( false )
				end
		end
		--print( "PORCA MISERIA" )
		li = 1
		FlagInCorsoSolver = false
		buttonBack:setEnabled( true )
		buttonRestart:setEnabled( true )
	end
end

function scene:hide( event )
	local phase = event.phase
	if ( phase == "will" ) then
		--audio.fadeOut( { time = 1000 })
	elseif ( phase == "did" ) then
		Runtime:removeEventListener("enterFrame", enterFrame)
			--print("HIDE: "..hide)
	end
end

function scene:destroy( event )
end


scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene




















 







--
-- Calculate some values
--



--
-- Using positions 1, 8 for X and Y, draw the object at the right place in the grid
--


			




--
-- Generate a few objects
--
--


	--else
	--	local xPos = math.random( 1, 6)
	--	local yPos = math.random( 1, 6 )
	--	flagOrizz=false
	--	spawnPiece(xPos, yPos,flagOrizz)
--	end
 

--timer.performWithDelay(200, function() movePiece( lastObject, 4, 5); end, 1)